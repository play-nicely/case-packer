$repoDir = '/var/repo';

Write-Output "Starting interactive build tools image";
Write-Output "Change directory to 'cd $repoDir' to debug";

$versionInfo = Get-Content `
              (Join-Path $PSScriptRoot 'CI/.shared-config/common/build-tools/.vars-build-tools-version.yml') `
              | ConvertFrom-Json;
              
docker run -i --rm --tty -v "${PSScriptRoot}:$repoDir" `
    --name play-nicely `
    "registry.gitlab.com/play-nicely/foundation/build-tools:$($versionInfo.variables.PIPE_BUILDTOOLS_VERSION)" `
    bash;
