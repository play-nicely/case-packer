[bdd]: https://en.wikipedia.org/wiki/Behavior-driven_development
[gitlab-after_script]: https://docs.gitlab.com/ee/ci/yaml/#after_script
[gitlab-before_script]: https://docs.gitlab.com/ee/ci/yaml/#before_script
[gitlab-extends]: https://docs.gitlab.com/ee/ci/yaml/#extends
[gitlab-script]: https://docs.gitlab.com/ee/ci/yaml/#script
[gitlab-variables]: https://docs.gitlab.com/ee/ci/yaml/#variables
[gitlab-var-use]: https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
[nodejs]: https://nodejs.org/
[npmjs]: https://www.npmjs.com/
[nuget]: https://nuget.org
[pn-executor]: https://www.nuget.org/packages/PlayNicely.Executor
[pn-projects]: https://www.nuget.org/packages/PlayNicely.Projects
[pn-specflow]: https://www.nuget.org/packages?q=playnicely.specflow
[semver]: https://semver.org

# Play Nicely - Foundation

Foundation exists to support the development of Play Nicely _tool_
[NuGet][nuget] packages. However, it is suitable for any tool package that 
requires release testing (which is all of them, right?).

The testing and debugging of these (design-time) packages can be particularly
challenging, due to the fact that they execute within the context of the
development process. They also typically run tasks for a _target_ project,
which is not easy to simulate within the IDE in a consistent and predictable
manner.

To simplify this interaction, the Foundation projects expose interfaces,
classes and tooling that support [Behavior-Driven Development][bdd] (BDD) of
design-time packages.

It includes:

* [**Projects**][pn-projects]\
  Virtual definition of test case (.NET) projects and packaging of them for
  easy distribution and test execution.
* [**Execution**][pn-executor]\
  Supports the creation of test case environments, with a test case project,
  upon which programs can be executed (including `dotnet`), and the various
  results asserted.
* [**SpecFlow**][pn-specflow]\
  Bindings for the definition of [BDD][bdd] scenarios that exercise in-development
  packages. Definitions include:
  * Excluding or requiring programs from/on the `PATH`
  * Defining a project, upon which to run
  * Running programs or `dotnet` commands
  * Checking stdout/stderr outputs, files, MSBuild errors, targets and/or
    projects.

To put it another way...

> In order to test use of [npm packages][npmjs] in Visual Studio projects, as a
  Developer, I want tools that allow easy definition, and execution, of test
  cases against my package.

This project was born from the desire to use [Node.js][nodejs] tooling within 
Visual Studio. The Node.js ecosystem, and community, has loads of amazing 
packages, tailwindcss, sass, and many more! Packages that .NET developers 
should (want to) use within their projects.

> ℹ️ **Why not use Visual Studio extensions?**\
  Sure, there are Visual Studio extensions that support use of these tools,
  but they add another dependency into the process, and they often don't work
  with CI/CD pipelines. If you already have a dependency on a npm package, and
  therefore npm (executable), it makes sense to just rely on that dependency.
  If, in Visual Studio, we directly depend on the npm package and tooling, we
  get the support of the community that is developing it, latest security 
  fixes, releases, etc.

## Getting started with development

The main repository has two root directories, `Code` and `CI`, all functional
code is in the `Code` directory and anything to do with CI/CD, other than the 
bootstrapping `.gitlab‑ci.yml`, is in the `CI` directory.

> ℹ️ **Wait, there are three root directories?**\
  In case you're thinking my maths is wrong, the third directory `Resources`
  is used to store additional project resources, such as icon _source_ files, 
  or diagrams. Sure these things matter, but they aren't worth discussing in
  here more than this reference.

### Payload Development

The code is managed within a single solution, but broken up into three,
independently versioned, layers:

```mermaid
graph TB
    subgraph sg_sf["SpecFlow"];
        sg_sf_dn("PlayNicely.SpecFlow.DotNet") --> sg_sf_exe("PlayNicely.SpecFlow.Executor")
    end

    subgraph sg_exe["Execution"];
        sg_exe_dn("PlayNicely.Executor.DotNet") --> sg_exe_exe("PlayNicely.Executor")
    end

    subgraph sg_prj["Projects"];
        sg_prj_prj("PlayNicely.Projects")
    end

    sg_sf_dn --> sg_exe_dn
    sg_sf_exe --> sg_exe_exe
    sg_exe_exe --> sg_prj_prj
```

Each layer is versioned independently and has a unit test project for its
features. If you're making changes, make sure you update the version number, we
use [Semantic Versioning 2.0][semver], and include unit tests for
the functionality (the version is defined in the `Version.props` file of each
package).

### CI/CD Development

The CI/CD configuration includes one **main** pipeline and, if the **main** 
pipeline succeeds, two manually triggered downstream pipelines,
**feature‑prerelease** and **production‑release**.

#### Main Pipeline

The **main** pipeline runs on all commits/pushes to the repository. It includes
stages to prepare build tools, build solution binaries, test those binaries,
package them into pre-release nuget packages, and, on some branches, publish
those pre-release packages to [nuget.org][nuget].

The jobs that run on the **main** pipeline, depend on the branch that is
pushed to the remote repository.

| Branch&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | CI Pipeline Activities |
|:----------------------------------|:------------|
| `main`            | <ol><li><a id="main-pipeline-steps">Prepare build tools</a></li><li>Compile binaries</li><li>Run tests</li><li>Generate pre-release packages</li><li>Publish to [nuget.org][nuget]</li><li>Create downstream `production‑release` pipelines</li></ol> |
| _everything-else_ | <ol><li>Prepare build tools</li><li>Compile binaries</li><li>Run tests</li><li>Generate pre-release packages</li><li>Create downstream pipeline `feature‑prerelease`</li></ol> |

> ℹ️ **Main pipeline is _not_ `main` branch**\
  Do not confuse the **main** pipeline with the `main` branch, the **main** 
  pipeline _runs on all commits_, the `main` branch is the current stable
  version of the code.

#### Feature‑prerelease Pipeline

Sometimes, when developing a feature on a feature branch, you need to test the
downstream use of those changes in a _real_ environment.  The
**feature‑prerelease** downstream pipeline supports this workflow. Simply
trigger the `start` job of this pipeline and it will publish the package to
[nuget.org][nuget] as a pre-release version, you can then consume and test it
in your downstream project.

#### Production‑release Pipeline

After merging to the `main` branch and completion of the **main** pipeline, a
manually started child pipeline is created. This child pipeline releases the
package to _production_. After triggering the `start` job, the pipeline
creates a _stable_ package and publishes it to [nuget.org][nuget].

> ⚠️ **Version matters**\
  Make sure you review what has changed since the last release and update
  the `Version.props` per [semver.org][semver] rules. Failure to update the
  version number will cause the release to fail.

#### Debugging CI/CD

The only way to test the full CI pipeline is to publish to the remote
repository and see what happens, but for individual steps it is possible to run
the docker image locally, and test in a _binary identical_ environment. 

The project has a PowerShell script to support this, `Start‑BuildTools.ps1`, or
bash, `start‑buildtools.sh`, if that's your weapon of choice. Running the
script will start a Docker container and launch an interactive console. The 
local repository will be mapped to the container's file system. From the 
console, you can run commands, test individual steps and check outputs.

##### `ci‑dev` Branches

When developing software it helps to have a non-production environment.
Developing CI/CD pipelines is just another type of software development,
unfortunately, all pipelines run in production.  It begs the question...

> How do I test my pipelines without effecting production?

That is what the `ci‑dev/*` branches are for, any commit pushed to a branch
under the `ci‑dev/` namespace, will execute as if `ci‑dev/` is removed, except
that the pipeline will not do any physical deployments.

For example, if you push changes to branch `ci‑dev/main`, the pipeline that
executes will be the same as the one that would, if a change were pushed to the
`main` branch. It will include any downstream pipelines, and run all tasks,
with these differences.

* The `publish` jobs will run but not actually publish anything, instead, the
  output will include echo'd commands that would have been run.
* The `release` jobs in any **production‑release** pipelines will be excluded
  from the pipeline.

This is achieved via two variables:

1. The variable `JOB_CI_SIMULATE` is set to `true` for simulated builds. This
   variable is set during a task's [`before_script`][gitlab-before_script]
   execution. You can test this variable in your scripts and alter execution
   accordingly.

   > ⚠ **Limited use of `JOB_CI_SIMULATE`**\
     This variable is only available in the [`script`][gitlab-script], or
     [`after_script`][gitlab-after_script], section, it can't be used in any
     other (yml) configuration section.

1. If you want to exclude a job from the simulated pipeline, your job can
   [extend][gitlab-extends] any of the `.branch‑*` jobs and set the CI 
   [variable][gitlab-variables] `JOB_CI_SIMULATE_EXCLUDE` to `"true"`. This
   variable _is_ available for use in [yml sections][gitlab-var-use].

## Support

If you find bugs or want to raise feature requests, you can do so
[here](https://gitlab.com/play-nicely/foundation/-/issues).

## Roadmap

Items on (or not) the roadmap.

### `specflow.json` Updates on Install

Update, or create, the `specflow.json` file of the target project when 
installing the PlayNicely.SpecFlow packages.

Yep, I reckon we'll do this.

## License

If you use this package, you accept the [MIT Licence](https://opensource.org/license/MIT/).

## Project status

This project is considered feature complete, if feature requests are received
they will be considered and applied at my discretion.
