﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Collections;
using System.Security;
using System.Text.Json;

namespace PlayNicely.Executor.DotNet;

using static DotNetConsts;

public class DotNetLogger : Logger
{
    public override void Initialize(IEventSource eventSource)
    {
        eventSource.BuildFinished += EventSource_BuildFinished;

        eventSource.ProjectStarted += EventSource_ProjectStarted;
        eventSource.TargetStarted += EventSource_TargetStarted;

        eventSource.ErrorRaised += EventSource_ErrorRaised;
    }

    private void EventSource_BuildFinished(object sender, BuildFinishedEventArgs e)
    {
        _resultBuilder.BuildFinished(e.Succeeded);

        using var writer = OpenLogWriter();

        var buildResult = _resultBuilder.Build();
        var resultAsJson = JsonSerializer.Serialize(buildResult, Json.SerializerOptions);

        writer.WriteLine(resultAsJson);
    }

    private void EventSource_ErrorRaised(object sender, BuildErrorEventArgs e)
    {
        _resultBuilder.ErrorOccurred(e.Code, e.Message);
    }

    private void EventSource_ProjectStarted(object sender, ProjectStartedEventArgs e)
    {
        ArgumentException.ThrowIfNullOrEmpty(e.ProjectFile);

        var properties = e.Properties?.Cast<DictionaryEntry>()
                                     .Select(de => new KeyValuePair<string, string>((string) de.Key, (string?) de.Value ?? string.Empty))
            ?? Array.Empty<KeyValuePair<string, string>>();
        var projectAbsolutePath = e.ProjectFile;
        var projectName = properties.Where(kvp => kvp.Key.Equals(KeyProjectName))
                                    .Select(kvp => kvp.Value as string)
                                    .FirstOrDefault()
            ?? Path.GetFileNameWithoutExtension(projectAbsolutePath);
        var projectDir = properties.Where(kvp => kvp.Key.Equals(KeyProjectDir))
                                   .Select(kvp => kvp.Value as string)
                                   .FirstOrDefault()
            ?? Path.GetDirectoryName(projectAbsolutePath)
            ?? throw new InvalidOperationException(ContentRes.Error_DotNetLogger_ProjectDirShouldNotBeNull);
        var projectFile = Path.GetRelativePath(projectDir, projectAbsolutePath);

        _resultBuilder.ProjectStarted(projectName, projectFile, projectDir);
    }

    private void EventSource_TargetStarted(object sender, TargetStartedEventArgs e)
    {
        _resultBuilder.TargetStarted(e.TargetName);
    }

    private TextWriter OpenLogWriter()
    {
        var logFileName = ResolveLogFileName();

        if(logFileName == null)
            return new StreamWriter(Console.OpenStandardOutput(), Console.OutputEncoding);

        try
        {
            return new StreamWriter(logFileName, false, Json.Encoding);
        }
        catch(Exception ex) when(ex is UnauthorizedAccessException
                              || ex is ArgumentNullException
                              || ex is PathTooLongException
                              || ex is DirectoryNotFoundException
                              || ex is NotSupportedException
                              || ex is ArgumentException
                              || ex is SecurityException
                              || ex is IOException)
        {
            throw new LoggerException(string.Format(ContentRes.Error_DotNetLogger_FailedToCreateFile,
                                                    nameof(DotNetExecutionContext),
                                                    ex.Message));
        }
    }

    private string? ResolveLogFileName()
    {
        if(null == Parameters)
            return null;

        var parameters = Parameters.Split(';', StringSplitOptions.RemoveEmptyEntries);
        var logFile = parameters.FirstOrDefault();

        if(string.IsNullOrEmpty(logFile))
            return null;

        return logFile;
    }

    private const string KeyProjectDir = "ProjectDir";
    private const string KeyProjectName = "ProjectName";

    private readonly DotNetExecutionContextBuilder _resultBuilder = new();
}
