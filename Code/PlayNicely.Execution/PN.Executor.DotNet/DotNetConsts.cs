﻿using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using PlayNicely.Projects;

namespace PlayNicely.Executor.DotNet;

internal static class DotNetConsts
{
    public static class Json
    {
        public static Encoding Encoding => ProjectsConsts.DefaultEncoding;
        public static JsonSerializerOptions SerializerOptions => _defaultJsonOptions.Value;

        private static JsonSerializerOptions MakeSerializerOptions()
        {
            return new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = false,
            };
        }

        private static readonly Lazy<JsonSerializerOptions> _defaultJsonOptions = new(MakeSerializerOptions);
    }
}
