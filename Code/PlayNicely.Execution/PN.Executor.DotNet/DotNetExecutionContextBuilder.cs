﻿using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace PlayNicely.Executor.DotNet;

internal class DotNetExecutionContextBuilder
{
    public DotNetExecutionContextBuilder()
    {
        Errors = [];
        Projects = new DotNetProjectCollection();
        Targets = ReadOnlyDictionary<string, int>.Empty;
    }

    public DotNetExecutionContext Build()
    {
        ThrowIfBuildNotFinished();

        IDictionary<string, DotNetProject>? projects = _projects?.ToImmutableDictionary(proj => proj.Name);

        return new()
        {
            Succeeded = Succeeded,
            Errors = Errors,
            Projects = projects ?? ReadOnlyDictionary<string, DotNetProject>.Empty,
            Targets = Targets,
        };
    }

    public DotNetExecutionContextBuilder BuildFinished(bool succeeded)
    {
        _succeeded = succeeded;

        return this;
    }

    public DotNetExecutionContextBuilder ErrorOccurred(string? code, string? message = null)
    {
        if(_errors == null)
        {
            Interlocked.CompareExchange(ref _errors, new(), null);
            Errors = _errors.AsReadOnly();
        }

        _errors.Add(new DotNetError(code, message));

        return this;
    }

    public DotNetExecutionContextBuilder ProjectStarted(string projectName, string projectPath, string workingDirectory)
    {
        if(_projects == null)
            Interlocked.CompareExchange(ref _projects, new(DotNetProjectComparer.Instance), null);

        _projects.Add(new DotNetProject(projectName, projectPath, workingDirectory));

        return this;
    }

    public DotNetExecutionContextBuilder TargetStarted(string targetName)
    {
        if(_targets == null)
        {
            Interlocked.CompareExchange(ref _targets, new(), null);
            Targets = _targets.AsReadOnly();
        }

        if(_targets.TryAdd(targetName, 1) == false)
            _targets[targetName]++;

        return this;
    }

    protected class DotNetProjectCollection : KeyedCollection<string, DotNetProject>
    {
        public DotNetProjectCollection() { }

        protected override string GetKeyForItem(DotNetProject item) => item.Name;
    }

    protected IList<DotNetError> Errors { get; private set; }
    protected DotNetProjectCollection Projects { get; }
    protected bool Succeeded => _succeeded == true;
    protected IDictionary<string, int> Targets { get; private set; }

    [MemberNotNull(nameof(_succeeded))]
    private void ThrowIfBuildNotFinished()
    {
        if(_succeeded == null)
            throw new InvalidOperationException(string.Format(ContentRes.Error_DotNet_CannotGenerateUntilFinished, nameof(DotNetExecutionContext)));
    }

    private List<DotNetError>? _errors;
    private HashSet<DotNetProject>? _projects;
    private bool? _succeeded;
    private Dictionary<string, int>? _targets;
}
