﻿using System.Diagnostics.CodeAnalysis;

namespace PlayNicely.Executor.DotNet;

public class DotNetProjectComparer : IEqualityComparer<DotNetProject>
{
    public bool Equals(DotNetProject? x, DotNetProject? y) => x?.Name == y?.Name;
    public int GetHashCode([DisallowNull] DotNetProject obj) => obj.GetHashCode();

    public static DotNetProjectComparer Instance => _instance.Value;

    private DotNetProjectComparer() { }

    private static readonly Lazy<DotNetProjectComparer> _instance = new(() => new DotNetProjectComparer());
}
