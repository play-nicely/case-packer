﻿[bdd]: https://en.wikipedia.org/wiki/Behavior-driven_development
[ms-logger]: https://learn.microsoft.com/en-us/visualstudio/msbuild/build-loggers?view=vs-2022
[pn-executor]: https://www.nuget.org/packages/PlayNicely.Executor
[pn-specflow-dotnet]: https://www.nuget.org/packages/PlayNicely.SpecFlow.DotNet

# Play Nicely - DotNet Executor

This package provides a `dotnet [command]` specific `ITestEnvironmentRunner`.
The runner returns a `ExecutionResult<DotNetExecutionContext>` result, which
exposes details about the dotnet process. These details are collected by a
custom [MSBuild.Logger][ms-logger] that is specifed during execution using the
`‑logger` parameter.

## Getting Started

The `DotNetRunner` class implements the `ITestEnvironmentRunner` interface,
thus extending the [PlayNicely.Executor][pn-executor] package to support .NET
specific use cases.

[PlayNicely.SpecFlow.DotNet][pn-specflow-dotnet] depends on this package,
providing [Behavior-Driven Development][bdd] language bindings that can be used
to exercise .NET testing scenarios.

For a more complete getting started guide, check out the 
[PlayNicely.Executor][pn-executor] package.
