﻿using System.ComponentModel;
using System.Diagnostics;

namespace PlayNicely.Executor.DotNet;

[DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
public class DotNetError
{
    public DotNetError(string? code, string? message = null)
    {
        if(string.IsNullOrEmpty(code) && string.IsNullOrEmpty(message))
            throw new ArgumentException(ContentRes.Error_DotNetError_ErrorMustIncludeCodeOrMessage);

        Code = code ?? string.Empty;
        Message = message ?? string.Empty;
    }

    [DefaultValue("")]
    public string Code { get; }

    [DefaultValue("")]
    public string Message { get; }

    private string DebuggerDisplay => $"{Code}{DebuggerSeparator}{Message}";
    private string DebuggerSeparator => string.IsNullOrEmpty(Code) == false && string.IsNullOrEmpty(Message) == false ? ": " : string.Empty;
}
