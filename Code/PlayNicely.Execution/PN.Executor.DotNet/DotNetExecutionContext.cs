﻿using System.Collections.ObjectModel;
using System.Text.Json.Serialization;

namespace PlayNicely.Executor.DotNet;

public class DotNetExecutionContext
{
    public DotNetExecutionContext()
    {
        Errors = Array.Empty<DotNetError>();
        Projects = ReadOnlyDictionary<string, DotNetProject>.Empty;
        Targets = ReadOnlyDictionary<string, int>.Empty;
    }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    public IList<DotNetError> Errors { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    public IDictionary<string, DotNetProject> Projects { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.Never)]
    public bool Succeeded { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    public IDictionary<string, int> Targets { get; set; }
}
