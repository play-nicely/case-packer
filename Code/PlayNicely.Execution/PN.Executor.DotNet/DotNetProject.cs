﻿namespace PlayNicely.Executor.DotNet;

public class DotNetProject : IEquatable<DotNetProject>
{
    public DotNetProject(string name, string projectPath, string workingDirectory)
    {
        ArgumentException.ThrowIfNullOrEmpty(name);
        ArgumentException.ThrowIfNullOrEmpty(projectPath);
        ArgumentException.ThrowIfNullOrEmpty(workingDirectory);

        Name = name;
        ProjectPath = projectPath;
        WorkingDirectory = workingDirectory;
    }

    public static bool Equals(DotNetProject? x, DotNetProject? y) => DotNetProjectComparer.Instance.Equals(x, y);
    public bool Equals(DotNetProject? other) => Equals(this, other);
    public override bool Equals(object? obj) => Equals(this, obj as DotNetProject);
    public override int GetHashCode() => Name.GetHashCode();

    public string Name { get; }
    public string ProjectPath { get; }
    public string WorkingDirectory { get; }
}
