﻿using System.Diagnostics;
using System.Text.Json;

namespace PlayNicely.Executor.DotNet;

using static DotNetConsts;

public class DotNetRunner : ProcessRunnerWithResultContext<DotNetExecutionContext>
{
    public DotNetRunner(string subCommand, params string[] args) : this(subCommand, (IEnumerable<string>) args) { }
    public DotNetRunner(string subCommand, IEnumerable<string>? args = null) : base(DotNetCommand, args)
    {
        ArgumentException.ThrowIfNullOrEmpty(subCommand);

        InitializeDebugSettings();

        _subCommand = subCommand;
    }

    public const string DotNetCommand = "dotnet";
    public static readonly IList<string> RequiredCommands = Array.AsReadOnly(new[] { DotNetCommand, });

    protected override IEnumerable<string> PrependArguments()
    {
        yield return _subCommand;

        if(string.IsNullOrEmpty(TestEnvironment.ProjectFilename) == false)
            yield return TestEnvironment.ProjectFilename;

        yield return $"-logger:{GetLoggerTypeDef()};{GetExecutionContextPath(TestEnvironment)}";
    }

    protected override async Task<DotNetExecutionContext> RetreiveExecutionContextAsync(Process process, CancellationToken cancel)
    {
        using var contextStream = new FileStream(GetExecutionContextPath(TestEnvironment), FileMode.Open);

        var dotNetContext = await JsonSerializer.DeserializeAsync<DotNetExecutionContext>(contextStream, Json.SerializerOptions, cancel)
                                                .ConfigureAwait(false);

        return dotNetContext!;
    }

    private static string GetExecutionContextPath(ITestEnvironment testEnv)
    {
        return Path.Join(testEnv.TempDirectory, ExecutionContextFileName);
    }

    private static string GetLoggerTypeDef()
    {
        var loggerType = typeof(DotNetLogger);
        var fullName = loggerType.FullName;
        var assemblyPath = loggerType.Assembly.Location;

        return $"{fullName},{assemblyPath}";
    }

    [Conditional("DEBUG")]
    private void InitializeDebugSettings()
    {
        RunTimeout = TimeSpan.FromSeconds(30);
    }

    private const string ExecutionContextFileName = "context.log";

    private readonly string _subCommand;
}
