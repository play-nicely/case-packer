using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.InteropServices;
using NuGet.Configuration;
using NUnit.Framework.Interfaces;
using PlayNicely.Executor.DotNet;
using PlayNicely.Projects;

namespace PlayNicely.Tests;

[TestFixture]
public class TestEnvironmentBuilderTests
{
    [Test]
    public async Task AbsolutePackageSourceAddedAsIs()
    {
        InitializeTestSubject();

        var absoluteSource = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? @"C:\NuGet" : "/nuget";

        _testSubject.AddPackageSource(absoluteSource, nameof(AbsolutePackageSourceAddedAsIs));

        using var testEnv = await _testSubject.BuildAsync()
                                              .ConfigureAwait(false);

        Assert.That(testEnv.GetPackageSources(), Has.Some.Matches<PackageSource>(ps => ps.Source == absoluteSource));
    }

    [Test]
    public void AddEmptySourceNameThrowsException()
    {
        InitializeTestSubject();

        Assert.That(() => _testSubject.AddPackageSource(@"..\NuGet", ""), Throws.ArgumentException);
    }

    [Test]
    public void AddNullPackageSourceThrowsException()
    {
        InitializeTestSubject();

        string? strNull = null;

        Assert.That(() => _testSubject.AddPackageSource(strNull!, "source-1"), Throws.ArgumentNullException);
    }

    [Test]
    public void AddNullSourceNameThrowsException()
    {
        InitializeTestSubject();

        string? strNull = null;

        Assert.That(() => _testSubject.AddPackageSource(@"..\NuGet", strNull!), Throws.ArgumentNullException);
    }

    [Test]
    public void AddSameNamedSourceThrowsException()
    {
        InitializeTestSubject();

        const string sourceName = nameof(AddSameNamedSourceThrowsException);

        _testSubject.AddPackageSource("NuGet", sourceName);

        Assert.That(() => _testSubject.AddPackageSource("BuGet", sourceName), Throws.ArgumentException);
    }

    [Test]
    public async Task BuildProjectDependingOnPackageInLocalSourceSucceeds()
    {
        const string sourceName = nameof(BuildProjectDependingOnPackageInLocalSourceSucceeds);

        using var testCasePackage = await BuildTestCasePackageAsync().ConfigureAwait(false);

        InitializeTestSubject();

        _testSubject.ClearPackageSources()
                    .AddPackageSource(testCasePackage.SourceLocation, sourceName);
        _testSubject.SetProject(CreateProjectDependingOnPackages(testCasePackage.PackageId));

        var buildResult = await RunDotNetBuildAsync().ConfigureAwait(false);

        Assert.That(buildResult.Succeeded);
    }

    [Test]
    public async Task BuildProjectWithMappedLocalSourceSucceeds()
    {
        const string packageSource = nameof(BuildProjectWithMappedLocalSourceSucceeds);

        using var testCasePackage = await BuildTestCasePackageAsync().ConfigureAwait(false);

        InitializeTestSubject();

        _testSubject.AddPackageSource(testCasePackage.SourceLocation, packageSource)
                    .MapPackagesToSource(packageSource, testCasePackage.PackageId)
                    .UseBestPackageSourceFallback();
        _testSubject.SetProject(CreateProjectDependingOnPackages(PackageIdJsonDotNet, testCasePackage.PackageId));

        var buildResult = await RunDotNetBuildAsync().ConfigureAwait(false);

        Assert.That(buildResult.Succeeded);
    }

    [Test]
    public void BuildWithUninstalledRequiredCommandThrowsException()
    {
        InitializeTestSubject();

        _testSubject.RequiredCommands("foo-bar");

        Assert.That(async () => await _testSubject.BuildAsync()
                                                  .ConfigureAwait(false),
                    Throws.InstanceOf<InvalidEnvironmentException>());
    }

    [Test]
    public async Task ClearPackageSourcesClearsAllIncludingSharedSources()
    {
        InitializeTestSubject();

        _testSubject.ClearPackageSources();

        using var testEnv = await _testSubject.BuildAsync()
                                              .ConfigureAwait(false);

        Assert.That(testEnv.GetPackageSources(), Is.Empty);
    }

    [Test]
    public async Task ClearPackageSourcesFailsOnProjectRestore()
    {
        InitializeTestSubject();

        _testSubject.ClearPackageSources();
        _testSubject.SetProject(CreateProjectDependingOnJsonDotNet());

        var buildResult = await RunDotNetBuildAsync().ConfigureAwait(false);

        Assert.Multiple(() =>
        {
            Assert.That(buildResult.Succeeded, Is.False);
            Assert.That(buildResult.Context.Errors,
                        Has.Some.Matches<DotNetError>(err => err.Code == NuGetError_UnableToResolvePackage
                                                            || err.Code == NuGetError_UnableToFindPackage));
        });
    }

    [Test]
    public void ExcludeRequiredCommandThrowException()
    {
        const string command = nameof(ExcludeRequiredCommandThrowException);

        InitializeTestSubject();

        _testSubject.RequiredCommands(command);

        Assert.That(() => _testSubject.ExcludeCommandFromPath(command), Throws.InvalidOperationException);
    }

    [Test]
    public async Task HttpPackageSourceAddedAsIs()
    {
        InitializeTestSubject();

        const string httpSource = "https://somesite.org/nuget-packages";

        _testSubject.AddPackageSource(httpSource, nameof(HttpPackageSourceAddedAsIs));

        using var testEnv = await _testSubject.BuildAsync()
                                              .ConfigureAwait(false);

        Assert.That(testEnv.GetPackageSources(), Has.Some.Matches<PackageSource>(ps => ps.Source == httpSource));
    }

    [Test]
    public async Task MapAllPackagesToEmptyLocalSourceFails()
    {
        const string sourceName = nameof(MapAllPackagesToEmptyLocalSourceFails);

        InitializeTestSubject();

        _testSubject.AddPackageSource("..", sourceName)
                    .MapPackagesToSource(sourceName, "*");
        _testSubject.SetProject(CreateProjectDependingOnJsonDotNet());

        var buildResult = await RunDotNetBuildAsync().ConfigureAwait(false);

        Assert.That(buildResult.Succeeded, Is.False);
    }

    [Test]
    [TestCaseSource(typeof(TestEnvironmentBuilderTests), nameof(RelativePackageSources))]
    public async Task RelativePackageSourceIsRelativeToExecutingAssembly(string relativeSource)
    {
        InitializeTestSubject();

        _testSubject.AddPackageSource(relativeSource, nameof(RelativePackageSourceIsRelativeToExecutingAssembly));

        using var testEnv = await _testSubject.BuildAsync()
                                              .ConfigureAwait(false);
        var absoluteSourcePath = Path.GetFullPath(relativeSource, ExecutingAssemblyDir);

        Assert.That(testEnv.GetPackageSources(), Has.Some.Matches<PackageSource>(ps => ps.Source == absoluteSourcePath));
    }

    [Test]
    public void RequireExcludedCommandThrowException()
    {
        const string command = "find";

        InitializeTestSubject();

        _testSubject.ExcludeCommandFromPath(command);

        Assert.That(() => _testSubject.RequiredCommands(command), Throws.InvalidOperationException);
    }

    [TearDown]
    protected void AfterEachTest()
    {
        if(TestContext.CurrentContext.Result.Outcome.Status != TestStatus.Failed)
            return;

        if(executionResult is not null)
            executionResult.WriteStreamToConsole();
    }

    [SetUp]
    protected void BeforeEachTest()
    {
        executionResult = null;
    }

    private class TestCasePackage : IDisposable
    {
        public TestCasePackage(ITestEnvironment environment, string packageId)
        {
            ArgumentNullException.ThrowIfNull(environment);
            ArgumentException.ThrowIfNullOrEmpty(packageId);

            _environment = environment;
            PackageId = packageId;
        }

        public void Dispose()
        {
            _environment.Dispose();
        }

        public string PackageId { get; }
        public string SourceLocation => _environment.TempDirectory;

        private readonly ITestEnvironment _environment;
    }

    private static TestCaseProject CreateProjectDependingOnJsonDotNet()
    {
        return CreateProjectDependingOnPackages(PackageIdJsonDotNet);
    }

    private static TestCaseProject CreateProjectDependingOnPackages(params string[] packageIds)
    {
        var testCaseProject = new TestCaseProject(nameof(ClearPackageSourcesFailsOnProjectRestore));

        testCaseProject.ProjectFile = testCaseProject.Root.AddFile("project.csproj");

        using var writer = new StreamWriter(testCaseProject.ProjectFile.OpenWriteStream());

        writer.WriteLine("<Project Sdk=\"Microsoft.NET.Sdk\">");
        writer.WriteLine("  <PropertyGroup>");
        writer.WriteLine("    <TargetFramework>netstandard2.0</TargetFramework>");
        writer.WriteLine("  </PropertyGroup>");

        writer.WriteLine("  <ItemGroup>");

        foreach(var packageId in packageIds)
            writer.WriteLine($"    <PackageReference Include=\"{packageId}\" Version=\"*\" />");

        writer.WriteLine("  </ItemGroup>");
        writer.WriteLine("</Project>");

        return testCaseProject;
    }

    private static string GetExecutingAssemblyDir()
    {
        var thisAssembly = Assembly.GetExecutingAssembly();
        var thisAssemblyDir = Path.GetDirectoryName(thisAssembly.Location);

        if(thisAssemblyDir is null)
            throw new InvalidOperationException(string.Format(ContentRes.Error_ExecutingAssembly_NoDirectory, thisAssembly.FullName));

        return thisAssemblyDir;
    }

    private async Task<TestCasePackage> BuildTestCasePackageAsync()
    {
        const string packageId = nameof(BuildTestCasePackageAsync);

        var builder = new TestEnvironmentBuilder();
        var packageProject = new TestCaseProject(packageId);

        var projectFile = packageProject.Root.AddFile("pack.csproj");

        packageProject.ProjectFile = projectFile;

        using(var writer = new StreamWriter(projectFile.OpenWriteStream()))
        {
            writer.WriteLine("<Project Sdk=\"Microsoft.NET.Sdk\">");
            writer.WriteLine("  <PropertyGroup>");
            writer.WriteLine("    <TargetFramework>netstandard2.0</TargetFramework>");
            writer.WriteLine($"    <PackageId>{packageId}</PackageId>");
            writer.WriteLine("  </PropertyGroup>");
            writer.WriteLine("</Project>");
        }

        builder.SetProject(packageProject);

        var testEnv = await builder.BuildAsync()
                                   .ConfigureAwait(false);
        try
        {
            var runner = new DotNetRunner("pack", "-o", testEnv.TempDirectory);

            var executionResult = await runner.ExecuteAsync(testEnv)
                                              .ConfigureAwait(false);

            if(executionResult.Succeeded == false)
            {
                executionResult.WriteStreamToConsole();

                Assert.Fail("The dotnet pack command failed.");
            }

            return new TestCasePackage(testEnv, packageId);
        }
        catch
        {
            testEnv.Dispose();
            throw;
        }
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeTestSubject()
    {
        _testSubject = new();
    }

    private async Task<ExecutionResult<DotNetExecutionContext>> RunDotNetBuildAsync()
    {
        var runner = new DotNetRunner("build");
        using var testEnv = await _testSubject!.BuildAsync()
                                               .ConfigureAwait(false);

        var buildResult = await runner.ExecuteAsync(testEnv)
                                      .ConfigureAwait(false);

        executionResult = buildResult;

        return buildResult;
    }

    private static string ExecutingAssemblyDir => _executingAssemblyDir.Value;

    private static IEnumerable<TestCaseData> RelativePackageSources
    {
        get
        {
            yield return new TestCaseData("../NuGet");
            yield return new TestCaseData("");
            yield return new TestCaseData("NuGet");
        }
    }

    private const string NuGetError_UnableToFindPackage = "NU1101";
    private const string NuGetError_UnableToResolvePackage = "NU1100";
    private const string PackageIdJsonDotNet = "Newtonsoft.Json";

    private static readonly Lazy<string> _executingAssemblyDir = new Lazy<string>(() => GetExecutingAssemblyDir());

    private TestEnvironmentBuilder? _testSubject;
    private ExecutionResult? executionResult;
}
