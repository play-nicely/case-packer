﻿using System.Diagnostics.CodeAnalysis;
using Moq;

namespace PlayNicely.Tests;

[TestFixture]
public class ProcessRunnerTests
{
    [Test]
    public void CancelledCommandThrowsException()
    {
        using var cancelSource = ExecuteCancellableCommand(out var executionResult);

        cancelSource.Cancel();

        Assert.That(async () => await executionResult.ConfigureAwait(false), Throws.InstanceOf<OperationCanceledException>());
    }

    [Test]
    public async Task FailedCommandIncludesStandardErrorContent()
    {
        var executionResult = await ExecuteFailureCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.StandardError, Is.Not.Empty);
    }

    [Test]
    public async Task FailingCommandReturnsNonZeroExitCode()
    {
        var executionResult = await ExecuteFailureCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.ExitCode, Is.Not.Zero);
    }

    [Test]
    public async Task NonZeroExitCodeDoesNotSucceed()
    {
        var executionResult = await ExecuteFailureCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.Succeeded, Is.False);
    }

    [Test]
    public async Task SucceedingCommandIncludesStandardOutputContent()
    {
        var executionResult = await ExecuteSuccessfulCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.StandardOutput, Is.Not.Empty);
    }

    [Test]
    public async Task SucceedingCommandReturnsZeroExitCode()
    {
        var executionResult = await ExecuteSuccessfulCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.ExitCode, Is.Zero);
    }

    [Test]
    public void TimedoutCommandThrowsException()
    {
        using var cancelSource = ExecuteCancellableCommand(1, out var executionResult);

        Assert.That(async () => await executionResult.ConfigureAwait(false), Throws.InstanceOf<TimeoutException>());
    }

    [Test]
    public async Task ZeroExitCodeIsConsideredSuccessfulCommand()
    {
        var executionResult = await ExecuteSuccessfulCommandAsync(default).ConfigureAwait(false);

        Assert.That(executionResult.Succeeded, Is.True);
    }

    [SetUp]
    protected void BeforeEachTest()
    {
        _testEnvMock = new Mock<ITestEnvironment>();
        _testEnvDouble = _testEnvMock.Object;
    }

    private CancellationTokenSource ExecuteCancellableCommand(out Task<ExecutionResult> executionTask)
    {
        return ExecuteCancellableCommand(null, out executionTask);
    }

    private CancellationTokenSource ExecuteCancellableCommand(int? timeoutMilliseconds, out Task<ExecutionResult> executionTask)
    {
        var cancelSource = new CancellationTokenSource();
        try
        {
            InitializeTestSubject("--watch", ".");

            if(timeoutMilliseconds.HasValue)
                _testSubject.RunTimeout = TimeSpan.FromMilliseconds(timeoutMilliseconds.Value);

            executionTask = _testSubject.ExecuteAsync(_testEnvDouble, cancelSource.Token);

            return cancelSource;
        }
        catch
        {
            cancelSource.Dispose();
            throw;
        }
    }

    private Task<ExecutionResult> ExecuteFailureCommandAsync(CancellationToken cancel)
    {
        InitializeTestSubject("blah-blah");

        return _testSubject.ExecuteAsync(_testEnvDouble, cancel);
    }

    private Task<ExecutionResult> ExecuteSuccessfulCommandAsync(CancellationToken cancel)
    {
        InitializeTestSubject("--version");

        return _testSubject.ExecuteAsync(_testEnvDouble, cancel);
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeTestSubject(params string[] args)
    {
        const string nodeJs = "node";

        var envVars = new EnvVars();
        var commandsInfo = envVars.GetCommandInfo(nodeJs);
        var commandInfo = commandsInfo.FirstOrDefault();

        if(commandInfo is null)
            Assert.Inconclusive($"Command {nodeJs} is unavailable on this system");

        _testSubject = new ProcessRunner(commandInfo.FileName, args);
    }

    [NotNull]
    private ITestEnvironment? _testEnvDouble;

    [NotNull]
    private Mock<ITestEnvironment>? _testEnvMock;

    private ProcessRunner? _testSubject;
}
