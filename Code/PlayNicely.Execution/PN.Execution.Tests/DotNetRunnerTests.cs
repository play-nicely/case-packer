﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using PlayNicely.Projects;
using PlayNicely.Executor.DotNet;

namespace PlayNicely.Tests;

[TestFixture]
public class DotNetRunnerTests
{
    [Test]
    public async Task DotNetBuildWithNoErrorsSucceeds()
    {
        InitializeBuildTestSubject();

        using var env = await CreateTestEnvironmentAsync().ConfigureAwait(false);

        var buildResult = await _testSubject.ExecuteAsync(env)
                                            .ConfigureAwait(false);

        Assert.That(buildResult.Succeeded);
    }

    [Test]
    public async Task FailedBuildRecordsWhichErrors()
    {
        InitializeBuildTestSubject();

        using var env = await CreateFailingEnvironmentAsync().ConfigureAwait(false);

        var buildResult = await _testSubject.ExecuteAsync(env)
                                            .ConfigureAwait(false);

        try
        {
            Assert.That(buildResult.Context?.Errors.Any(), Is.True);
        }
        catch
        {
            TestContext.Out.WriteLine(string.Join("\n", buildResult.Context?
                                                                   .Errors
                                                                   .Select(err => $"{err.Code}: {err.Message}") ?? []));
            throw;
        }
    }

    [Test]
    public async Task FailedBuildHasStandardOutput()
    {
        InitializeBuildTestSubject();

        using var env = await CreateFailingEnvironmentAsync().ConfigureAwait(false);

        var buildResult = await _testSubject.ExecuteAsync(env)
                                            .ConfigureAwait(false);

        try
        {
            Assert.That(buildResult.StandardOutput, Is.Not.Empty);
        }
        catch
        {
            TestContext.Out.WriteLine(buildResult.StandardOutput);
            throw;
        }
    }

    [Test]
    public async Task SuccessfulBuildRecordsWhichProjectsWereBuilt()
    {
        InitializeBuildTestSubject();

        using var env = await CreateTestEnvironmentAsync().ConfigureAwait(false);

        var buildResult = await _testSubject.ExecuteAsync(env)
                                            .ConfigureAwait(false);

        try
        {
            Assert.That(buildResult.Context?.Projects.Any(), Is.True);
        }
        catch
        {
            TestContext.Out.WriteLine(string.Join("\n", buildResult.Context?
                                                                   .Projects
                                                                   .Select(proj => $"{proj.Key}: {proj.Value.ProjectPath}") ?? []));
        }
    }

    [Test]
    public async Task SuccessfulBuildRecordsWhichTargetsExecuted()
    {
        InitializeBuildTestSubject();

        using var env = await CreateTestEnvironmentAsync().ConfigureAwait(false);

        var buildResult = await _testSubject.ExecuteAsync(env)
                                            .ConfigureAwait(false);

        try
        {
            Assert.That(buildResult.Context?.Targets.Any(), Is.True);
        }
        catch
        {
            TestContext.Out.WriteLine(string.Join("\n", buildResult.Context?
                                                                   .Targets
                                                                   .Select(kvp => $"{kvp.Key}: {kvp.Value}") ?? []));
            throw;
        }
    }

    private static Task<ITestEnvironment> CreateFailingEnvironmentAsync([CallerMemberName] string? projectName = null,
                                                                        CancellationToken cancel = default)
    {
        return CreateTestEnvironmentAsync(project =>
        {
            var codeFile = project.Root.AddFile("program.cs");

            using var writer = new StreamWriter(codeFile.OpenWriteStream());

            writer.WriteLine("Console.WriteLine(\"Hello World\") // Note: no semi-colon");
        }, projectName, cancel);
    }

    private static TestCaseProject CreateProject(Action<TestCaseProject>? configure = null, [CallerMemberName] string? projectName = null)
    {
        var testCaseProject = new TestCaseProject(projectName!);
        var projectFile = testCaseProject.Root.AddFile($"{projectName}.csproj");

        testCaseProject.ProjectFile = projectFile;

        using(var writer = new StreamWriter(projectFile.OpenWriteStream()))
        {
            writer.WriteLine("<Project Sdk=\"Microsoft.NET.Sdk\">");
            writer.WriteLine("  <PropertyGroup>");
            writer.WriteLine("    <TargetFramework>netstandard2.0</TargetFramework>");
            writer.WriteLine("  </PropertyGroup>");
            writer.WriteLine("</Project>");
        }

        configure?.Invoke(testCaseProject);

        return testCaseProject;
    }

    private static async Task<ITestEnvironment> CreateTestEnvironmentAsync(Action<TestCaseProject>? configure = null,
                                                                           [CallerMemberName] string? projectName = null,
                                                                           CancellationToken cancel = default)
    {
        SimpleTestEnvironment? env = null;
        try
        {
            var testCaseProject = CreateProject(configure, projectName);

            env = new()
            {
                ProjectFilename = testCaseProject.ProjectFile?.FullName,
            };

            var fileSystem = new FileSystemPackageWriter(env.ProjectDirectory);

            await testCaseProject.SaveToAsync(fileSystem, cancel)
                                 .ConfigureAwait(false);
        }
        catch
        {
            env?.Dispose();
            throw;
        }

        return env;
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeBuildTestSubject()
    {
        InitializeTestSubject("build");
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeTestSubject(string subCommand, params string[] args)
    {
        _testSubject = new(subCommand, args);
    }

    private DotNetRunner? _testSubject;
}
