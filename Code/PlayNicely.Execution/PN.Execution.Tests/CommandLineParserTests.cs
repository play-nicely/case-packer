﻿namespace PlayNicely.Tests;

[TestFixture]
public class CommandLineParserTests
{
    [Test]
    public void InvalidEscapeCharThrowsException()
    {
        Assert.That(() => _testSubject.Parse("hello\\xworld"), Throws.ArgumentException);
    }

    [Test]
    [TestCaseSource(typeof(CommandLineParserTests), nameof(ParseArgumentTestCases))]
    public void ParseArguments(string commandLine, string expectedCommand, IEnumerable<string> expectedArgs)
    {
        var cmdAndArgs = _testSubject.Parse(commandLine);

        Assert.Multiple(() =>
        {
            Assert.That(cmdAndArgs.Command, Is.EqualTo(expectedCommand));
            Assert.That(cmdAndArgs.Arguments, Is.EquivalentTo(expectedArgs));
        });
    }

    [Test]
    [TestCaseSource(typeof(CommandLineParserTests), nameof(ParseSingleCommandTestCases))]
    public void ParseSingleCommand(string commandLine, string expectedCommand)
    {
        var cmdAndArgs = _testSubject.Parse(commandLine);

        Assert.That(cmdAndArgs.Command, Is.EqualTo(expectedCommand));
    }

    [Test]
    [TestCaseSource(typeof(CommandLineParserTests), nameof(ParseUnmatchedQuotes))]
    public void UnmatchedQuoteThrowsException(string commandLine)
    {
        Assert.That(() => _testSubject.Parse(commandLine), Throws.ArgumentException);
    }

    private static IEnumerable<TestCaseData> ParseArgumentTestCases
    {
        get
        {
            // basic
            yield return new TestCaseData("cmd arg1 arg2", "cmd", new[] { "arg1", "arg2", });
            yield return new TestCaseData("cmd \"arg1\" arg2", "cmd", new[] { "arg1", "arg2", });
            yield return new TestCaseData("cmd arg1 \"arg2\"", "cmd", new[] { "arg1", "arg2", });

            // basic double quotes
            yield return new TestCaseData("cmd \"arg1=x\" arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg1=\"x\" arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd \"arg1\"=x arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg\"1\"=x arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg1\"\"=x arg2", "cmd", new[] { "arg1=x", "arg2", });

            // basic single quotes
            yield return new TestCaseData("cmd \'arg1=x\' arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg1=\'x\' arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd \'arg1\'=x arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg\'1\'=x arg2", "cmd", new[] { "arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg1\'\'=x arg2", "cmd", new[] { "arg1=x", "arg2", });

            // basic spaces
            yield return new TestCaseData("cmd \\ arg1=x arg2", "cmd", new[] { " arg1=x", "arg2", });
            yield return new TestCaseData("cmd arg1=x\\  arg2", "cmd", new[] { "arg1=x ", "arg2", });
            yield return new TestCaseData("cmd arg1\\ =x arg2", "cmd", new[] { "arg1 =x", "arg2", });
            yield return new TestCaseData("cmd arg\\ 1\\ =x arg2", "cmd", new[] { "arg 1 =x", "arg2", });
            yield return new TestCaseData("cmd arg1\\ \\ =x arg2", "cmd", new[] { "arg1  =x", "arg2", });
        }
    }

    private static IEnumerable<TestCaseData> ParseSingleCommandTestCases
    {
        get
        {
            // Basic commands
            yield return new TestCaseData("hello", "hello");

            // Basic command with double quotes
            yield return new TestCaseData("\"fully_quoted\"", "fully_quoted");
            yield return new TestCaseData("mid\"dle_and_end\"", "middle_and_end");
            yield return new TestCaseData("mi\"dd\"le", "middle");
            yield return new TestCaseData("\"start_and_midd\"le", "start_and_middle");
            yield return new TestCaseData("middle\"\"empty", "middleempty");

            // Basic command with single quotes
            yield return new TestCaseData("\'fully_quoted\'", "fully_quoted");
            yield return new TestCaseData("mid\'dle_and_end\'", "middle_and_end");
            yield return new TestCaseData("mi\'dd\'le", "middle");
            yield return new TestCaseData("\'start_and_midd\'le", "start_and_middle");
            yield return new TestCaseData("middle\'\'empty", "middleempty");

            // Basic command with escaped space
            yield return new TestCaseData("\\ start", " start");
            yield return new TestCaseData("midd\\ le", "midd le");
            yield return new TestCaseData("end\\ ", "end ");
            yield return new TestCaseData("mi\\ dd\\ le", "mi dd le");
            yield return new TestCaseData("midd\\ \\ le", "midd  le");

            // Basic command with escaped double-quote
            yield return new TestCaseData("\\\"start", "\"start");
            yield return new TestCaseData("midd\\\"le", "midd\"le");
            yield return new TestCaseData("end\\\"", "end\"");
            yield return new TestCaseData("mi\\\"dd\\\"le", "mi\"dd\"le");
            yield return new TestCaseData("midd\\\"\\\"le", "midd\"\"le");

            // Basic command with escaped single-quote
            yield return new TestCaseData("\\\'start", "\'start");
            yield return new TestCaseData("midd\\\'le", "midd\'le");
            yield return new TestCaseData("end\\\'", "end\'");
            yield return new TestCaseData("mi\\\'dd\\\'le", "mi\'dd\'le");
            yield return new TestCaseData("midd\\\'\\\'le", "midd\'\'le");
        }
    }

    private static IEnumerable<TestCaseData> ParseUnmatchedQuotes
    {
        get
        {
            // Double quote in command
            yield return new TestCaseData("\"hello");
            yield return new TestCaseData("hello\"");
            yield return new TestCaseData("he\"llo");

            // single quote in command
            yield return new TestCaseData("\'hello");
            yield return new TestCaseData("hello\'");
            yield return new TestCaseData("he\'llo");

            // double quote in args
            yield return new TestCaseData("hello \"world");
            yield return new TestCaseData("hello world\"");
            yield return new TestCaseData("hello wo\"rld");

            // single quote in args
            yield return new TestCaseData("hello \'world");
            yield return new TestCaseData("hello world\'");
            yield return new TestCaseData("hello wo\'rld");
        }
    }

    private CommandLineParser _testSubject = CommandLineParser.Default;
}
