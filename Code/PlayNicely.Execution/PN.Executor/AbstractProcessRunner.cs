﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PlayNicely.Executor;

using static Projects.ProjectsConsts;

public abstract class AbstractProcessRunner<T> : ITestEnvironmentRunner<T> where T : ExecutionResult
{
    public async Task<T> ExecuteAsync(ITestEnvironment testEnv, CancellationToken cancel = default)
    {
        ArgumentNullException.ThrowIfNull(testEnv);
        cancel.ThrowIfCancellationRequested();

        TestEnvironment = testEnv;

        return await ExecuteProcessAsync(cancel).ConfigureAwait(false);
    }

    public IEnumerable<string> Arguments => _arguments;
    public string Command => _command;
    public TimeSpan RunTimeout { get; set; } = DefaultRunTimeout;

    public static readonly TimeSpan DefaultRunTimeout = TimeSpan.FromSeconds(60);

    protected AbstractProcessRunner(string command, params string[] arguments) : this(command, (IEnumerable<string>) arguments) { }

    protected AbstractProcessRunner(string command, IEnumerable<string>? arguments = null)
    {
        ArgumentException.ThrowIfNullOrEmpty(command);

        _command = command;
        _arguments = arguments ?? [];
    }

    protected virtual IEnumerable<string> AppendArguments() => [];
    protected abstract Task<T> CreateExecutionResultAsync(Process process, ExecutionResult baseResult, CancellationToken cancel);
    protected virtual IEnumerable<string> PrependArguments() => [];

    [AllowNull]
    protected ITestEnvironment TestEnvironment { get; private set; }

    private class StreamCollector
    {
        public void DataReceived(object sender, DataReceivedEventArgs e)
        {
            if(string.IsNullOrEmpty(e.Data))
                return;

            _data.Append(Environment.NewLine);
            _data.Append(e.Data);
        }

        public string Data => _data.ToString();
        private readonly StringBuilder _data = new();
    }

    private void ConfigureCommandArguments(Collection<string> startInfoArguments)
    {
        foreach(var arg in PrependArguments().Concat(_arguments).Concat(AppendArguments()))
            startInfoArguments.Add(arg);
    }

    private ProcessStartInfo CreateProcessStartInfo()
    {
        var startInfo = new ProcessStartInfo
        {
            ErrorDialog = false,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            StandardOutputEncoding = DefaultEncoding,
            StandardErrorEncoding = DefaultEncoding,
        };

        startInfo.FileName = _command;

        ConfigureCommandArguments(startInfo.ArgumentList);

        return startInfo;
    }

    private async Task<T> ExecuteProcessAsync(CancellationToken cancel)
    {
        var startInfo = CreateProcessStartInfo();
        var standardOutput = new StreamCollector();
        var standardError = new StreamCollector();
        var result = default(T);

        TestEnvironment.ReadyToStartInfo(startInfo);

        using(var process = new Process())
        {
            var executionState = -1;

            process.ErrorDataReceived += standardError.DataReceived;
            process.OutputDataReceived += standardOutput.DataReceived;

            process.StartInfo = startInfo;
            process.Start();

            process.BeginErrorReadLine();
            process.BeginOutputReadLine();

            using(var timeoutSource = new CancellationTokenSource(RunTimeout))
            using(var waitForExitSource = CancellationTokenSource.CreateLinkedTokenSource(timeoutSource.Token, cancel))
            {
                try
                {
                    while(process.HasExited == false)
                    {
                        waitForExitSource.Token.ThrowIfCancellationRequested();

                        await Task.Yield();
                    }

                    executionState = process.ExitCode;
                }
                catch(OperationCanceledException ex)
                {
                    if(timeoutSource.IsCancellationRequested)
                        throw new TimeoutException(string.Format(ContentRes.Error_ProcessRunner_TimedOut,
                                                                 startInfo.FileName,
                                                                 RunTimeout.TotalSeconds),
                                                   ex);
                }
            }

            cancel.ThrowIfCancellationRequested();
            var workingDirectory = string.IsNullOrEmpty(startInfo.WorkingDirectory)
                ? Environment.CurrentDirectory
                : startInfo.WorkingDirectory;
            var baseResult = new ExecutionResult(executionState, workingDirectory, standardOutput.Data, standardError.Data);

            cancel.ThrowIfCancellationRequested();
            result = await CreateExecutionResultAsync(process, baseResult, cancel).ConfigureAwait(false);
        }

        return result;
    }

    private readonly IEnumerable<string> _arguments;
    private readonly string _command;
}
