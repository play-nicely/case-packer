﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;
using NuGet.Configuration;
using PlayNicely.Projects;

namespace PlayNicely.Executor;

[DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
public partial class TestEnvironmentBuilder
{
    public TestEnvironmentBuilder()
    {
        _excludedCommands = new HashSet<string>();
        _requiredCommands = new HashSet<string>();
        _packageSources = new();
        _nuGetSettingsCommands = new List<Action<ISettings>>();
        _sourceMappings = new();
    }

    public TestEnvironmentBuilder AddPackageSource(string sourceLocation, string sourceName, string? baseDirectory = null)
    {
        ArgumentException.ThrowIfNullOrEmpty(sourceName);
        ArgumentNullException.ThrowIfNull(sourceLocation);

        _packageSources.Add(sourceName, sourceLocation);
        _nuGetSettingsCommands.Add(settings =>
        {
            var locationToAdd = sourceLocation;

            if(Uri.IsWellFormedUriString(locationToAdd, UriKind.Absolute) == false && Path.IsPathFullyQualified(locationToAdd) == false)
                locationToAdd = Path.GetFullPath(locationToAdd,
                                                 baseDirectory ?? GetCallingAssemblyDir(Assembly.GetCallingAssembly()));

            settings.AddOrUpdate(ConfigurationConstants.PackageSources, new AddItem(sourceName, locationToAdd));
        });

        return this;
    }

    public async Task<ITestEnvironment> BuildAsync(CancellationToken cancel = default)
    {
        cancel.ThrowIfCancellationRequested();
        ThrowIfInconsistentRequiredExcludedCommands();

        var testEnv = new TestEnvironment(_requiredCommands, _excludedCommands);

        cancel.ThrowIfCancellationRequested();
        if(Project is not null)
            await testEnv.WriteToProjectDirectoryAsync(Project, cancel)
                         .ConfigureAwait(false);

        if(_sourceMappings.Count > 0)
            CreateSourceMappingCommands();

        cancel.ThrowIfCancellationRequested();
        if(_nuGetSettingsCommands.Count > 0)
            await testEnv.ConfigureNuGetAsync(ConfigureNuGetAsync, cancel)
                         .ConfigureAwait(false);

        return testEnv;
    }

    public TestEnvironmentBuilder ClearPackageSources()
    {
        _nuGetSettingsCommands.Add(settings => settings.AddOrUpdate(ConfigurationConstants.PackageSources, new ClearItem()));

        return this;
    }

    public TestEnvironmentBuilder ExcludeCommandFromPath(string command)
    {
        _excludedCommands.Add(command);

        ThrowIfCommandIsRequired(command);

        return this;
    }

    public TestEnvironmentBuilder MapPackagesToSource(string sourceName, string packagePattern)
    {
        _sourceMappings.Add(new(packagePattern, _ => sourceName));

        return this;
    }

    public TestEnvironmentBuilder PackageSourceFallback(string sourceName)
    {
        return MapPackagesToSource(sourceName, MatchAllPackages);
    }

    public TestEnvironmentBuilder RequiredCommands(params string[] commands) => RequiredCommands((IEnumerable<string>) commands);

    public TestEnvironmentBuilder RequiredCommands(IEnumerable<string> commands)
    {
        foreach(var command in commands)
            _requiredCommands.Add(command);

        ThrowIfAnyCommandIsExcluded(commands);

        return this;
    }

    public TestEnvironmentBuilder SetProject(TestCaseProject project)
    {
        Project = project;

        return this;
    }

    public TestEnvironmentBuilder UseBestPackageSourceFallback()
    {
        _sourceMappings.Add(new(MatchAllPackages, settings =>
        {
            var sources = new[] { string.Empty, string.Empty, string.Empty, string.Empty };

            foreach(var packageSource in settings.GetPackageSources())
            {
                if(IsNuGetOrgSource(packageSource))
                    return packageSource.Name;
                else if(IsUnset(sources[0]) && IsNuGetOrgUrl(packageSource))
                    sources[0] = packageSource.Name;
                else if(IsUnset(sources[1]) && packageSource.IsHttps)
                    sources[1] = packageSource.Name;
                else if(IsUnset(sources[2]) && packageSource.IsHttp)
                    sources[2] = packageSource.Name;
                else if(IsUnset(sources[3]))
                    sources[3] = packageSource.Name;
            }

            return sources.FirstOrDefault(src => src != string.Empty)
                ?? throw new InvalidOperationException(ContentRes.Error_EnvBuilder_NoSuitableFallbackSources);
        }));

        return this;
    }

    public TestCaseProject? Project { get; set; }

    private readonly struct SourceMapping
    {
        public SourceMapping(string packagePattern, Func<ISettings, string> sourceSelector)
        {
            ArgumentException.ThrowIfNullOrEmpty(packagePattern);
            ArgumentNullException.ThrowIfNull(sourceSelector);

            SourceSelector = sourceSelector;
            PackagePattern = packagePattern;
        }

        public string PackagePattern { get; }
        public Func<ISettings, string> SourceSelector { get; }
    }

    private static string GetCallingAssemblyDir(Assembly assembly)
    {
        var callingAssemblyDir = Path.GetDirectoryName(assembly.Location);

        if(callingAssemblyDir is null)
            throw new InvalidOperationException(string.Format(ContentRes.Error_EnvBuilder_NoCallingAssemblyDirectory, assembly.FullName));

        return callingAssemblyDir;
    }

    private static bool IsNuGetOrgSource(PackageSource source) => source.Name == SourceNuGetDotOrg;
    private static bool IsNuGetOrgUrl(PackageSource source) => PatternWebsiteNuGetOrg.IsMatch(source.Source);
    private static bool IsUnset(string sourceName) => sourceName == string.Empty;

    private Task ConfigureNuGetAsync(ISettings settings, CancellationToken cancel)
    {
        foreach(var command in _nuGetSettingsCommands)
        {
            cancel.ThrowIfCancellationRequested();
            command(settings);
        }

        return Task.CompletedTask;
    }

    private void CreateSourceMappingCommands()
    {
        foreach(var mapping in _sourceMappings)
        {
            _nuGetSettingsCommands.Add(settings =>
            {
                var sourceName = mapping.SourceSelector(settings);
                var mappingProvider = new PackageSourceMappingProvider(settings, true);

                var sourceItem = mappingProvider.GetPackageSourceMappingItems()
                                                .FirstOrDefault(item => item.Key == sourceName);

                if(sourceItem is null)
                    sourceItem = new PackageSourceMappingSourceItem(sourceName, new[] { new PackagePatternItem(mapping.PackagePattern) });
                else
                    sourceItem.Patterns.Add(new PackagePatternItem(mapping.PackagePattern));

                settings.AddOrUpdate(ConfigurationConstants.PackageSourceMapping, sourceItem);
            });
        }
    }

    private void ThrowIfAnyCommandIsExcluded(IEnumerable<string> commands)
    {
        var invalidCommands = string.Join(',', _excludedCommands.Intersect(commands));

        if(string.IsNullOrEmpty(invalidCommands) == false)
            throw new InvalidOperationException(string.Format(ContentRes.Error_EnvBuilder_CannotExcludeRequiredCommands, invalidCommands));
    }

    private void ThrowIfCommandIsRequired(string command)
    {
        if(_requiredCommands.Contains(command))
            throw new InvalidOperationException(string.Format(ContentRes.Error_EnvBuilder_CommandIsRequired, command));
    }

    private void ThrowIfInconsistentRequiredExcludedCommands()
    {
        var invalidCommands = string.Join(',', _requiredCommands.Intersect(_excludedCommands));

        if(string.IsNullOrEmpty(invalidCommands) == false)
            throw new InvalidOperationException(string.Format(ContentRes.Error_EnvBuilder_InconsistentRequiredExcludedCommands,
                                                              invalidCommands));
    }

    private static Regex PatternWebsiteNuGetOrg => _patternWebsiteNuGetOrg.Value;
    private string DebuggerDisplay => Project?.ProjectPath ?? GetType().FullName ?? nameof(TestEnvironmentBuilder);

    private const string MatchAllPackages = "*";
    private const string SourceNuGetDotOrg = "nuget.org";

    private static readonly Lazy<Regex> _patternWebsiteNuGetOrg = new(() => new Regex("(?ns-imx:^https?://(.+\\.)?nuget\\.org(/|$))"));
    private static readonly IList<string> PackagePatternsPlaceholder = [];

    private readonly HashSet<string> _excludedCommands;
    private readonly List<Action<ISettings>> _nuGetSettingsCommands;
    private readonly Dictionary<string, string> _packageSources;
    private readonly HashSet<string> _requiredCommands;
    private readonly List<SourceMapping> _sourceMappings;
}
