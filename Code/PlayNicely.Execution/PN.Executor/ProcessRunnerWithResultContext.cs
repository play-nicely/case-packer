﻿using System.Diagnostics;

namespace PlayNicely.Executor;

public abstract class ProcessRunnerWithResultContext<TContext> : AbstractProcessRunner<ExecutionResult<TContext>>
{
    protected ProcessRunnerWithResultContext(string command, params string[] arguments) : base(command, arguments) { }
    protected ProcessRunnerWithResultContext(string command, IEnumerable<string>? arguments = null) : base(command, arguments) { }

    protected override async Task<ExecutionResult<TContext>> CreateExecutionResultAsync(Process process,
                                                                                        ExecutionResult baseResult,
                                                                                        CancellationToken cancel)
    {
        cancel.ThrowIfCancellationRequested();

        var context = await RetreiveExecutionContextAsync(process, cancel).ConfigureAwait(false);

        if(context is null)
            throw new InvalidExecutionException(string.Format(ContentRes.Error_ProcessRunner_NoContext,
                                                              process.ProcessName,
                                                              typeof(TContext).Name));

        return baseResult.Extend(context);
    }

    protected abstract Task<TContext> RetreiveExecutionContextAsync(Process process, CancellationToken cancel);
}
