﻿using System.Diagnostics;

namespace PlayNicely.Executor;

public interface ITestEnvironment : IDisposable
{
    void ReadyToStartInfo(ProcessStartInfo startInfo);

    string ProjectDirectory { get; }
    string? ProjectFilename { get; }
    string TempDirectory { get; }
}
