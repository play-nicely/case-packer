﻿namespace PlayNicely.Executor;

public class SimpleTestEnvironment : FileSystemTestEnvironment
{
    public List<string>? PackageSources { get; set; }

    public new string? ProjectFilename
    {
        get => ProjectFileRelativePath;
        set => ProjectFileRelativePath = value;
    }
}
