﻿using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;

namespace PlayNicely.Executor;

public class CommandLineParser
{
    public readonly struct CommandAndArguments
    {
        public CommandAndArguments(string command, IEnumerable<string>? arguments) : this()
        {
            ArgumentException.ThrowIfNullOrEmpty(command);

            Command = command;
            Arguments = arguments ?? [];
        }

        public IEnumerable<string> Arguments { get; }
        public string Command { get; }
    }

    public CommandLineParser(char escapeChar, IEnumerable<char>? quoteChars) : this(escapeChar, quoteChars?.ToArray()) { }

    public CommandLineParser(char escapeChar, params char[]? quoteChars)
    {
        EscapeChar = escapeChar;
        QuoteChars = quoteChars is null ? Array.Empty<char>() : Array.AsReadOnly(quoteChars);

        _parserPattern = new(MakeParserPattern);
    }

    public CommandAndArguments Parse(string commandLine)
    {
        var tokens = ParseCommandLine(commandLine).ToArray();

        return new(tokens[0], tokens[1..]);
    }

    public static CommandLineParser Default => Linux;
    public static CommandLineParser Linux => _linux.Value;

    public char EscapeChar { get; }
    public IList<char> QuoteChars { get; }

    protected virtual IEnumerable<string> ParseCommandLine(string commandLine)
    {
        var context = new ParseContext(commandLine, ParserPattern);

        while(context.CurrentToken != CommandLineToken.None)
        {
            context.AppendUpToToken();

            if(context.CurrentToken == CommandLineToken.Space)
            {
                yield return context.ResolveArgument();
            }
            else if(context.CurrentToken == CommandLineToken.Escape)
            {
                context.ZeroWidthToken();
                context.NextToken();

                if(context.CurrentToken == CommandLineToken.None)
                    throw new ArgumentException(string.Format(ContentRes.Error_Parser_InvalidCharacterEscape,
                                                              commandLine,
                                                              EscapeChar,
                                                              context.CurrentPosition),
                                                nameof(commandLine));

                context.AppendToken();
            }
            else
            {
                ParseWithinQuotes(context);
            }

            context.NextToken();
        }

        yield return context.ResolveArgument();
    }

    private enum CommandLineToken
    {
        None,
        Space,
        Escape,
        Quote,
    }

    private class ParseContext
    {
        public ParseContext(string commandLine, Regex parserPattern)
        {
            ArgumentException.ThrowIfNullOrEmpty(commandLine);

            ArgCommandLine = nameof(commandLine);

            CommandLine = commandLine;
            _currentPosition = 0;
            _currentArgument = new();
            _currentToken = parserPattern.Match(commandLine);
        }

        public void AppendToken()
        {
            _currentArgument.Append(_currentToken.Value);
            _currentPosition += _currentToken.Length;
        }

        public void AppendUpToToken()
        {
            _currentArgument.Append(CommandLine[_currentPosition.._currentToken.Index]);
            _currentPosition = _currentToken.Index;
        }

        [MemberNotNull(nameof(QuoteGroupId))]
        public void EnterQuotes()
        {
            var quoteGroup = _currentToken.Groups
                                          .Skip<Group>(1)
                                          .FirstOrDefault(group => group.Success);

            if(quoteGroup is null)
                throw new ArgumentException(string.Format(ContentRes.Error_Parser_InvalidQuoteContext,
                                                          CommandLine,
                                                          CurrentPosition),
                                            ArgCommandLine);

            QuoteGroupId = quoteGroup.Name;
            _currentPosition += _currentToken.Length;
        }

        public void NextToken()
        {
            _currentToken = _currentToken.NextMatch();
        }

        public string ResolveArgument()
        {
            if(_currentToken.Success)
                return ResolveCurrentArgument();
            else
                return ResolveFinalArgument();
        }

        public bool TryExitQuotes()
        {
            var isCurrentQuotes = IsCurrentTokenCurrentQuotes;

            if(isCurrentQuotes)
            {
                QuoteGroupId = null;
                _currentPosition += _currentToken.Length;
            }

            return isCurrentQuotes;
        }

        public void ZeroWidthToken()
        {
            _currentPosition += _currentToken.Length;
        }

        public string CommandLine { get; }
        public int CurrentPosition => _currentPosition;

        public CommandLineToken CurrentToken
        {
            get
            {
                if(_currentToken.Success == false)
                    return CommandLineToken.None;
                else if(_currentToken.Groups[GroupSpace].Success)
                    return CommandLineToken.Space;
                else if(_currentToken.Groups[GroupEscape].Success)
                    return CommandLineToken.Escape;
                else
                    return CommandLineToken.Quote;
            }
        }

        public string? QuoteGroupId { get; private set; }

        internal readonly string ArgCommandLine;

        private string GetAndClearCurrentArgument()
        {
            var result = _currentArgument.ToString();

            _currentArgument.Clear();

            return result;
        }

        private string ResolveCurrentArgument()
        {
            _currentPosition += _currentToken.Length;

            return GetAndClearCurrentArgument();
        }

        private string ResolveFinalArgument()
        {
            if(_currentArgument.Length <= 0 && _currentPosition >= CommandLine.Length)
                return string.Empty;

            _currentArgument.Append(CommandLine[_currentPosition..]);
            _currentPosition = CommandLine.Length;

            return GetAndClearCurrentArgument();
        }

        private bool IsCurrentTokenCurrentQuotes
        {
            get
            {
                if(QuoteGroupId is null)
                    return false;

                return _currentToken.Groups[QuoteGroupId].Success;
            }
        }

        private readonly StringBuilder _currentArgument;
        private int _currentPosition;
        private Match _currentToken;
    }

    private static CommandLineParser MakeLinuxParser() => new CommandLineParser('\\', '"', '\'');

    private Regex MakeParserPattern()
    {
        var tokens = new List<string>(10);

        tokens.Add($"(?<{GroupEscape}>{Regex.Escape(EscapeChar.ToString())})");
        tokens.Add($"(?<{GroupSpace}>\\s)");

        var quoteIndex = 1;
        foreach(var quoteChar in QuoteChars)
            tokens.Add($"(?<{GroupQuote}{quoteIndex++}>{Regex.Escape(quoteChar.ToString())})");

        return new Regex($"(?ins-mx:{string.Join('|', tokens)})");
    }

    private void ParseWithinQuotes(ParseContext context)
    {
        context.EnterQuotes();
        context.NextToken();

        while(context.CurrentToken != CommandLineToken.None)
        {
            context.AppendUpToToken();

            if(context.CurrentToken == CommandLineToken.Space || context.CurrentToken == CommandLineToken.Escape)
            {
                context.AppendToken();
            }
            else if(context.CurrentToken != CommandLineToken.None)
            {
                if(context.TryExitQuotes())
                    return;

                context.AppendToken();
            }

            context.NextToken();
        }

        throw new ArgumentException(string.Format(ContentRes.Error_Parser_UnmatchedQuoteCharacter,
                                                  context.CommandLine,
                                                  context.CurrentPosition),
                                    context.ArgCommandLine);
    }

    private Regex ParserPattern => _parserPattern.Value;

    private const string GroupEscape = "escape";
    private const string GroupQuote = "quote";
    private const string GroupSpace = "space";

    private static readonly Lazy<CommandLineParser> _linux = new(() => MakeLinuxParser());

    private readonly Lazy<Regex> _parserPattern;
}
