﻿using System.Diagnostics;

namespace PlayNicely.Executor;

[DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
public class ExecutionResult
{
    public ExecutionResult(int exitCode, string workingDirectory, string standardOutput, string standardError)
    {
        ArgumentException.ThrowIfNullOrEmpty(workingDirectory);
        ArgumentNullException.ThrowIfNull(standardError);
        ArgumentNullException.ThrowIfNull(standardOutput);

        WorkingDirectory = workingDirectory;
        ExitCode = exitCode;
        StandardOutput = standardOutput;
        StandardError = standardError;
    }

    public static ExecutionResult<TContext> Create<TContext>(int exitCode,
                                                             string workingDirectory,
                                                             string standardOutput,
                                                             string standardError,
                                                             TContext context)
    {
        return new ExecutionResult<TContext>(exitCode, workingDirectory, standardOutput, standardError, context);
    }

    public ExecutionResult<TContext> Extend<TContext>(TContext context)
    {
        return new ExecutionResult<TContext>(this, context);
    }

    public int ExitCode { get; }
    public string StandardError { get; }
    public string StandardOutput { get; }
    public bool Succeeded => ExitCode == 0;
    public string WorkingDirectory { get; }

    private string DebuggerDisplay => $"Execution {this.GetStatusText()}";
}

public class ExecutionResult<TContext> : ExecutionResult
{
    public ExecutionResult(int exitCode,
                           string workingDirectory,
                           string standardOutput,
                           string standardError,
                           TContext context) : base(exitCode, workingDirectory, standardOutput, standardError)
    {
        ArgumentNullException.ThrowIfNull(context);

        Context = context;
    }

    public ExecutionResult(ExecutionResult source, TContext context) : this(source?.ExitCode ?? 0,
                                                                            source?.WorkingDirectory ?? ".",
                                                                            source?.StandardOutput ?? string.Empty,
                                                                            source?.StandardError ?? string.Empty,
                                                                            context)
    {
        ArgumentNullException.ThrowIfNull(source);
    }

    public TContext Context { get; }
}
