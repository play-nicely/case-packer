﻿using PlayNicely.Projects;

namespace PlayNicely.Executor;

[Serializable]
public class InvalidEnvironmentException : PlayNicelyException
{
    public InvalidEnvironmentException() : base() { }
    public InvalidEnvironmentException(string? message) : base(message) { }
    public InvalidEnvironmentException(string? message, Exception? innerException) : base(message, innerException) { }
}
