﻿using System.Diagnostics;

namespace PlayNicely.Executor;

public class ProcessRunner : AbstractProcessRunner<ExecutionResult>
{
    public ProcessRunner(string command, params string[] args) : base(command, args) { }
    public ProcessRunner(string command, IEnumerable<string>? arguments = null) : base(command, arguments) { }

    public static ProcessRunner Parse(string commandLine, CommandLineParser? parser = null)
    {
        var nonNullParser = parser ?? CommandLineParser.Default;
        var commandAndArgs = nonNullParser.Parse(commandLine);

        return new ProcessRunner(commandAndArgs.Command, commandAndArgs.Arguments);
    }

    protected override Task<ExecutionResult> CreateExecutionResultAsync(Process process,
                                                                        ExecutionResult baseResult,
                                                                        CancellationToken cancel)
    {
        return Task.FromResult(baseResult);
    }
}
