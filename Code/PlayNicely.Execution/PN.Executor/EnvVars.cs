﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace PlayNicely.Executor;

using IOPath = System.IO.Path;

public class EnvVars : IDictionary<string, string>
{
    public interface IPath
    {
        void Prepend(string path);

        IEnumerable<string> Items { get; set; }
    }

    public class CommandInfo
    {
        public CommandInfo(string command, string directory, string fileName)
        {
            ArgumentException.ThrowIfNullOrEmpty(command);
            ArgumentException.ThrowIfNullOrEmpty(directory);
            ArgumentException.ThrowIfNullOrEmpty(fileName);

            Command = command;
            FullPath = IOPath.Join(directory, fileName);
            Directory = directory;
            FileName = fileName;
        }

        public string Command { get; }
        public string Directory { get; }
        public string FileName { get; }
        public string FullPath { get; }

        internal bool IsAvailable => File.Exists(FullPath);
    }

    public EnvVars(IDictionary<string, string>? envVars = null)
    {
        _envVars = envVars ?? CopyEnvironmentVariables();

        Path = new PathVariable(this);
    }

    public void Add(string key, string value) => _envVars.Add(key, value);
    public void Add(KeyValuePair<string, string> item) => _envVars.Add(item);
    public void Clear() => _envVars.Clear();
    public bool Contains(KeyValuePair<string, string> item) => _envVars.Contains(item);
    public bool ContainsKey(string key) => _envVars.ContainsKey(key);
    public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex) => _envVars.CopyTo(array, arrayIndex);
    public IEnumerable<CommandInfo> GetCommandInfo(params string[] commands) => GetCommandInfo((IEnumerable<string>) commands);

    public IEnumerable<CommandInfo> GetCommandInfo(IEnumerable<string> commands)
    {
        var executableExtensions = GetPathExt();
        var paths = GetPath();

        return commands.SelectMany(command => paths.SelectMany(path => executableExtensions.Select(ext => new CommandInfo(command, path, string.Concat(command, ext)))))
                       .Where(cmdInfo => cmdInfo.IsAvailable);
    }

    public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => _envVars.GetEnumerator();
    public bool Remove(string key) => _envVars.Remove(key);
    public bool Remove(KeyValuePair<string, string> item) => _envVars.Remove(item);
    public bool TryGetValue(string key, [MaybeNullWhen(false)] out string value) => _envVars.TryGetValue(key, out value);

    IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) _envVars).GetEnumerator();

    public int Count => _envVars.Count;
    public bool IsReadOnly => _envVars.IsReadOnly;
    public ICollection<string> Keys => _envVars.Keys;
    public IPath Path { get; }
    public string this[string key] { get => _envVars[key]; set => _envVars[key] = value; }
    public ICollection<string> Values => _envVars.Values;

    private static class Separators
    {
        public const char PathExt = ';';
        public static readonly char Path = IOPath.PathSeparator;
    }

    private static class Variables
    {
        public const string Path = "PATH";
        public const string PathExt = "PATHEXT";
    }

    private class PathVariable : IPath
    {
        public PathVariable(EnvVars owner)
        {
            _owner = owner;
        }

        public void Prepend(string path)
        {
            _owner.SetPath(_owner.GetPath()
                                 .Prepend(path));
        }

        public IEnumerable<string> Items
        {
            get => _owner.GetPath();
            set => _owner.SetPath(value);
        }

        private readonly EnvVars _owner;
    }

    private static IDictionary<string, string> CopyEnvironmentVariables()
    {
        var currentVariables = Environment.GetEnvironmentVariables();
        var result = new Dictionary<string, string>(currentVariables.Count, StringComparer.InvariantCultureIgnoreCase);

        foreach(string key in currentVariables.Keys)
        {
            if((currentVariables[key] is string value) == false)
                continue;

            result.Add(key, value);
        }

        return result;
    }

    private IEnumerable<string> GetPath()
    {
        return _envVars.TryGetValue(Variables.Path, out var path)
            ? path.Split(Separators.Path, StringSplitOptions.RemoveEmptyEntries)
            : [];
    }

    private IEnumerable<string> GetPathExt()
    {
        return _envVars.TryGetValue(Variables.PathExt, out var pathExt)
            ? pathExt.Split(Separators.PathExt, StringSplitOptions.RemoveEmptyEntries)
            : FallbackPathExt;
    }

    private void SetPath(IEnumerable<string> newPaths)
    {
        var newPath = string.Join(Separators.Path, newPaths);

        if(_envVars.TryAdd(Variables.Path, newPath) == false)
            _envVars[Variables.Path] = newPath;
    }

    private static readonly IEnumerable<string> FallbackPathExt = Array.AsReadOnly(new[] { string.Empty, });

    private readonly IDictionary<string, string> _envVars;
}
