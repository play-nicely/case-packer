﻿using PlayNicely.Projects;

namespace PlayNicely.Executor;

[Serializable]
public class InvalidExecutionException : PlayNicelyException
{
    public InvalidExecutionException() : base() { }
    public InvalidExecutionException(string? message) : base(message) { }
    public InvalidExecutionException(string? message, Exception? innerException) : base(message, innerException) { }
}
