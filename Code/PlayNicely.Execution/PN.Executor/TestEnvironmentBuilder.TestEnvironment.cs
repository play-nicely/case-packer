﻿using System.Diagnostics;
using System.Text;
using NuGet.Configuration;
using PlayNicely.Projects;

namespace PlayNicely.Executor;

partial class TestEnvironmentBuilder
{
    private class TestEnvironment : FileSystemTestEnvironment
    {
        public TestEnvironment(IEnumerable<string> requiredCommands, IEnumerable<string> excludedCommands)
        {
            ArgumentNullException.ThrowIfNull(requiredCommands);
            ArgumentNullException.ThrowIfNull(excludedCommands);

            _envVars = new();
            _binDirectory = new("bin");

            RequiredCommands(requiredCommands);
            ExcludeCommandsFromPath(excludedCommands);
            AddRequiredCommandsToPath();
        }

        public void CleanProjectDirectory() => CleanDirectory(ProjectDirectory);

        public async Task ConfigureNuGetAsync(Func<ISettings, CancellationToken, Task> configure, CancellationToken cancel = default)
        {
            var settings = new Settings(ProjectDirectory);

            await configure(settings, cancel).ConfigureAwait(false);

            settings.SaveToDisk();
        }

        public override void ReadyToStartInfo(ProcessStartInfo startInfo)
        {
            base.ReadyToStartInfo(startInfo);
            ReplaceVariablesWith(startInfo.Environment);
        }

        public async Task WriteToProjectDirectoryAsync(TestCaseProject? project, CancellationToken cancel = default)
        {
            ThrowIfDisposed();
            cancel.ThrowIfCancellationRequested();

            ProjectFileRelativePath = project?.ProjectPath;
            if(project is null)
                return;

            var blobWriter = new FileSystemPackageWriter(ProjectDirectory);

            await project.SaveToAsync(blobWriter, cancel)
                         .ConfigureAwait(false);
        }

        protected override void DisposeManaged()
        {
            _binDirectory.Dispose();
            base.DisposeManaged();
        }

        private class EncodedStringWriter : StringWriter
        {
            public EncodedStringWriter(Encoding encoding)
            {
                _encoding = encoding;
            }

            public override Encoding Encoding => _encoding;

            private readonly Encoding _encoding;
        }

        private static void CleanDirectory(string fullPath)
        {
            foreach(var directory in Directory.EnumerateDirectories(fullPath))
                Directory.Delete(directory, true);

            foreach(var file in Directory.EnumerateFiles(fullPath))
                File.Delete(file);
        }

        private void AddRequiredCommandsToPath() => _envVars.Path.Prepend(_binDirectory.FullPath);

        private void ExcludeCommandsFromPath(IEnumerable<string> commands)
        {
            var foundOnPaths = _envVars.GetCommandInfo(commands)
                                       .Select(cmdInfo => cmdInfo.Directory);

            _envVars.Path.Items = _envVars.Path.Items.Except(foundOnPaths);
        }

        private void ReplaceVariablesWith(IDictionary<string, string?> environment)
        {
            environment.Clear();

            foreach(var keyValue in _envVars)
                environment.Add(keyValue.Key, keyValue.Value);
        }

        private void RequiredCommands(IEnumerable<string> commands)
        {
            var commandsInfo = _envVars.GetCommandInfo(commands)
                                       .DistinctBy(cmdInfo => cmdInfo.Command);
            var unavailableCommands = commands.Except(commandsInfo.Select(ci => ci.Command))
                                              .ToList();

            if(unavailableCommands.Count != 0)
                throw new InvalidEnvironmentException(string.Format(ContentRes.Error_TestEnv_RequiredCommandsNotFound,
                                                                    string.Join("\n  • ", unavailableCommands)));

            foreach(var commandInfo in commandsInfo)
                File.CreateSymbolicLink(Path.Join(_binDirectory.FullPath, commandInfo.FileName), commandInfo.FullPath);
        }

        private readonly TemporaryDirectory _binDirectory;
        private readonly EnvVars _envVars;
    }
}
