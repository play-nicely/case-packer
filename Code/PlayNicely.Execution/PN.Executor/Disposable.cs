﻿namespace PlayNicely.Executor;

internal static class Disposable
{
    public static bool IsDisposed(int disposed) => disposed == ObjectDisposed;

    public static bool RequiresDisposal(ref int disposed)
    {
        return Interlocked.Exchange(ref disposed, ObjectDisposed) != ObjectDisposed;
    }

    public const int ObjectDisposed = 1;
    public const int ObjectNotDisposed = ~ObjectDisposed;
}
