﻿using System.Text.Json;

namespace PlayNicely.Executor;

public static class ExecutionResultExtensions
{
    public static void WriteStreamToConsole(this ExecutionResult executionResult)
    {
        Console.WriteLine("{0}: {1}", ContentRes.Text_ExecutionResult_Succeeded, executionResult.Succeeded);
        Console.WriteLine("{0}: {1}", ContentRes.Text_ExecutionResult_ExitCode, executionResult.ExitCode);

        WriteAnyContentToConsole(executionResult.StandardOutput, ContentRes.Text_ExecutionResult_StandardOutput, Console.Out);
        WriteAnyContentToConsole(executionResult.StandardError, ContentRes.Text_ExecutionResult_StandardError, Console.Error);

        WriteExecutionContext(executionResult);
    }

    internal static string GetStatusText(this ExecutionResult result)
    {
        return StatusText[result.Succeeded];
    }

    private static void WriteAnyContentToConsole(string? content, string title, TextWriter writer)
    {
        if(string.IsNullOrEmpty(content))
            return;

        Console.WriteLine("{0}:", title);
        writer.WriteLine(content);
    }

    private static void WriteExecutionContext(ExecutionResult executionResult)
    {
        var resultType = executionResult.GetType();

        if(resultType.IsGenericType == false)
            return;

        var propInfo = resultType.GetProperty(nameof(ExecutionResult<object>.Context));
        var context = propInfo!.GetValue(executionResult);

        Console.WriteLine("{0}:", ContentRes.Text_ExecutionResult_Context);
        Console.WriteLine(JsonSerializer.Serialize(context));
    }

    private static IDictionary<bool, string> StatusText => _statusText.Value;

    private static readonly Lazy<IDictionary<bool, string>> _statusText = new(() => new Dictionary<bool, string>
    {
        [true] = ContentRes.Text_Succeeded,
        [false] = ContentRes.Text_Failed,
    });
}
