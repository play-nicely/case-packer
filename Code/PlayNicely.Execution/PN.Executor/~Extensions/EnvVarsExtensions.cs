﻿namespace PlayNicely.Executor;

public static class EnvVarsExtensions
{
    public static bool CommandExists(this EnvVars envVars, string command)
    {
        return envVars.GetCommandInfo(command)
                      .Any();
    }
}
