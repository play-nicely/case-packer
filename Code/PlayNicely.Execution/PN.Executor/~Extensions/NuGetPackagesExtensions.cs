﻿using NuGet.Configuration;

namespace PlayNicely.Executor;

public static class NuGetPackagesExtensions
{
    public static IEnumerable<PackageSource> GetPackageSources(this ITestEnvironment testEnv)
    {
        var settings = new Settings(testEnv.ProjectDirectory);

        return settings.GetPackageSources();
    }

    public static IEnumerable<PackageSource> GetPackageSources(this ISettings nuGetSettings)
    {
        var packageSourceProvider = new PackageSourceProvider(nuGetSettings);

        return packageSourceProvider.LoadPackageSources();
    }
}
