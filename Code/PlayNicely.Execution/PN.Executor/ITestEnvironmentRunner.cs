﻿namespace PlayNicely.Executor;

public interface ITestEnvironmentRunner<T> where T : ExecutionResult
{
    Task<T> ExecuteAsync(ITestEnvironment testEnv, CancellationToken cancel = default);
}
