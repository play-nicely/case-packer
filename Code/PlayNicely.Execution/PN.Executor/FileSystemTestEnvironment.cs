﻿using System.Diagnostics;

namespace PlayNicely.Executor;

[DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
public abstract class FileSystemTestEnvironment : ITestEnvironment
{
    public void Dispose()
    {
        GC.SuppressFinalize(this);
        Dispose(true);
    }

    public virtual void ReadyToStartInfo(ProcessStartInfo startInfo)
    {
        ArgumentNullException.ThrowIfNull(startInfo);
        ThrowIfDisposed();

        startInfo.WorkingDirectory = _projectDirectory.FullPath;
    }

    public string ProjectDirectory => _projectDirectory.FullPath;
    public string? ProjectFilename => ProjectFileRelativePath;
    public string TempDirectory => _tempDirectory.FullPath;

    protected FileSystemTestEnvironment()
    {
        _projectDirectory = new("project");
        _tempDirectory = new("temp");
    }

    protected void Dispose(bool fromDispose)
    {
        if(Disposable.RequiresDisposal(ref _disposed) == false)
            return;

        if(fromDispose)
            DisposeManaged();

        DisposeUnmanaged();
    }

    protected virtual void DisposeManaged()
    {
        _projectDirectory.Dispose();
        _tempDirectory.Dispose();
    }

    protected virtual void DisposeUnmanaged() { }

    protected void ThrowIfDisposed() => ObjectDisposedException.ThrowIf(_disposed == Disposable.ObjectDisposed, this);

    protected string? ProjectFileRelativePath
    {
        get
        {
            ThrowIfDisposed();

            return _projectFileRelativePath;
        }
        set
        {
            ThrowIfDisposed();

            _projectFileRelativePath = value;
        }
    }

    private string DebuggerDisplay => Path.Join(_projectDirectory.FullPath, ProjectFilename);

    private readonly TemporaryDirectory _projectDirectory;
    private readonly TemporaryDirectory _tempDirectory;

    private int _disposed = Disposable.ObjectNotDisposed;
    private string? _projectFileRelativePath;
}
