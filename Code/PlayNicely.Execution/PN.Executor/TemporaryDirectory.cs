﻿using System.Diagnostics;

namespace PlayNicely.Executor;

[DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
internal sealed class TemporaryDirectory : IDisposable
{
    public TemporaryDirectory(string? id = null)
    {
        var uniqueName = Guid.NewGuid().ToString("D");

        Id = id is null ? uniqueName : $"{id}-{uniqueName}";
        _fullPath = Path.Join(Path.GetTempPath(), Id);

        Directory.CreateDirectory(_fullPath);
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    public string FullPath
    {
        get
        {
            ObjectDisposedException.ThrowIf(Disposable.IsDisposed(_disposed), this);

            return _fullPath;
        }
    }

    public string Id { get; }

    ~TemporaryDirectory()
    {
        Dispose(disposing: false);
    }

    private void Dispose(bool disposing)
    {
        if(Disposable.RequiresDisposal(ref _disposed) == false)
            return;

        if(disposing)
            DisposeManaged();

        DisposeUnmanaged();
    }

    private void DisposeManaged() { }
    private void DisposeUnmanaged() => Directory.Delete(_fullPath, true);

    private string DebuggerDisplay => _fullPath;

    private readonly string _fullPath;
    private int _disposed = Disposable.ObjectNotDisposed;
}
