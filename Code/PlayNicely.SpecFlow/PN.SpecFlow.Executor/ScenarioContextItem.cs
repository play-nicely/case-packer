﻿namespace PlayNicely.SpecFlow;

public class ScenarioContextItem<T> where T : class
{
    public ScenarioContextItem(string key)
    {
        ArgumentException.ThrowIfNullOrEmpty(key);

        Key = key;
    }

    public T GetOrAddValue(ScenarioContext scenarioContext, Func<T> valueCreator)
    {
        T value;

        while(scenarioContext.TryGetValue<T>(Key, out value) == false)
            scenarioContext[Key] = valueCreator();

        return value;
    }

    public T? GetValue(ScenarioContext scenarioContext)
    {
        return scenarioContext.TryGetValue<T>(Key, out var value) ? value : null;
    }

    public T RequireValue(ScenarioContext scenarioContext, string? stepName)
    {
        var value = GetValue(scenarioContext);

        if(value is null)
            throw new InvalidOperationException(string.Format(ContentRes.Error_RequiredScenarioItem_IsNull,
                                                              stepName,
                                                              typeof(T).Name,
                                                              Key));

        return value;
    }

    public void SetValue(ScenarioContext scenarioContext, T? value)
    {
        if(value is null)
            scenarioContext.Remove(Key);
        else
            scenarioContext[Key] = value;
    }

    public string Key { get; }
}
