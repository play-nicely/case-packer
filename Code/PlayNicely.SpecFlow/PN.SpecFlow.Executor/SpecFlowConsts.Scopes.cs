﻿namespace PlayNicely.SpecFlow;

partial class SpecFlowConsts
{
    public static class Scopes
    {
        public static class Tags
        {
            public const string RequiresTestEnvironment = "test-environment";
        }
    }
}
