﻿using System.Runtime.CompilerServices;

namespace PlayNicely.SpecFlow;

public static class ExecutionResultExtensions
{
    public static void CollectResult<T>(this ScenarioContext scenarioContext, T executionResult) where T : ExecutionResult
    {
        Item.SetValue(scenarioContext, executionResult);
    }

    public static ExecutionResult? GetExecutionResult(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static ExecutionResult<TContext>? GetExecutionResult<TContext>(this ScenarioContext scenarioContext)
    {
        var genericResult = scenarioContext.GetExecutionResult();

        if(genericResult is null)
            return default;

        return genericResult.ToExactResult<TContext>();
    }

    public static ExecutionResult RequireExecutionResult(this ScenarioContext scenarioContext,
                                                         [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static ExecutionResult<TContext> RequireExecutionResult<TContext>(this ScenarioContext scenarioContext,
                                                                             [CallerMemberName] string? stepName = null)
    {
        return RequireExecutionResult(scenarioContext, stepName).ToExactResult<TContext>();
    }

    private static ScenarioContextItem<ExecutionResult> MakeScenarioItem() => new(KeyExecutionResult);

    private static ExecutionResult<TContext> ToExactResult<TContext>(this ExecutionResult genericResult)
    {
        if(genericResult is not ExecutionResult<TContext> exactResult)
            throw new InvalidCastException(string.Format(ContentRes.Error_ExecutionResult_CannotCastToResultType,
                                                         genericResult.GetType().Name,
                                                         typeof(ExecutionResult<TContext>).Name));

        return exactResult;
    }

    private static ScenarioContextItem<ExecutionResult> Item => _item.Value;

    private const string KeyExecutionResult = "execution-result";
    private readonly static Lazy<ScenarioContextItem<ExecutionResult>> _item = new(MakeScenarioItem);
}
