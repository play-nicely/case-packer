﻿namespace PlayNicely.SpecFlow;

public static class DisposableExtensions
{
    public static void MarkForDisposal(this ScenarioContext scenarioContext, IDisposable disposable)
    {
        scenarioContext.GetDisposableResourceList()
                       .Add(disposable);
    }

    internal static void DisposeResources(this ScenarioContext scenarioContext)
    {
        foreach(var disposable in scenarioContext.GetDisposableResourceList())
            disposable.Dispose();
    }

    private static List<IDisposable> GetDisposableResourceList(this ScenarioContext scenarioContext)
    {
        return scenarioContext.TryGetValue<List<IDisposable>>(KeyDisposeResources, out var disposeResources)
            ? disposeResources
            : new();
    }

    private const string KeyDisposeResources = "dispose-resources";
}
