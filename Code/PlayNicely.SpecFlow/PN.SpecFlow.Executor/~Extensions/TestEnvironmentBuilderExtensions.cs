﻿using System.Runtime.CompilerServices;

namespace PlayNicely.SpecFlow;

public static class TestEnvironmentBuilderExtensions
{
    public static void CreateEnvironmentBuilder(this ScenarioContext scenarioContext)
    {
        Item.SetValue(scenarioContext, new TestEnvironmentBuilder());
    }

    public static TestEnvironmentBuilder? GetEnvironmentBuilder(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static TestEnvironmentBuilder RequireEnvironmentBuilder(this ScenarioContext scenarioContext,
                                                                   [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static void SetEnvironmentBuilder(this ScenarioContext scenarioContext, TestEnvironmentBuilder? builder)
    {
        Item.SetValue(scenarioContext, builder);
    }

    private static ScenarioContextItem<TestEnvironmentBuilder> MakeScenarioItem() => new(KeyTestEnvironmentBuilder);

    private static ScenarioContextItem<TestEnvironmentBuilder> Item => _item.Value;

    private const string KeyTestEnvironmentBuilder = "test-environment-builder";
    private readonly static Lazy<ScenarioContextItem<TestEnvironmentBuilder>> _item = new(MakeScenarioItem);
}
