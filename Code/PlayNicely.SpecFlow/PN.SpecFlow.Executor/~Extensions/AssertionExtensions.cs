﻿using PlayNicely.SpecFlow.Assertions;

namespace PlayNicely.SpecFlow;

public static class AssertionExtensions
{
    public static DirectoryAssertions PathShould(this string instance)
    {
        return new DirectoryAssertions(instance);
    }

    public static ExecutionResultAssertions<T> Should<T>(this T instance) where T : ExecutionResult
    {
        return new ExecutionResultAssertions<T>(instance);
    }
}
