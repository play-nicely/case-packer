﻿namespace PlayNicely.SpecFlow;

partial class SpecFlowConsts
{
    public static class BindingOrder
    {
        public const int Cleanup = 1001000;
        public const int Post = 1000000;
        public const int Pre = 1000;
    }
}
