namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public sealed class GivenExecutorSteps
{
    public GivenExecutorSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [Given("command (.+?) is installed")]
    public void GivenCommandIsInstalled(string command)
    {
        _scenarioContext.RequireEnvironmentBuilder()
                        .RequiredCommands(command);
    }

    [Given("command (.+?) is uninstalled")]
    public void GivenCommandIsNotInstalled(string command)
    {
        _scenarioContext.RequireEnvironmentBuilder()
                        .ExcludeCommandFromPath(command);
    }

    private readonly ScenarioContext _scenarioContext;
}
