namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public sealed class WhenExecutorSteps
{
    public WhenExecutorSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [When("command (.+?) is executed")]
    public async Task WhenProcessIsExecutedAsync(string commandLine)
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();

        var testEnv = await builder.BuildAsync(default)
                                   .ConfigureAwait(false);
        try
        {
            var runner = ProcessRunner.Parse(commandLine);

            _scenarioContext.MarkForDisposal(testEnv);
            _scenarioContext.CollectResult(await runner.ExecuteAsync(testEnv)
                                                       .ConfigureAwait(false));
        }
        catch
        {
            testEnv.Dispose();
            throw;
        }
    }

    private readonly ScenarioContext _scenarioContext;
}
