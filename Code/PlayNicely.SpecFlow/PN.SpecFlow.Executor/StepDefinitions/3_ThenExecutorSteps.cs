namespace PlayNicely.SpecFlow;

public enum ExecutionOutcome
{
    Succeed,
    Fail,
}

[Binding]
public sealed class ThenExecutorSteps
{
    public ThenExecutorSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    #region Execution outcome

    [Then("the command should ([^ ]+)")]
    public void ThenTheCommandShould(ExecutionOutcome outcome)
    {
        _scenarioContext.RequireExecutionResult()
                        .Should()
                        .HaveOutcome(outcome);
    }

    [Then("the command should return exitcode (-?[0-9]+)")]
    public void ThenTheCommandShouldReturnExitcode(int exitCode)
    {
        _scenarioContext.RequireExecutionResult()
                        .Should()
                        .HaveExitCode(exitCode);
    }

    #endregion Execution outcome

    #region File system object existence

    [Then("directory (.+) should exist")]
    [Then("working directory should contain directory (.+)")]
    public void ThenDirectoryShouldExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .ContainDirectory(path);
    }

    [Then("directory (.+) should not exist")]
    [Then("working directory should not contain directory (.+)")]
    public void ThenDirectoryShouldNotExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .NotContainDirectory(path);
    }

    [Then("file (.+) should exist")]
    [Then("working directory should contain file (.+)")]
    public void ThenFileShouldExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .ContainFile(path);
    }

    [Then("file (.+) should not exist")]
    [Then("working directory should not contain file (.+)")]
    public void ThenFileShouldNotExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .NotContainFile(path);
    }

    [Then("path (.+) should exist")]
    [Then("working directory path (.+) should exist")]
    public void ThenPathShouldExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .ContainPath(path);
    }

    [Then("path (.+) should not exist")]
    [Then("working directory path (.+) should not exist")]
    public void ThenPathShouldNotExist(string path)
    {
        _scenarioContext.RequireExecutionResult()
                        .WorkingDirectory
                        .PathShould()
                        .NotContainPath(path);
    }

    #endregion File system object existence

    #region Standard streams

    [Then("stderr should contain (.+)")]
    public void ThenStandardErrorShouldContain(string str)
    {
        _scenarioContext.RequireExecutionResult()
                        .StandardError
                        .Should()
                        .ContainEquivalentOf(str);
    }

    [Then("stderr should match (.+)")]
    public void ThenStandardErrorShouldMatch(string pattern)
    {
        _scenarioContext.RequireExecutionResult()
                        .StandardError
                        .Should()
                        .MatchRegex(pattern);
    }

    [Then("stdout should contain (.+)")]
    public void ThenStandardOutputShouldContain(string str)
    {
        _scenarioContext.RequireExecutionResult()
                        .StandardOutput
                        .Should()
                        .ContainEquivalentOf(str);
    }

    [Then("stdout should match (.+)")]
    public void ThenStandardOutputShouldMatch(string pattern)
    {
        _scenarioContext.RequireExecutionResult()
                        .StandardOutput
                        .Should()
                        .MatchRegex(pattern);
    }

    #endregion Standard streams

    private readonly ScenarioContext _scenarioContext;
}
