﻿using PlayNicely.SpecFlow;

namespace PlayNicely.Tests;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public class EnvironmentBuilderHooks
{
    public EnvironmentBuilderHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [BeforeScenario(Order = BindingOrder.Pre)]
    public void BeforeEachScenario()
    {
        _scenarioContext.CreateEnvironmentBuilder();
    }

    private readonly ScenarioContext _scenarioContext;
}
