﻿namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

[Binding]
internal class DisposalHooks
{
    public DisposalHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [AfterScenario(Order = BindingOrder.Cleanup)]
    public void AfterEachScenario()
    {
        _scenarioContext.DisposeResources();
    }

    private readonly ScenarioContext _scenarioContext;
}
