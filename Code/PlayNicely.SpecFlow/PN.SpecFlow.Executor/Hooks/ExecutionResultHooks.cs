﻿namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

[Binding]
public class ExecutionResultHooks
{
    public ExecutionResultHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [AfterScenario(Order = BindingOrder.Post)]
    public void AfterEachScenario()
    {
        if(_scenarioContext.TestError is null)
            return;

        var executionResult = _scenarioContext.GetExecutionResult();

        if(executionResult is null)
        {
            Console.WriteLine(ContentRes.Message_FailedScenario_NoExecutionResult);
            return;
        }

        executionResult.WriteStreamToConsole();
    }

    private readonly ScenarioContext _scenarioContext;
}
