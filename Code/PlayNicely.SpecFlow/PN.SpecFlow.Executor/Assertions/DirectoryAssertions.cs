﻿using FluentAssertions.Execution;
using FluentAssertions.Primitives;

namespace PlayNicely.SpecFlow.Assertions;

public class DirectoryAssertions : StringAssertions<DirectoryAssertions>
{
    public DirectoryAssertions(string baseDirectory) : base(baseDirectory)
    {
        ArgumentException.ThrowIfNullOrEmpty(baseDirectory);

        var fullyQualifiedDirectory = baseDirectory;

        if(Path.IsPathFullyQualified(fullyQualifiedDirectory) == false)
            fullyQualifiedDirectory = Path.GetFullPath(fullyQualifiedDirectory);

        if(Path.IsPathFullyQualified(fullyQualifiedDirectory) == false)
            throw new ArgumentException(string.Format(ContentRes.Error_DirectoryAssertions_RequiresFullyQualifiedPath, baseDirectory), nameof(baseDirectory));
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> ContainDirectory(string directoryPath, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(directoryPath, nameof(directoryPath), because, becauseArgs)
            .ForCondition(fullPath => Directory.Exists(fullPath))
            .FailWith(ContentRes.Assert_Directory_ContainsPath, _ => directoryPath);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> ContainFile(string filePath, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(filePath, nameof(filePath), because, becauseArgs)
            .ForCondition(fullPath => File.Exists(fullPath))
            .FailWith(ContentRes.Assert_Directory_ContainsFile, _ => filePath);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> ContainPath(string path, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(path, nameof(path), because, becauseArgs)
            .ForCondition(fullPath => Directory.Exists(fullPath) || File.Exists(fullPath))
            .FailWith(ContentRes.Assert_Directory_ContainsPath, _ => path);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> NotContainDirectory(string directoryPath, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(directoryPath, nameof(directoryPath), because, becauseArgs)
            .ForCondition(fullPath => Directory.Exists(fullPath) == false)
            .FailWith(ContentRes.Assert_Directory_DoesNotContainDirectory, _ => directoryPath);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> NotContainFile(string filePath, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(filePath, nameof(filePath), because, becauseArgs)
            .ForCondition(fullPath => File.Exists(fullPath) == false)
            .FailWith(ContentRes.Assert_Directory_DoesNotContainFile, _ => filePath);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    [CustomAssertion]
    public AndConstraint<DirectoryAssertions> NotContainPath(string path, string because = "", params object[] becauseArgs)
    {
        AssertOnPath(path, nameof(path), because, becauseArgs)
            .ForCondition(fullPath => Directory.Exists(fullPath) == false && File.Exists(fullPath) == false)
            .FailWith(ContentRes.Assert_Directory_DoesNotContainDirectory, _ => path);

        return new AndConstraint<DirectoryAssertions>(this);
    }

    protected override string Identifier => "directory";

    private GivenSelector<string> AssertOnPath(string path, string pathArgName, string because, params object[] becauseArgs)
    {
        return Execute.Assertion
                      .BecauseOf(because, becauseArgs)
                      .ForCondition(string.IsNullOrEmpty(path) == false)
                      .FailWith(ContentRes.Assert_ExecutionResult_PathEmpty, pathArgName)
                      .Then
                      .Given(() => Path.Join(Subject, path));
    }
}
