﻿using FluentAssertions.Execution;
using FluentAssertions.Primitives;

namespace PlayNicely.SpecFlow.Assertions;

public class ExecutionResultAssertions<T> : ExecutionResultAssertions<T, ExecutionResultAssertions<T>> where T : ExecutionResult
{
    public ExecutionResultAssertions(T instance) : base(instance) { }
}

public class ExecutionResultAssertions<T, TAssertions> : ReferenceTypeAssertions<T, TAssertions>
                                                         where T : ExecutionResult
                                                         where TAssertions : ExecutionResultAssertions<T, TAssertions>
{
    public ExecutionResultAssertions(T instance) : base(instance) { }

    public AndConstraint<TAssertions> Fail(string because = "", params object[] becauseArgs)
    {
        return HaveOutcome(ExecutionOutcome.Fail, because, becauseArgs);
    }

    public AndConstraint<TAssertions> HaveExitCode(int expectedExitCode, string because = "", params object[] becauseArgs)
    {
        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .Given(() => Subject.ExitCode)
               .ForCondition(actualExitCode => actualExitCode == expectedExitCode)
               .FailWith(ContentRes.Assert_ExecutionResult_ExecutionExitCode, _ => expectedExitCode, actualExitCode => actualExitCode);

        return new AndConstraint<TAssertions>((TAssertions) this);
    }

    public AndConstraint<TAssertions> HaveOutcome(ExecutionOutcome expectedOutcome, string because = "", params object[] becauseArgs)
    {
        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .ForCondition(Enum.IsDefined(expectedOutcome))
               .FailWith(ContentRes.Assert_ExecutionResult_InvalidExecutionOutcome, nameof(ExecutionOutcome), expectedOutcome)
               .Then
               .Given(() => Subject.Succeeded ? ExecutionOutcome.Succeed : ExecutionOutcome.Fail)
               .ForCondition(actualOutcome => actualOutcome == expectedOutcome)
               .FailWith(ContentRes.Assert_ExecutionResult_ExecutionOutcome, _ => expectedOutcome, actualOutcome => actualOutcome);

        return new AndConstraint<TAssertions>((TAssertions) this);
    }

    public AndConstraint<TAssertions> Succeed(string because = "", params object[] becauseArgs)
    {
        return HaveOutcome(ExecutionOutcome.Succeed, because, becauseArgs);
    }

    protected override string Identifier => "execution result";
}
