namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public sealed class GivenDotNetSteps
{
    public GivenDotNetSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [Given("packages under test are output to (.+) and filenames match (.+)")]
    public void GivenPackagesUnderTestAreOutputToAndFilenamesMatch(string sourceLocation, string packagePattern)
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();
        var sourceName = Guid.NewGuid().ToString("D");

        builder.AddPackageSource(sourceLocation, sourceName)
               .MapPackagesToSource(sourceName, packagePattern)
               .UseBestPackageSourceFallback();
    }

    private readonly ScenarioContext _scenarioContext;
}
