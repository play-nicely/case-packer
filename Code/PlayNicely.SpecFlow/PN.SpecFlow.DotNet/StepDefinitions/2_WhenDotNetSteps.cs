using System.Collections.ObjectModel;

namespace PlayNicely.SpecFlow;

using static SpecFlowConsts;

public enum ProjectActionVerb
{
    Built,
    Packaged,
    Published,
}

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public sealed class WhenDotNetSteps
{
    public WhenDotNetSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [When("project (.+?) is ([^ ]+)")]
    public Task WhenProjectIsAsync(string projectName, ProjectActionVerb projectVerb)
    {
        return RunDotNetCommandAsync(projectName, projectVerb, null);
    }

    [When("project (.+?) is ([^ ]+) to (.+)")]
    public Task WhenProjectIsToAsync(string projectName, ProjectActionVerb projectVerb, string outputDirectory)
    {
        return RunDotNetCommandAsync(projectName, projectVerb, outputDirectory);
    }

    private static IDictionary<ProjectActionVerb, string> MakeVerbSubCommand()
    {
        return new ReadOnlyDictionary<ProjectActionVerb, string>(new Dictionary<ProjectActionVerb, string>
        {
            [ProjectActionVerb.Built] = "build",
            [ProjectActionVerb.Packaged] = "pack",
            [ProjectActionVerb.Published] = "publish",
        });
    }

    private async Task<TestCaseProject> LoadProjectAsync(string projectName, CancellationToken cancel)
    {
        cancel.ThrowIfCancellationRequested();

        return await _scenarioContext.RequireSuite()
                                     .LoadProjectAsync(projectName, cancel)
                                     .ConfigureAwait(false);
    }

    private async Task RunDotNetCommandAsync(string projectName, ProjectActionVerb projectVerb, string? outputDirectory)
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();

        builder.RequiredCommands(DotNetRunner.RequiredCommands);
        builder.Project = await LoadProjectAsync(projectName, default).ConfigureAwait(false);

        var testEnv = await builder.BuildAsync(default)
                                   .ConfigureAwait(false);
        try
        {
            var commandArgs = string.IsNullOrEmpty(outputDirectory) ? [] : new[] { "-o", outputDirectory };
            var runner = new DotNetRunner(VerbSubCommand[projectVerb], commandArgs);

            _scenarioContext.MarkForDisposal(testEnv);
            _scenarioContext.CollectResult(await runner.ExecuteAsync(testEnv)
                                                       .ConfigureAwait(false));
        }
        catch
        {
            testEnv.Dispose();
            throw;
        }
    }

    private static IDictionary<ProjectActionVerb, string> VerbSubCommand => _verbSubCommand.Value;

    private static readonly Lazy<IDictionary<ProjectActionVerb, string>> _verbSubCommand = new(MakeVerbSubCommand);
    private readonly ScenarioContext _scenarioContext;
}
