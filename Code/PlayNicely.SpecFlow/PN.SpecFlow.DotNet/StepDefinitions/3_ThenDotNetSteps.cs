namespace PlayNicely.SpecFlow;

[Binding]
public sealed class ThenDotNetSteps
{
    public ThenDotNetSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    #region Project File Existence

    [Then("project (.+) contains directory (.+)")]
    public void ThenProjectContainsDirectory(string projectName, string directoryPath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainDirectory(directoryPath);
    }

    [Then("project (.+) contains file (.+)")]
    public void ThenProjectContainsFile(string projectName, string filePath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainFile(filePath);
    }

    [Then("project (.+) contains path (.+)")]
    public void ThenProjectContainsPath(string projectName, string path)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainPath(path);
    }

    [Then("project (.+) does not contain directory (.+)")]
    public void ThenProjectDoesNotContainDirectory(string projectName, string directoryPath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainDirectory(directoryPath);
    }

    [Then("project (.+) does not contain file (.+)")]
    public void ThenProjectDoesNotContainFile(string projectName, string filePath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainFile(filePath);
    }

    [Then("project (.+) does not contain path (.+)")]
    public void ThenProjectDoesNotContainPath(string projectName, string path)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeProject(projectName)
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainPath(path);
    }

    [Then("the project contains directory (.+)")]
    public void ThenTheProjectContainsDirectory(string directoryPath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainDirectory(directoryPath);
    }

    [Then("the project contains file (.+)")]
    public void ThenTheProjectContainsFile(string filePath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainFile(filePath);
    }

    [Then("the project contains path (.+)")]
    public void ThenTheProjectContainsPath(string path)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .ContainPath(path);
    }

    [Then("the project does not contain directory (.+)")]
    public void ThenTheProjectDoesNotContainDirectory(string directoryPath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainDirectory(directoryPath);
    }

    [Then("the project does not contain file (.+)")]
    public void ThenTheProjectDoesNotContaisFile(string filePath)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainFile(filePath);
    }

    [Then("the project does not contain path (.+)")]
    public void ThenTheProjectDoesNotContainPath(string path)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .IncludeSingleProject()
                        .Where
                        .WorkingDirectory
                        .PathShould()
                        .NotContainPath(path);
    }

    #endregion Project File Existence

    #region Reported Errors

    [Then("(.+?) should be in the list of errors")]
    public void ThenShouldBeInTheListOfErrors(string errorCode)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .ReportError(errorCode);
    }

    #endregion Reported Errors

    #region Targets

    [Then("target (.+?) should not run")]
    public void ThenTargetShouldNotRun(string targetName)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .NotRunTarget(targetName);
    }

    [Then("target (.+?) should run")]
    public void ThenTargetShouldRun(string targetName)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .RunTarget(targetName);
    }

    [Then("target (.+?) should run ([0-9]+]) times?")]
    public void ThenTargetShouldRunTimes(string targetName, int numberOfTimes)
    {
        _scenarioContext.RequireExecutionResult<DotNetExecutionContext>()
                        .Should()
                        .RunTarget(targetName)
                        .AndNumberOfTimes
                        .Should()
                        .BeGreaterThanOrEqualTo(numberOfTimes);
    }

    #endregion Targets

    private readonly ScenarioContext _scenarioContext;
}
