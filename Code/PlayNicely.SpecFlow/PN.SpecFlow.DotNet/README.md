﻿[ms-package-source-mapping]: https://learn.microsoft.com/en-us/nuget/consume-packages/package-source-mapping
[pn-executor-best-fallback]: https://www.nuget.org/packages/PlayNicely.Executor#define-the-environment 
[pn-projects-ide]: https://www.nuget.org/packages/PlayNicely.Projects#using-the-ide

# Play Nicely - SpecFlow DotNet

This package extends the Play Nicely SpecFlow bindings for `dotnet` testing.
It provides step definitions for NuGet package sources and mappings,
`dotnet [build|pack|publish]`, and testing the output of that process,
including what errors occurred, if any.

## Getting Started

To start using these bindings in a SpecFlow project, install this package as a
dependency. Then add or update the `specflow.json` so that the project picks up
the bindings.

With this done, you can use the bindings to specify your scenarios.

### Install the package

From the Package Manager Console:

```PowerShell
Install-Package PlayNicely.SpecFlow.DotNet
```

### Add or Update specflow.json

For SpecFlow to pick up the bindings from external assemblies, they have to be
configured in the project's `specflow.json` file.  If the project doesn't
already have it, add the item to the root of your SpecFlow project.

Add the external assembly to the file:

```json
{
    "stepAssemblies": [
        { "assembly": "PlayNicely.SpecFlow.DotNet" }
    ]
}
```

You can now use the step definitions and bindings from Play Nicely.

## Creating Test Scenarios

A typical test scenario involves setting up (NuGet) package sources and
mappings, performing a `dotnet` command and then asserting the execution result
context, for dotnet related build properties, e.g. projects built, targets run,
errors that occurred, etc.

### Step Definitions

This package provides the following step definitions for use in test scenarios.

#### Given

* Given `packages under test are output to (directory) and filenames match (pattern)`\
  This creates a NuGet [package source and mapping][ms-package-source-mapping],
  with a [_best_ default fallback][pn-executor-best-fallback] source.

#### When

* When `project (project) is [built|packaged|published]`\
  This attempts to load a project from a `TestSuite`, then execute
  `dotnet build` (or `pack`, `publish` respectively) on that project.
* When `project (project) is [built|packaged|published] to (directory)`\
  Same as above but explicitly specifies the output directory `-o directory`.

#### Then

* Then `project (project) contains [directory|file|path] (relative/path)`\
  Assert that the `project.WorkingDirectory` contains a directory, file or
  either at _relative/path_.
* Then `project (project) does not contain [directory|file|path] (relative/path)`\
  Assert that the `project.WorkingDirectory` **does not** contain a directory,
  file or either at _relative/path_.
* Then `the project contains [directory|file|path] (relative/path)`\
  Similar assertion to a named project, except it is expected that only one
  project is part of this execution.  That project's `WorkingDirectory` is used
  for the assertions.  
* Then `the project does not contain [directory|file|path] (relative/path)`\
  Similar assertion to a named project, except it is expected that only one
  project is part of this execution.  That project's `WorkingDirectory` is used
  for the assertions.  
* Then `(errorCode) should be in the list of errors`\
  Assert that the `errorCode` should be in the `DotNetExecutionContext.Errors`
  collection.
* Then `target (targetName) should run`\
  Assert that the `<Target/>` with the name `targetName` did run as part of the
  build.
* Then `target (targetName) should run (times) times?`\
  Assert that `targetName` ran at least `times` number of times.
* Then `target (targetName) should not run`\
  Assert that the `<Target/>` with the name `targetName` did **not** run as 
  part of the build, pack or publish.

### Test Suites

`dotnet` tests typically require a number of pre-defined test (csproj)
projects. The `When (project) is built` step requires a way to load these test
projects, to do that a test suite needs to be defined.  Create a concrete class
that implements `ITestSuite` interface, or derive from `TestSuite` and set the
`ResourceNamespaceType`, the full name of which is the base name for all test 
case project resources.

> ℹ️ **Defining test case projects**\
  See [here][pn-projects-ide] for a detailed explanation of how to define test
  case projects in the IDE.

Once created, use a hook and call extension method `_scenarioContext.SetSuite`
to set the suite for the subsequent scenarios.