﻿namespace PlayNicely.SpecFlow.Assertions;

public class DotNetProjectConstraint<TAssertions> : AndWhichConstraint<TAssertions, DotNetProject>
{
    public DotNetProjectConstraint(TAssertions parentConstraint, DotNetProject project)
        : base(parentConstraint, project) { }

    public DotNetProject Where => Which;
}
