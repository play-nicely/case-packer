﻿namespace PlayNicely.SpecFlow.Assertions;

public class ProjectTargetConstraint<TAssertions> : AndWhichConstraint<TAssertions, int>
{
    public ProjectTargetConstraint(TAssertions parentConstraint, int numberOfTimesRan)
        : base(parentConstraint, numberOfTimesRan) { }

    public int AndNumberOfTimes => Which;
}
