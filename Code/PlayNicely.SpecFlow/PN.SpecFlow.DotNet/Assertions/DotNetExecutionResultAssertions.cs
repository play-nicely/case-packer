﻿namespace PlayNicely.SpecFlow.Assertions;

public class DotNetExecutionResultAssertions : DotNetExecutionResultAssertions<DotNetExecutionResultAssertions>
{
    public DotNetExecutionResultAssertions(ExecutionResult<DotNetExecutionContext> instance) : base(instance) { }
}

public class DotNetExecutionResultAssertions<TAssertions>
    : ExecutionResultAssertions<ExecutionResult<DotNetExecutionContext>, TAssertions>
      where TAssertions : DotNetExecutionResultAssertions<TAssertions>
{
    public DotNetExecutionResultAssertions(ExecutionResult<DotNetExecutionContext> instance) : base(instance) { }

    public DotNetProjectConstraint<TAssertions> IncludeProject(string projectName,
                                                               string because = "",
                                                               params object[] becauseArgs)
    {
        var projectFound = Subject.Context.Projects.TryGetValue(projectName, out var project);

        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .ForCondition(projectFound)
               .FailWith(ContentRes.Assert_ExecutionResult_ProjectNotFound, projectName);

        return new DotNetProjectConstraint<TAssertions>((TAssertions) this, project!);
    }

    public DotNetProjectConstraint<TAssertions> IncludeSingleProject(string because = "", params object[] becauseArgs)
    {
        var project = Subject.Context.Projects.Values.SingleOrDefault();

        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .ForCondition(project is not null)
               .FailWith(ContentRes.Assert_ExecutionResult_CannotInferProject, Subject.Context?.Projects.Count);

        return new DotNetProjectConstraint<TAssertions>((TAssertions) this, project!);
    }

    public AndConstraint<TAssertions> NotRunTarget(string targetName, string because = "", params object[] becauseArgs)
    {
        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .ForCondition(Subject.Context.Targets?.ContainsKey(targetName) != true)
               .FailWith(ContentRes.Assert_ExecutionResult_TargetDidRun, targetName);

        return new AndConstraint<TAssertions>((TAssertions) this);
    }

    public AndConstraint<TAssertions> ReportError(string errorCode, string because = "", params object[] becauseArgs)
    {
        Execute.Assertion
               .ForCondition(Subject.Context.Errors?.Any(error => error.Code == errorCode) == true)
               .FailWith(ContentRes.Assert_ExecutionResult_ErrorNotFound, errorCode);

        return new AndConstraint<TAssertions>((TAssertions) this);
    }

    public ProjectTargetConstraint<TAssertions> RunTarget(string targetName, string because = "", params object[] becauseArgs)
    {
        Execute.Assertion
               .BecauseOf(because, becauseArgs)
               .ForCondition(Subject.Context.Targets?.ContainsKey(targetName) == true)
               .FailWith(ContentRes.Assert_ExecutionResult_TargetNotFound, targetName, nameof(DotNetExecutionContext.Targets));

        return new ProjectTargetConstraint<TAssertions>((TAssertions) this, Subject.Context.Targets![targetName]);
    }

    protected override string Identifier => "execution result";
}
