﻿namespace PlayNicely.SpecFlow;

// TODO: Maybe use attributes to identify the test suite
public abstract class TestCaseSuite : ITestSuite
{
    public async Task<TestCaseProject> LoadProjectAsync(string projectName, CancellationToken cancel)
    {
        ArgumentException.ThrowIfNullOrEmpty(projectName);
        cancel.ThrowIfCancellationRequested();

        using var reader = ResourceSetPackageReader.Create(ResourceNamespaceType, projectName);

        var project = new TestCaseProject(projectName);

        await project.LoadFromAsync(reader, cancel)
                     .ConfigureAwait(false);

        return project;
    }

    protected virtual Type ResourceNamespaceType => GetType();
}
