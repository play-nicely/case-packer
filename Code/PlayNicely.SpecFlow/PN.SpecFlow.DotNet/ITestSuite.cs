﻿namespace PlayNicely.SpecFlow;

public interface ITestSuite
{
    Task<TestCaseProject> LoadProjectAsync(string projectName, CancellationToken cancel = default);
}
