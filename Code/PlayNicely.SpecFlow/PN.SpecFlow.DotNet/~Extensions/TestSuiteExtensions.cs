﻿using System.Runtime.CompilerServices;

namespace PlayNicely.SpecFlow;

public static class TestSuiteExtensions
{
    public static ITestSuite? GetSuite(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static ITestSuite RequireSuite(this ScenarioContext scenarioContext, [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static void SetSuite(this ScenarioContext scenarioContext, ITestSuite? suite)
    {
        Item.SetValue(scenarioContext, suite);
    }

    private static ScenarioContextItem<ITestSuite> MakeScenarioItem() => new(KeyTestCaseSuite);

    private static ScenarioContextItem<ITestSuite> Item => _item.Value;

    private const string KeyTestCaseSuite = "testcase-suite";

    private readonly static Lazy<ScenarioContextItem<ITestSuite>> _item = new(MakeScenarioItem);
}
