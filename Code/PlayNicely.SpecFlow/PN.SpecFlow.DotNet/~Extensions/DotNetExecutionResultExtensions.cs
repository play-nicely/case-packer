﻿using PlayNicely.SpecFlow.Assertions;

namespace PlayNicely.SpecFlow;

public static class DotNetExecutionResultExtensions
{
    public static DotNetExecutionResultAssertions Should(this ExecutionResult<DotNetExecutionContext> instance)
    {
        return new DotNetExecutionResultAssertions(instance);
    }
}
