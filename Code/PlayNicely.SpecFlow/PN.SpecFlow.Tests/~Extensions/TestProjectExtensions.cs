﻿using System.Runtime.CompilerServices;
using PlayNicely.Projects;

namespace PlayNicely.Tests;

public static class TestProjectExtensions
{
    public static TestCaseProject GetOrAddTestProject(this ScenarioContext scenarioContext, [CallerMemberName] string? stepName = null)
    { 
        return Item.GetOrAddValue(scenarioContext, () => new(stepName ?? Guid.NewGuid().ToString("D")));
    }

    public static TestCaseProject? GetTestProject(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static TestCaseProject RequireTestProject(this ScenarioContext scenarioContext, [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static void SetTestProjectName(this ScenarioContext scenarioContext, TestCaseProject? testProject)
    {
        Item.SetValue(scenarioContext, testProject);
    }

    private static ScenarioContextItem<TestCaseProject> MakeScenarioItem() => new(KeyTestProject);

    private static ScenarioContextItem<TestCaseProject> Item => _item.Value;

    private const string KeyTestProject = "test-project";
    private readonly static Lazy<ScenarioContextItem<TestCaseProject>> _item = new(MakeScenarioItem);
}
