﻿using System.Runtime.CompilerServices;

namespace PlayNicely.Tests;

public static class TestProjectNameExtensions
{
    public static string? GetTestProjectName(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static string RequireTestProjectName(this ScenarioContext scenarioContext, [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static void SetTestProjectName(this ScenarioContext scenarioContext, string? testProject)
    {
        Item.SetValue(scenarioContext, testProject);
    }

    private static ScenarioContextItem<string> MakeScenarioItem() => new(KeyTestProjectName);

    private static ScenarioContextItem<string> Item => _item.Value;

    private const string KeyTestProjectName = "test-project-name";
    private readonly static Lazy<ScenarioContextItem<string>> _item = new(MakeScenarioItem);
}
