﻿using System.Diagnostics.CodeAnalysis;

namespace PlayNicely.Tests;

public static class TestExceptionExtensions
{
    public static void CollectException(this ScenarioContext scenarioContext, Exception ex)
    {
        scenarioContext[KeyTestException] = ex;
    }

    public static bool TryGetException(this ScenarioContext scenarioContext, [NotNullWhen(true)] out Exception? ex)
    {
        return scenarioContext.TryGetValue<Exception>(KeyTestException, out ex);
    }

    private const string KeyTestException = "test-exception";
}
