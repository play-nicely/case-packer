﻿using System.Runtime.CompilerServices;
using PlayNicely.Executor;

namespace PlayNicely.Tests;

public static class TestEnvironmentExtensions
{
    public static ITestEnvironment? GetTestEnvironment(this ScenarioContext scenarioContext)
    {
        return Item.GetValue(scenarioContext);
    }

    public static ITestEnvironment RequireTestEnvironment(this ScenarioContext scenarioContext,
                                                          [CallerMemberName] string? stepName = null)
    {
        return Item.RequireValue(scenarioContext, stepName);
    }

    public static void SetTestEnvironment(this ScenarioContext scenarioContext, ITestEnvironment? testEnvironment)
    {
        Item.SetValue(scenarioContext, testEnvironment);
    }

    private static ScenarioContextItem<ITestEnvironment> MakeScenarioItem() => new(KeyTestEnvironment);

    private static ScenarioContextItem<ITestEnvironment> Item => _item.Value;

    private const string KeyTestEnvironment = "test-environment";
    private readonly static Lazy<ScenarioContextItem<ITestEnvironment>> _item = new(MakeScenarioItem);
}
