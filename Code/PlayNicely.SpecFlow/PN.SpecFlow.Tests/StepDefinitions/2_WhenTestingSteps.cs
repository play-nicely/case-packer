﻿namespace PlayNicely.Tests;

using PlayNicely.Executor;
using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public class WhenTestingSteps
{
    public WhenTestingSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
        _executorSteps = new WhenExecutorSteps(scenarioContext);
        _dotnetSteps = new WhenDotNetSteps(scenarioContext);
    }

    [When("execution of (.*) is attempted")]
    public async Task WhenCommandExecutionIsAttemptedAsync(string command)
    {
        try
        {
            await _executorSteps.WhenProcessIsExecutedAsync(command)
                                .ConfigureAwait(false);
        }
        catch(Exception ex)
        {
            _scenarioContext.CollectException(ex);
        }
    }

    [When("scenario is (.*) and output to (.*)")]
    public Task WhenScenarioIsAndOutputTo(ProjectActionVerb actionVerb, string outputDirectory)
    {
        return _dotnetSteps.WhenProjectIsToAsync(_scenarioContext.RequireTestProjectName(), actionVerb, outputDirectory);
    }

    [When("scenario is (.*) with default options")]
    public Task WhenScenarioIsWithDefaultOptions(ProjectActionVerb actionVerb)
    {
        return _dotnetSteps.WhenProjectIsAsync(_scenarioContext.RequireTestProjectName(), actionVerb);
    }

    [When("simple test command is executed")]
    public async Task WhenSimpleTestCommandIsExecutedAsync()
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();
        var testProject = _scenarioContext.GetTestProject();

        if(testProject != null)
            builder.SetProject(testProject);

        var testEnv = await CreateTestEnvironmentAsync(default).ConfigureAwait(false);
        var runner = ProcessRunner.Parse(SimpleCommand);

        _scenarioContext.CollectResult(await runner.ExecuteAsync(testEnv)
                                                    .ConfigureAwait(false));
    }

    [When("test environment construction is attempted")]
    public async Task WhenTestEnvironmentConstructionIsAttemptedAsync()
    {
        try
        {
            await CreateTestEnvironmentAsync(default).ConfigureAwait(false);
        }
        catch(Exception ex)
        {
            _scenarioContext.CollectException(ex);
        }
    }

    private async Task<ITestEnvironment> CreateTestEnvironmentAsync(CancellationToken cancel)
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();

        var testEnv = await builder.BuildAsync(default)
                                   .ConfigureAwait(false);
        try
        {
            _scenarioContext.MarkForDisposal(testEnv);

            return testEnv;
        }
        catch
        {
            testEnv.Dispose();
            throw;
        }
    }

    private static string SimpleCommand
    {
        get
        {
            if(OperatingSystem.IsWindows())
                return "cmd.exe /C echo hello";
            else if(OperatingSystem.IsLinux() || OperatingSystem.IsMacOS())
                return "echo hello";
            else
                throw new NotSupportedException(string.Format(ContentRes.Error_OperatingSystemNotSupported_SimpleCommand,
                                                              Environment.OSVersion.Platform));
        }
    }

    private readonly WhenDotNetSteps _dotnetSteps;
    private readonly WhenExecutorSteps _executorSteps;
    private readonly ScenarioContext _scenarioContext;
}
