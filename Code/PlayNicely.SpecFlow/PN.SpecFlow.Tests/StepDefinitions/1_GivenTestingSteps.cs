﻿namespace PlayNicely.Tests;

using System;
using System.Text.RegularExpressions;
using PlayNicely.Projects;

using static SpecFlowConsts;

[Scope(Tag = Scopes.Tags.RequiresTestEnvironment)]
[Binding]
public class GivenTestingSteps
{
    public GivenTestingSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [Given("command (.*) is required but not installed")]
    public void GivenCommandIsNotInstalled(string command)
    {
        var builder = _scenarioContext.RequireEnvironmentBuilder();

        builder.RequiredCommands(command);
    }

    [Given("scenario is based on (.*)")]
    public void GivenScenarioIsBasedOn(string projectName)
    {
        _scenarioContext.SetTestProjectName(projectName);
    }

    [Given("test project includes directory (.*)")]
    public void GivenTestProjectIncludesDirectory(string directoryPath)
    {
        EnsureTestProjectIncludesDirectory(PatternPathSeparator.Split(directoryPath));
    }

    [Given("test project includes file (.*)")]
    public void GivenTestProjectIncludesFile(string filePath)
    {
        var testProject  = _scenarioContext.GetOrAddTestProject();
        var pathSegments = PatternPathSeparator.Split(filePath);
        var directory    = EnsureTestProjectIncludesDirectory(testProject, pathSegments.SkipLast(1));

        directory.AddFileAndContent(pathSegments.Last(), "hello world");
    }

    private TestCaseProject.IFileSystemContainer EnsureTestProjectIncludesDirectory(IEnumerable<string> directories)
    {
        return EnsureTestProjectIncludesDirectory(_scenarioContext.GetOrAddTestProject(), directories);
    }

    private TestCaseProject.IFileSystemContainer EnsureTestProjectIncludesDirectory(TestCaseProject testProject, IEnumerable<string> directories)
    {
        var container = (TestCaseProject.IFileSystemContainer) testProject.Root;

        foreach(var directory in directories)
        {
            if(container.TryGet(directory, out var entity))
            {
                if(entity is not TestCaseProject.IFileSystemContainer subContainer)
                    throw new InvalidOperationException(string.Format(ContentRes.Error_FileSystem_EnsureDirectoryIsFile, string.Join('/', directories), directory));

                container = subContainer;
            }
            else
            {
                container = container.AddDirectory(directory);
            }
        }

        return container;
    }

    private static readonly Regex PatternPathSeparator = new Regex(@"(\\|\/)");

    private readonly ScenarioContext _scenarioContext;
}
