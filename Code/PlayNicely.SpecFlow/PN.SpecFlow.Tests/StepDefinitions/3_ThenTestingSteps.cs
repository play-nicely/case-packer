using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using FluentAssertions.Execution;
using PlayNicely.Projects;
using PlayNicely.Executor;
using PlayNicely.Executor.DotNet;

namespace PlayNicely.Tests;

[Binding]
public sealed class ThenTestingSteps
{
    public ThenTestingSteps(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
        _dotnetSteps = new(scenarioContext);
    }

    [Then("an? (.*) should be thrown")]
    public void ThenAnExceptionShouldBeThrown(string exceptionType)
    {
        var expectedType = GetTypeNamed(exceptionType);

        using(new AssertionScope())
        {
            expectedType.Should()
                        .NotBeNull(ContentRes.Assert_ExceptionType_CannotBeNull, exceptionType);

            _scenarioContext.TryGetException(out var actualException)
                            .Should()
                            .BeTrue();

            actualException.Should()
                           .BeAssignableTo(expectedType);
        }
    }

    [Then("build output of project (.*) contains file (.*)")]
    public void ThenBuildOutputOfProjectContainsFile(string projectName, string filePath)
    {
        var testPath = Path.Join(TestConsts.OutputPath, TestConsts.TargetFramework, filePath);

        _dotnetSteps.ThenProjectContainsFile(projectName, testPath);
    }

    [Then("the project build output contains file (.*)")]
    public void ThenTheProjectBuildOutputContainsFile(string filePath)
    {
        var testPath = Path.Join(TestConsts.OutputPath, TestConsts.TargetFramework, filePath);

        _dotnetSteps.ThenProjectContainsFile(_scenarioContext.RequireTestProjectName(), testPath);
    }

    [Then("the project package output contains file (.*)")]
    public void ThenTheProjectPackageOutputContainsFile(string filePath)
    {
        var testPath = Path.Join(TestConsts.PackageOutputPath, filePath);

        _dotnetSteps.ThenProjectContainsFile(_scenarioContext.RequireTestProjectName(), testPath);
    }

    [Then("the project publish output contains file (.*)")]
    public void ThenTheProjectPublishOutputContainsFile(string filePath)
    {
        var testPath = Path.Join(TestConsts.PublishPath, filePath);

        _dotnetSteps.ThenProjectContainsFile(_scenarioContext.RequireTestProjectName(), testPath);
    }

    private static Type? GetTypeNamed(string typeName)
    {
        Type? type;

        if(TryGetTypeNamed(typeName, out type))
            return type;

        foreach(var friendNamespace in FriendNamespaces)
        {
            if(TryGetTypeNamed($"{friendNamespace}.{typeName}", out type))
                return type;

            foreach(var assembly in FriendAssemblies)
            {
                if(TryGetTypeNamed(typeName, assembly, out type))
                    return type;

                if(TryGetTypeNamed($"{friendNamespace}.{typeName}", assembly, out type))
                    return type;
            }
        }

        return null;
    }

    private static IList<Assembly> MakeFriendAssemblies() => Array.AsReadOnly(new[]
    {
        typeof(ProjectsConsts).Assembly,
        typeof(ITestEnvironment).Assembly,
        typeof(DotNetRunner).Assembly,
        typeof(EnvironmentBuilderHooks).Assembly,
        typeof(GivenDotNetSteps).Assembly,
    });

    private static IList<string> MakeFriendNamespaces()
    {
        IList<string>? namespaces = FriendAssemblies.SelectMany(assembly => assembly.GetTypes())
                                                    .Where(type => string.IsNullOrEmpty(type.Namespace) == false)
                                                    .Select(type => type.Namespace!)
                                                    .Distinct()
                                                    .ToList()
                                                    .AsReadOnly();

        return namespaces ?? [];
    }

    private static bool TryGetTypeNamed(string typeName, [NotNullWhen(true)] out Type? type)
    {
        return TryGetTypeNamed(typeName, Type.GetType, out type);
    }

    private static bool TryGetTypeNamed(string typeName, Assembly assembly, [NotNullWhen(true)] out Type? type)
    {
        return TryGetTypeNamed(typeName, assembly.GetType, out type);
    }

    private static bool TryGetTypeNamed(string typeName, Func<string, Type?> getType, [NotNullWhen(true)] out Type? type)
    {
        type = getType(typeName);

        return type is not null;
    }

    private static IList<Assembly> FriendAssemblies => _friendAssemblies.Value;
    private static IList<string> FriendNamespaces => _friendNamespaces.Value;
    private static readonly Lazy<IList<Assembly>> _friendAssemblies = new(() => MakeFriendAssemblies());
    private static readonly Lazy<IList<string>> _friendNamespaces = new(() => MakeFriendNamespaces());

    private readonly ThenDotNetSteps _dotnetSteps;
    private readonly ScenarioContext _scenarioContext;
}
