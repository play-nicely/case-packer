﻿namespace PlayNicely.Tests;

internal static class TestConsts
{
    public static class BasicCommands
    {
        public const string ProjectFileName = ProjectName + ".csproj";
        public const string ProjectName = nameof(BasicCommands);
    }

    public static class BuildErrors
    {
        public const string ProjectFileName = ProjectName + ".csproj";
        public const string ProjectName = nameof(BuildErrors);
    }

    public static class MultiProjectSolution
    {
        public static class ConsoleApp
        {
            public const string ProjectDirectory = ProjectName;
            public const string ProjectFileName = ProjectName + ".csproj";
            public const string ProjectName = nameof(ConsoleApp);
        }

        public static class SharedLib
        {
            public const string ProjectDirectory = ProjectName;
            public const string ProjectFileName = ProjectName + ".csproj";
            public const string ProjectName = nameof(SharedLib);
        }

        public const string SolutionFileName = SolutionName + ".sln";
        public const string SolutionName = nameof(MultiProjectSolution);
    }

    public const string OutputPath = "bin-out";
    public const string PackageOutputPath = "nuget";
    public const string PublishPath = "pub";
    public const string PublishProfile = "fld-profile";
    public const string TargetFramework = "netstandard2.0";
}
