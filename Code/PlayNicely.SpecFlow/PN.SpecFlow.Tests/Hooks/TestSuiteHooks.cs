﻿using System.Collections.ObjectModel;
using PlayNicely.Projects;

namespace PlayNicely.Tests.Hooks;

using static TestConsts;

[Binding]
internal class TestSuiteHooks
{
    public TestSuiteHooks(ScenarioContext scenarioContext)
    {
        _scenarioContext = scenarioContext;
    }

    [BeforeScenario]
    public void BeforeEachScenario()
    {
        _scenarioContext.SetSuite(TestSuite.Instance);
    }

    private class TestSuite : ITestSuite
    {
        public Task<TestCaseProject> LoadProjectAsync(string projectName, CancellationToken cancel)
        {
            if(_projects.TryGetValue(projectName, out var loadProject) == false)
                throw new InvalidOperationException(string.Format(ContentRes.Error_TestSuite_ProjectNotFound, projectName));

            return Task.FromResult(loadProject());
        }

        public static ITestSuite Instance => _instance.Value;

        private TestSuite()
        {
            _projects = new ReadOnlyDictionary<string, Func<TestCaseProject>>(new Dictionary<string, Func<TestCaseProject>>
            {
                [BasicCommands.ProjectName] = LoadBasicCommandProject,
                [BuildErrors.ProjectName] = LoadBuildErrorsProject,
                [MultiProjectSolution.SolutionName] = LoadMultiProjectSolution,
            });
        }

        private TestCaseProject LoadBasicCommandProject()
        {
            var project = new TestCaseProject(BasicCommands.ProjectName);

            project.AddProjectFile(BasicCommands.ProjectFileName,
                                   string.Format(ContentRes.File_BasicCommands_Project,
                                                 TargetFramework,
                                                 OutputPath,
                                                 PackageOutputPath,
                                                 PublishProfile))
                   .AddFileAndContent("program.cs", ContentRes.File_BasicCommands_Program)
                   .AddDirectory("Properties")
                   .AddDirectory("PublishProfiles")
                   .AddFileAndContent($"{PublishProfile}.pubxml",
                                      string.Format(ContentRes.File_BasicCommands_PublishProfile, PublishPath));

            project.Root.AddDirectory("wwwroot")
                        .AddFileAndContent("theme.css", ContentRes.File_BasicCommands_ThemeStyles);

            return project;
        }

        private TestCaseProject LoadBuildErrorsProject()
        {
            var project = new TestCaseProject(TestConsts.BuildErrors.ProjectName);

            project.AddProjectFile(TestConsts.BuildErrors.ProjectFileName, ContentRes.File_BuildErrors_Project)
                   .AddFileAndContent("program.cs", ContentRes.File_BuildErrors_Program);

            return project;
        }

        private TestCaseProject LoadMultiProjectSolution()
        {
            var project = new TestCaseProject(MultiProjectSolution.SolutionName);

            project.Root.AddFileAndContent(MultiProjectSolution.SolutionFileName, ContentRes.File_MultiProjectSolution_Solution);

            project.Root.AddDirectory(MultiProjectSolution.SharedLib.ProjectDirectory)
                        .AddFileAndContent(MultiProjectSolution.SharedLib.ProjectFileName,
                                           string.Format(ContentRes.File_MultiProjectSolution_SharedLib_Project,
                                                         TargetFramework,
                                                         OutputPath))
                        .AddFileAndContent("utils.cs", ContentRes.File_MultiProjectSolution_SharedLib_Utils);

            project.Root.AddDirectory(MultiProjectSolution.ConsoleApp.ProjectDirectory)
                        .AddFileAndContent(MultiProjectSolution.ConsoleApp.ProjectFileName,
                                           string.Format(ContentRes.File_MultiProjectSolution_ConsoleApp_Project,
                                                         TargetFramework,
                                                         OutputPath))
                        .AddFileAndContent("program.cs", ContentRes.File_MultiProjectSolution_ConsoleApp_Program);

            return project;
        }

        private static readonly Lazy<ITestSuite> _instance = new(() => new TestSuite());
        private readonly IDictionary<string, Func<TestCaseProject>> _projects;
    }

    private readonly ScenarioContext _scenarioContext;
}
