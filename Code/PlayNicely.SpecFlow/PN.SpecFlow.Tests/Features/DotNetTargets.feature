﻿@test-environment
Feature: DotNet Targets

DotNet runner collects details about which Targets run.

Scenario: MSBuild Targets that execute are recorded
    Given scenario is based on BasicCommands
	When scenario is published with default options
	Then target Build should run
	    And target Publish should run

Scenario: MSBuild Targets that don't execute are not recorded
    Given scenario is based on BasicCommands
	When scenario is built with default options
	Then target Publish should not run
