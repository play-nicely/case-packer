﻿@test-environment
Feature: DotNet Status and Errors

DotNet runner collects status information and any errors that occur.

Scenario: Testing builds can have their outcome checked
    Given scenario is based on BasicCommands
	When scenario is built with default options
	Then the command should succeed

Scenario: Errors that occur during failed builds, can be checked
    Given scenario is based on BuildErrors
	When scenario is built with default options
	Then the command should fail
	    And CS1002 should be in the list of errors
