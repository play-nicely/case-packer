﻿@test-environment
Feature: File System Object Existence

Scenarios that check for the existence, or not, of files, directories or either.
			   
Scenario: Tests allow checking for the existence of a file
     Given test project includes file test.txt
     When simple test command is executed
	 Then working directory should contain file test.txt

Scenario: Tests allow checking for the lack of existence of a file
     When simple test command is executed
	 Then working directory should not contain file test.txt

Scenario: Testing for file existence returns false for directories
     Given test project includes directory mydir
     When simple test command is executed
	 Then working directory should not contain file mydir

Scenario: Testing for file existence supports files in directories
     Given test project includes directory a-dir
     And test project includes file a-dir/test.txt
     When simple test command is executed
	 Then working directory should contain file a-dir/test.txt

Scenario: Tests allow checking for the existence of a directories
     Given test project includes file mydir/test.txt
     When simple test command is executed
	 Then working directory should contain directory mydir

Scenario: Tests allow checking for the lack of existence of a directory
     When simple test command is executed
	 Then working directory should not contain directory mydir

Scenario: Testing for directory existence returns false for files
     Given test project includes file test.txt
     When simple test command is executed
	 Then working directory should not contain directory test.txt

# TODO: Once support for empty directories is available for IPackageWriter/Reader
#         Tests for sub-directories
#         Tests for paths