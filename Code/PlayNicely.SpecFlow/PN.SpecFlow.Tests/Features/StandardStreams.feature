﻿@test-environment
Feature: Standard Streams

Scenarios to assess a program's standard streams.
			   
Scenario: Standard output stream can be checked to see if it contains a string
    Given command dotnet is installed
	When command dotnet -? is executed
	Then stdout should contain usage

Scenario: Standard output stream can be checked to see if it matches a regex pattern
    Given command dotnet is installed
	When command dotnet --version is executed
	Then stdout should match [0-9]+\.[0-9]+\.[0-9]

Scenario: Standard error stream can be checked to see if it contains a string
    Given command dotnet is installed
	When command dotnet help build --bogus is executed
	Then stderr should contain --bogus

Scenario: Standard error stream can be checked to see if it matches a regex pattern
    Given command dotnet is installed
	When command dotnet help xytestyz is executed
	Then stderr should match .*xytestyz
