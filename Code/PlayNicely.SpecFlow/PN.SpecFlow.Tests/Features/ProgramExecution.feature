﻿@test-environment
Feature: Program Execution

Basic process execution and environment setup scenarios.
			   
Scenario: The success of a program's execution can be tested
    Given command dotnet is installed
	When command dotnet --version is executed
	Then the command should succeed

Scenario: The failure of a program's execution can be tested
    Given command dotnet is installed
	When command dotnet -x is executed
	Then the command should fail

Scenario: The exit code of a program can be tested
    Given command dotnet is installed
	When command dotnet --version is executed
	Then the command should return exitcode 0

Scenario: If a required program isn't installed an exception is thrown when the test environment is contructed
    Given command blah is required but not installed
	When test environment construction is attempted
	Then an InvalidEnvironmentException should be thrown

Scenario: Attempting to run a program that isn't installed throws exception
	When execution of blah is attempted
	Then a System.ComponentModel.Win32Exception should be thrown

Scenario: Attempting to run a program that is installed, but has been uninstalled, throws exception
    Given command ping is uninstalled
	    And command gcc is uninstalled
	When execution of ping -? is attempted
	    And execution of gcc -v is attempted
	Then a System.ComponentModel.Win32Exception should be thrown
