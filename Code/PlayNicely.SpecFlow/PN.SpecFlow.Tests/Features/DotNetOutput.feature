﻿@test-environment
Feature: DotNet	Outputs

DotNet runner and ITestSuite provide tooling to execute dotnet commands against .NET projects.

Scenario: Successfully building dotnet projects, outputs binaries
    Given scenario is based on BasicCommands
	When scenario is built with default options
	Then the project build output contains file BasicCommandTest.dll

Scenario: Successfully packing dotnet projects, outputs packages
    Given scenario is based on BasicCommands
	When scenario is packaged with default options
	Then the project package output contains file BasicCommandTest.1.0.0.nupkg

Scenario: Successfully publishing dotnet projects, outputs content
    Given scenario is based on BasicCommands
	When scenario is published with default options
	Then the project publish output contains file wwwroot/theme.css

Scenario: Successfully building dotnet solutions, outputs all project's binaries
    Given scenario is based on MultiProjectSolution
	When scenario is built with default options
	Then build output of project ConsoleApp contains file ConsoleApp.dll
	    And build output of project SharedLib contains file SharedLib.dll

Scenario: The output location of dotnet commands can be explicitly specified
    Given scenario is based on BasicCommands
	When scenario is built and output to my-out
	Then the project contains file my-out/BasicCommandTest.dll
