using Moq;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PlayNicely.Tests;

partial class TestCaseProjectTests
{
    [Test]
    public async Task LoadAddsFilesToProject()
    {
        const string projectFile = "reader.csproj";

        InitializeTestSubject();

        _packageReaderMock.SetupSequence(reader => reader.ReadAsync(It.IsAny<CancellationToken>()))
                          .Returns(MakeBlobDouble(projectFile))
                          .Returns(NoMoreBlobs());

        await _testSubject.LoadFromAsync(_packageReaderDouble)
                          .ConfigureAwait(false);

        Assert.That(_testSubject.Root.Exists(projectFile), Is.True);
    }

    [Test]
    public async Task LoadContinuesUntilReadReturnsNull()
    {
        InitializeTestSubject();

        _packageReaderMock.Setup(reader => reader.ReadAsync(It.IsAny<CancellationToken>()))
                          .Returns(NoMoreBlobs())
                          .Verifiable(Times.Once);

        await _testSubject.LoadFromAsync(_packageReaderDouble)
                          .ConfigureAwait(false);

        _packageReaderMock.Verify();
        Assert.That(_testSubject.Root.Count, Is.Zero);
    }

    [Test]
    public async Task LoadCreatesDirectoriesInProjects()
    {
        const string projectDirectory = "project";
        const string projectFilePath = projectDirectory + TestCaseProject.FileSystem.DirectorySeparator + "reader.csproj";

        InitializeTestSubject();

        _packageReaderMock.SetupSequence(reader => reader.ReadAsync(It.IsAny<CancellationToken>()))
                          .Returns(MakeBlobDouble(projectFilePath))
                          .Returns(NoMoreBlobs());

        await _testSubject.LoadFromAsync(_packageReaderDouble)
                          .ConfigureAwait(false);

        Assert.That(_testSubject.Root.Exists(projectDirectory), Is.True);
        Assert.That(_testSubject.Root.Exists(projectFilePath), Is.True);
    }

    [Test]
    public async Task LoadReadsContentFromBlobs()
    {
        const string projectContent = "content-file.cs";
        const string content = "hello world";

        InitializeTestSubject();

        _packageReaderMock.SetupSequence(reader => reader.ReadAsync(It.IsAny<CancellationToken>()))
                          .Returns(MakeBlobDouble(projectContent, content: content))
                          .Returns(NoMoreBlobs());

        await _testSubject.LoadFromAsync(_packageReaderDouble)
                          .ConfigureAwait(false);

        var file = _testSubject.Root.GetFiles().First(file => file.Name ==  projectContent);
        var fileContent = await ReadAllContentAsync(file).ConfigureAwait(false);

        Assert.That(fileContent, Is.EqualTo(content));
    }

    [Test]
    public async Task LoadSetsProjectFileIfBlobIsProjectIsTrue()
    {
        const string projectFile = "project-file.csproj";

        InitializeTestSubject();

        _packageReaderMock.SetupSequence(reader => reader.ReadAsync(It.IsAny<CancellationToken>()))
                          .Returns(MakeBlobDouble(projectFile, isProjectFile: true))
                          .Returns(NoMoreBlobs());

        await _testSubject.LoadFromAsync(_packageReaderDouble)
                          .ConfigureAwait(false);

        Assert.That(_testSubject.ProjectFile, Is.EqualTo(_testSubject.Root[projectFile]));
    }

    [Test]
    public void LoadWithNullReaderThrowsException()
    {
        InitializeTestSubject();

        Assert.That(async () => await _testSubject.LoadFromAsync(_nullReader!)
                                                  .ConfigureAwait(false),
                    Throws.ArgumentNullException);
    }

    [SetUp]
    protected void BeforeEachPackageReadTest()
    {
        _packageReaderMock = new Mock<IProjectPackageReader>();
        _packageReaderDouble = _packageReaderMock.Object;
    }

    private static Task<IProjectBlob?> MakeBlobDouble(string projectFile, string? content = null, bool isProjectFile = false)
    {
        var blobMock = new Mock<IProjectBlob>();

        blobMock.SetupAllProperties();
        blobMock.SetupGet(blob => blob.FullName)
                .Returns(projectFile);
        blobMock.SetupGet(blob => blob.IsProjectFile)
                .Returns(isProjectFile);

        blobMock.Setup(blob => blob.OpenReadStream())
                .Returns(() => content is null ? Stream.Null : new MemoryStream(Encoding.UTF8.GetBytes(content)));

        return Task.FromResult<IProjectBlob?>(blobMock.Object);
    }

    private static Task<IProjectBlob?> NoMoreBlobs() => Task.FromResult<IProjectBlob?>(null);

    private static async Task<string> ReadAllContentAsync(TestCaseProject.IFileSystemFile file)
    {
        using var reader = new StreamReader(file.OpenReadStream(), Encoding.UTF8);

        return await reader.ReadToEndAsync();
    }

    private readonly IProjectPackageReader? _nullReader = null;

    [NotNull]
    private IProjectPackageReader? _packageReaderDouble;

    [NotNull]
    private Mock<IProjectPackageReader>? _packageReaderMock;
}
