using System.Diagnostics.CodeAnalysis;
using Moq;

namespace PlayNicely.Tests;

partial class TestCaseProjectTests
{
    [Test]
    public async Task ProjectFileBlobHasIsProjectFileTrue()
    {
        const string projectFile = "hello-world.csproj";

        _packageWriterMock.Setup(writer => writer.WriteAsync(It.Is<IProjectBlob>(blob => blob.IsProjectFile == true),
                                                             It.IsAny<CancellationToken>()))
                          .Returns(Task.CompletedTask)
                          .Verifiable();

        InitializeTestSubject();
        _testSubject.ProjectFile = _testSubject.Root.AddFile(projectFile);

        await _testSubject.SaveToAsync(_packageWriterDouble)
                          .ConfigureAwait(false);

        _packageWriterMock.Verify();
    }

    [Test]
    public void SaveWithNullWriterThrowsException()
    {
        InitializeTestSubject();

        Assert.That(async () => await _testSubject.SaveToAsync(_nullWriter!)
                                                  .ConfigureAwait(false),
                    Throws.ArgumentNullException);
    }

    [Test]
    public async Task SaveWritesAllFilesUsingWriter()
    {
        const string projectDirectory = "project";
        const string projectFile = "project.csproj";
        const string projectCode = "code.cs";
        const string solutionFile = "solution.sln";

        const string projectFilePath = projectDirectory + TestCaseProject.FileSystem.DirectorySeparator + projectFile;
        const string projectCodePath = projectDirectory + TestCaseProject.FileSystem.DirectorySeparator + projectCode;

        _packageWriterMock.Setup(writer => writer.WriteAsync(It.Is<IProjectBlob>(blob => blob.FullName == projectFilePath),
                                                             It.IsAny<CancellationToken>()))
                          .Returns(Task.CompletedTask)
                          .Verifiable();
        _packageWriterMock.Setup(writer => writer.WriteAsync(It.Is<IProjectBlob>(blob => blob.FullName == projectCodePath),
                                                             It.IsAny<CancellationToken>()))
                          .Returns(Task.CompletedTask)
                          .Verifiable();
        _packageWriterMock.Setup(writer => writer.WriteAsync(It.Is<IProjectBlob>(blob => blob.FullName == solutionFile),
                                                             It.IsAny<CancellationToken>()))
                          .Returns(Task.CompletedTask)
                          .Verifiable();

        InitializeTestSubject();

        var project = _testSubject.Root.AddDirectory(projectDirectory);

        _testSubject.Root.AddFile(solutionFile);
        project.AddFile(projectFile);
        project.AddFile(projectCode);

        await _testSubject.SaveToAsync(_packageWriterDouble)
                          .ConfigureAwait(false);

        _packageWriterMock.Verify();
    }

    [Test]
    public async Task SaveWritesFileUsingWriter()
    {
        const string projectFile = "hello-world.csproj";

        _packageWriterMock.Setup(writer => writer.WriteAsync(It.Is<IProjectBlob>(blob => blob.FullName == projectFile),
                                                             It.IsAny<CancellationToken>()))
                          .Returns(Task.CompletedTask)
                          .Verifiable();

        InitializeTestSubject();
        _testSubject.Root.AddFile(projectFile);

        await _testSubject.SaveToAsync(_packageWriterDouble)
                          .ConfigureAwait(false);

        _packageWriterMock.Verify();
    }

    [SetUp]
    protected void BeforeEachPackageWriteTest()
    {
        _packageWriterMock = new Mock<IProjectPackageWriter>();
        _packageWriterDouble = _packageWriterMock.Object;
    }

    private readonly IProjectPackageWriter? _nullWriter = null;

    [NotNull]
    private IProjectPackageWriter? _packageWriterDouble;

    [NotNull]
    private Mock<IProjectPackageWriter>? _packageWriterMock;
}
