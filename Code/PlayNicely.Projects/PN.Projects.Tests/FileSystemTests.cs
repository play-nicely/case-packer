using System.Diagnostics.CodeAnalysis;

namespace PlayNicely.Tests;

[TestFixture]
public class FileSystemTests
{
    [Test]
    public void AddFileFromAnotherFileSystemThrowsException()
    {
        InitializeTestSubject();

        var altFileSystem = new TestCaseProject.FileSystem();
        var newFile = altFileSystem.AddFile("test");

        Assert.That(() => newFile.Parent = _testSubject, Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void CircularParentReferenceThrowsException()
    {
        InitializeTestSubject();

        var dir1 = _testSubject.AddDirectory("a");
        var dir2 = dir1.AddDirectory("b");

        Assert.That(() => dir1.Parent = dir2, Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void CloningIsDeep()
    {
        const string code = "hello.cs";
        const string project = "project.csproj";
        const string directory = "project-dir";

        InitializeTestSubject();

        _testSubject.AddFile(code);
        _testSubject.AddFile(project);

        var dirProject = _testSubject.AddDirectory(directory)
                                     .AddFile(project);

        var cloned = _testSubject.Clone();

        cloned.AddFile("another.file");

        Assert.Multiple(() =>
        {
            Assert.That(cloned[code], Is.Not.SameAs(_testSubject[code]));
            Assert.That(cloned[code].Name, Is.EqualTo(_testSubject[code].Name));

            Assert.That(_testSubject, Has.Count.Not.EqualTo(cloned.Count));

            Assert.That(cloned.Exists(dirProject.FullName));
            Assert.That(cloned[directory], Is.Not.SameAs(_testSubject[directory]));
        });
    }

    [Test]
    public void ContainerTryGetReturnsFalseIfItemDoesNotExist()
    {
        InitializeTestSubject();

        Assert.That(_testSubject.TryGet("anything", out var _), Is.False);
    }

    [Test]
    public void ContainerTryGetReturnsTrueIfItemDoesExist()
    {
        const string directoryName = "this-dir";

        InitializeTestSubject();

        _testSubject.AddDirectory(directoryName);

        Assert.That(_testSubject.TryGet(directoryName, out var _), Is.True);
    }

    [Test]
    public void DeleteRemovesChildFromContainer()
    {
        InitializeTestSubject();

        var file = _testSubject.AddFile("file.name");

        file.Delete();

        Assert.That(_testSubject, Has.Count.EqualTo(0));
    }

    [Test]
    public void DuplicateDirectoryFileNamesInAContainerThrowsException()
    {
        const string directoryName = "something";
        const string fileName = directoryName;

        InitializeTestSubject();

        _testSubject.AddDirectory(directoryName);

        Assert.That(() => _testSubject.AddFile(fileName), Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void DuplicateDirectoryNamesInAContainerThrowsException()
    {
        const string directoryName = "directory";

        InitializeTestSubject();

        _testSubject.AddDirectory(directoryName);

        Assert.That(() => _testSubject.AddDirectory(directoryName), Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void DuplicateFileDirectoryNamesInAContainerThrowsException()
    {
        const string directoryName = "something";
        const string fileName = directoryName;

        InitializeTestSubject();

        _testSubject.AddFile(fileName);

        Assert.That(() => _testSubject.AddDirectory(directoryName), Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void DuplicateFileNamesInAContainerThrowsException()
    {
        const string fileName = "file.name";

        InitializeTestSubject();

        _testSubject.AddFile(fileName);

        Assert.That(() => _testSubject.AddFile(fileName), Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [Test]
    public void FileSystemContainer_Exists()
    {
        Assert.Ignore("Define tests for the Exists method, simple name, path name");
    }

    [Test]
    public void FileSystemContainer_GetDirectories()
    {
        Assert.Ignore("Define tests for the GetDirectories method");
    }

    [Test]
    public void FileSystemContainer_GetFiles()
    {
        Assert.Ignore("Define tests for the GetFiles method");
    }

    [Test]
    public void FileSystemFile_OpenReadStream()
    {
        Assert.Ignore("FileSystemFile_OpenReadStream test placeholder");
    }

    [Test]
    public void FileSystemFile_OpenWriteStream()
    {
        Assert.Ignore("FileSystemFile_OpenWriteStream test placeholder");
    }

    [Test]
    public void IfContainerTryGetReturnsTrueWhenItemIsValid()
    {
        const string fileName = "afile.txt";

        InitializeTestSubject();

        _testSubject.AddFile(fileName);

        Assert.Multiple(() =>
        {
            Assert.That(_testSubject.TryGet(fileName, out var file), Is.True);
            Assert.That(file?.Name, Is.EqualTo(fileName));
        });
    }

    [Test]
    public void SetNameToEmptyThrowsException()
    {
        InitializeTestSubject();

        var newFile = _testSubject.AddFile("test");

        Assert.That(() => newFile.Name = string.Empty, Throws.ArgumentException);
    }

    [Test]
    public void SetNameToNullThrowsException()
    {
        InitializeTestSubject();

        var newFile = _testSubject.AddFile("test");
        string? nullName = null;

        Assert.That(() => newFile.Name = nullName!, Throws.ArgumentNullException);
    }

    [Test]
    public void SetParentAddsEntityToParent()
    {
        const string directoryName = "directory";
        const string fileName1 = "file1.txt";
        const string fileName2 = "file2.txt";

        InitializeTestSubject();

        var file1 = _testSubject.AddFile(fileName1);
        var directory = _testSubject.AddDirectory(directoryName);

        directory.AddFile(fileName2);

        file1.Parent = directory;

        Assert.That(directory, Does.Contain(file1));
    }

    [Test]
    public void SetParentRemovesEntityFromOldParent()
    {
        const string directoryName = "directory";
        const string fileName1 = "file1.txt";
        const string fileName2 = "file2.txt";

        InitializeTestSubject();

        var file1 = _testSubject.AddFile(fileName1);
        var directory = _testSubject.AddDirectory(directoryName);

        directory.AddFile(fileName2);

        file1.Parent = directory;

        Assert.That(_testSubject, Does.Not.Contain(file1));
    }

    [Test]
    public void SetParentWithDuplicateNameThrowsException()
    {
        const string directoryName = "directory";
        const string fileName = "file";

        InitializeTestSubject();

        var file = _testSubject.AddFile(fileName);
        var directory = _testSubject.AddDirectory(directoryName);

        directory.AddFile(fileName);

        Assert.That(() => file.Parent = directory, Throws.InstanceOf<TestCaseProject.FileSystemException>());
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeTestSubject()
    {
        _testSubject = new TestCaseProject.FileSystem();
    }

    private TestCaseProject.FileSystem? _testSubject;
}
