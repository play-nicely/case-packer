﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace PlayNicely.Tests;

[TestFixture]
public partial class TestCaseProjectTests
{
    // TODO: Tests for resx reader, what if the ProjectFile key is set but the file doens't exist?
    [Test]
    public void CloneWithNullSourceProjectThrowException()
    {
        TestCaseProject? source = null;

        Assert.That(() => new TestCaseProject(source!), Throws.ArgumentNullException);
    }

    [Test]
    public void CloningIsDeep()
    {
        InitializeTestSubject();

        _testSubject.Root.AddFile("hello.cs");
        _testSubject.ProjectFile = _testSubject.Root.AddFile("project.csproj");

        var cloned = _testSubject.Clone();

        Assert.Multiple(() =>
        {
            Assert.That(cloned.Root, Is.Not.SameAs(_testSubject.Root));

            Assert.That(cloned.ProjectFile, Is.Not.SameAs(_testSubject.ProjectFile));
            Assert.That(cloned.ProjectFile!.Name, Is.EqualTo(_testSubject.ProjectFile.Name));
        });
    }

    [Test]
    public void CreateWithEmptyProjectNameThrowException()
    {
        var projectName = string.Empty;

        Assert.That(() => new TestCaseProject(projectName), Throws.ArgumentException);
    }

    [Test]
    public void CreateWithNullProjectNameThrowException()
    {
        string? projectName = null;

        Assert.That(() => new TestCaseProject(projectName!), Throws.ArgumentNullException);
    }

    [Test]
    public void SetProjectFileToUnownedFileThrowsException()
    {
        InitializeTestSubject();

        var altFileSystem = new TestCaseProject.FileSystem();
        var projectFile = altFileSystem.AddFile("test.csproj");

        Assert.That(() => _testSubject.ProjectFile = projectFile, Throws.InstanceOf<InvalidProjectException>());
    }

    [MemberNotNull(nameof(_testSubject))]
    private void InitializeTestSubject([CallerMemberName] string? testName = null)
    {
        _testSubject = new TestCaseProject(testName!);
    }

    private TestCaseProject? _testSubject;
}
