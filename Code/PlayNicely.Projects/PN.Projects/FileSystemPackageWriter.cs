﻿using System.IO;

namespace PlayNicely.Projects;

public class FileSystemPackageWriter : IProjectPackageWriter
{
    public FileSystemPackageWriter(string? baseDirectory)
    {
        var validBaseDirectory = baseDirectory ?? string.Empty;

        _baseDirectory = Path.IsPathFullyQualified(validBaseDirectory)
            ? validBaseDirectory
            : Path.GetFullPath(validBaseDirectory);

        if(Path.IsPathFullyQualified(_baseDirectory) == false)
            throw new ArgumentException(string.Format(ContentRes.Error_FileSystemWriter_BaseDirectoryMustBeResolvableToAbsolutePath,
                                                      GetType().Name,
                                                      baseDirectory));
    }

    public async Task WriteAsync(IProjectBlob blob, CancellationToken cancel = default)
    {
        var filePath = Path.Join(_baseDirectory, blob.FullName);

        EnsureDirectoryExists(filePath);

        using var blobStream = blob.OpenReadStream();
        using var fileStream = new FileStream(filePath, FileMode.Create);

        await blobStream.CopyToAsync(fileStream, cancel)
                        .ConfigureAwait(false);
    }

    public char NameSeparator => Path.DirectorySeparatorChar;

    private static void EnsureDirectoryExists(string fullPath)
    {
        var directory = Path.GetDirectoryName(fullPath)
            ?? throw new InvalidOperationException(string.Format(ContentRes.Error_FileSystemWriter_AbsoluteDirectoryRequired,
                                                                 fullPath));

        Directory.CreateDirectory(directory);
    }

    private readonly string _baseDirectory;
}
