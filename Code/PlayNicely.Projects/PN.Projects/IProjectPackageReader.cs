﻿namespace PlayNicely.Projects;

public interface IProjectPackageReader
{
    // TODO: Support reading packages with empty directories
    Task<IProjectBlob?> ReadAsync(CancellationToken cancel = default);
}
