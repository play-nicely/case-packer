﻿using System.Collections.Generic;
using System.IO;

namespace PlayNicely.Projects;

public partial class TestCaseProject : ICloneable
{
    public TestCaseProject(string projectName)
    {
        ArgumentException.ThrowIfNullOrEmpty(projectName);

        Name = projectName;
        Root = new FileSystem();
    }

    public TestCaseProject(TestCaseProject source)
    {
        ArgumentNullException.ThrowIfNull(source);

        Name = source.Name;
        Root = source.Root.Clone();

        if(source.ProjectFile is not null)
            ProjectFile = GetFile(source.ProjectFile.FullName);
    }

    public TestCaseProject Clone() => new(this);

    public async Task LoadFromAsync(IProjectPackageReader packageReader, CancellationToken cancel = default)
    {
        ArgumentNullException.ThrowIfNull(packageReader);

        cancel.ThrowIfCancellationRequested();

        Root.Format();

        var blob = await packageReader.ReadAsync(cancel)
                                      .ConfigureAwait(false);
        while(blob != null)
        {
            cancel.ThrowIfCancellationRequested();
            var projectFile = EnsureFileTree(blob);

            if(blob.IsProjectFile)
                ProjectFile = projectFile;

            using var fileStream = projectFile.OpenWriteStream();
            using var blobStream = blob.OpenReadStream();

            cancel.ThrowIfCancellationRequested();
            await blobStream.CopyToAsync(fileStream, cancel)
                            .ConfigureAwait(false);

            blob = await packageReader.ReadAsync(cancel)
                                      .ConfigureAwait(false);
        }
    }

    public Task SaveToAsync(IProjectPackageWriter writer, CancellationToken cancel = default)
    {
        ArgumentNullException.ThrowIfNull(writer);

        return WriteAllEntriesAsync(writer, Root, cancel);
    }

    object ICloneable.Clone() => Clone();

    public string Name { get; }

    public IFileSystemFile? ProjectFile
    {
        get => _projectFile;
        set
        {
            if(_projectFile == value)
                return;

            if(value is not null && Root.Owned(value) == false)
                throw new InvalidProjectException(string.Format(ContentRes.Error_TestCaseProject_InvalidProjectFile, value.Name));

            _projectFile = value;
        }
    }

    public string? ProjectPath => ProjectFile?.FullName;
    public FileSystem Root { get; }

    private class StreamBlob : IProjectBlob, IDisposable
    {
        public StreamBlob(string fullName)
        {
            ArgumentException.ThrowIfNullOrEmpty(fullName);

            FullName = fullName;
            _content = new MemoryStream();
        }

        public void Dispose()
        {
            _content.Dispose();
        }

        public Stream OpenReadStream() => new MemoryStream(_content.ToArray(), false);

        public Task SetContentAsync(Stream newContent, CancellationToken cancel = default)
        {
            cancel.ThrowIfCancellationRequested();
            _content.SetLength(newContent.Length);

            return newContent.CopyToAsync(_content, cancel);
        }

        public string FullName { get; }
        public bool IsProjectFile { get; set; } = false;

        private readonly MemoryStream _content;
    }

    private static IFileSystemEntity? FindEntityRecursive(IEnumerable<IFileSystemEntity> items,
                                                          Func<IFileSystemEntity, bool> predicate)
    {
        foreach(var item in items)
        {
            if(predicate(item))
                return item;

            if(item is IEnumerable<IFileSystemEntity> children)
                return FindEntityRecursive(children, predicate);
        }

        return null;
    }

    private IFileSystemFile EnsureFileTree(IProjectBlob blob)
    {
        IFileSystemContainer current = Root;

        var nameSegments = blob.FullName.Split(ProjectBlob.NameSeparator);
        for(var i = 0; i < nameSegments.Length - 1; i++)
        {
            var name = nameSegments[i];
            var entity = current.TryGet(name, out var e) ? e : null;

            if(entity is null)
                current = current.AddDirectory(name);
            else if(entity is IFileSystemContainer nextContainer)
                current = nextContainer;
            else
                throw new InvalidProjectException(string.Format(ContentRes.Error_TestCaseProject_TreeConflict,
                                                                blob.FullName,
                                                                entity.Name));
        }

        return current.AddFile(nameSegments[^1]);
    }

    private IFileSystemFile? GetFile(string fullPath)
    {
        return FindEntityRecursive(Root, entity => entity.FullName == fullPath) as IFileSystemFile;
    }

    private async Task WriteAllEntriesAsync(IProjectPackageWriter writer,
                                            IEnumerable<IFileSystemEntity> entities,
                                            CancellationToken cancel)
    {
        foreach(var entity in entities)
        {
            if(entity is IFileSystemFile file)
            {
                var fileName = file.FullName;

                using var fileStream = file.OpenReadStream();
                using var blob = new StreamBlob(fileName);

                blob.IsProjectFile = fileName == ProjectPath;

                await blob.SetContentAsync(fileStream, cancel)
                          .ConfigureAwait(false);

                await writer.WriteAsync(blob, cancel)
                            .ConfigureAwait(false);
            }
            else if(entity is IEnumerable<IFileSystemEntity> container)
            {
                await WriteAllEntriesAsync(writer, container, cancel).ConfigureAwait(false);
            }
            else
            {
                throw new InvalidProjectException(string.Format(ContentRes.Error_TestCaseProject_WriteInvalidEntity,
                                                                entity.Name,
                                                                entity?.GetType().Name));
            }
        }
    }

    private IFileSystemFile? _projectFile;
}
