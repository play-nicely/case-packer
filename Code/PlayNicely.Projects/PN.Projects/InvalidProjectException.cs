﻿namespace PlayNicely.Projects;

[Serializable]
public class InvalidProjectException : PlayNicelyException
{
    public InvalidProjectException() : base() { }
    public InvalidProjectException(string? message) : base(message) { }
    public InvalidProjectException(string? message, Exception? innerException) : base(message, innerException) { }
}
