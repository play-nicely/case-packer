﻿using System.Collections;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace PlayNicely.Projects;

using static ProjectsConsts;

public class ResourceSetPackageReader : IProjectPackageReader, IDisposable
{
    public ResourceSetPackageReader(ResourceSet resourceSet, bool ownsResourceSet = true)
    {
        ArgumentNullException.ThrowIfNull(resourceSet);

        _ownsResourceSet = ownsResourceSet;
        _resourceSet = resourceSet;
        _projectFile = new(GetProjectFile);
    }

    public static ResourceSetPackageReader Create(Assembly assembly, string resourceName)
    {
        ArgumentNullException.ThrowIfNull(assembly);
        ArgumentException.ThrowIfNullOrEmpty(resourceName);

        var resourceManager = new ResourceManager(resourceName, assembly);
        var resourceSet = resourceManager.GetResourceSet(CultureInfo.InvariantCulture, true, true)
            ?? throw new InvalidOperationException(string.Format(ContentRes.Error_ResourceSetReader_ResourceNotFound,
                                                                 assembly.FullName,
                                                                 resourceName));
        try
        {
            return new ResourceSetPackageReader(resourceSet);
        }
        catch
        {
            resourceSet.Dispose();
            throw;
        }
    }

    public static ResourceSetPackageReader Create<T>(string? projectName = null) => Create(typeof(T), projectName);
    public static ResourceSetPackageReader Create(Type type, string? projectName = null)
    {
        ArgumentNullException.ThrowIfNull(type);

        var assembly = type.Assembly;
        var resourceProjectName = projectName ?? type.Name;
        var resourceNameComponents = new[] { type.Namespace, resourceProjectName, };
        var resourceName = string.Join(".", resourceNameComponents.Where(s => string.IsNullOrEmpty(s) == false));

        return Create(assembly, resourceName);
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);

        _resourceSet.Dispose();
    }

    public Task<IProjectBlob?> ReadAsync(CancellationToken cancel)
    {
        cancel.ThrowIfCancellationRequested();

        if(_enumerator == null)
            Interlocked.CompareExchange(ref _enumerator, _resourceSet.GetEnumerator(), null);

        if(_enumerator.MoveNext() == false)
            return Task.FromResult<IProjectBlob?>(null);

        return _enumerator.Entry.Key.Equals(KeyProjectFile)
            ? ReadAsync(cancel)
            : Task.FromResult<IProjectBlob?>(new ResourceBlob(_enumerator.Entry, ProjecteFile));
    }

    public string? ProjecteFile => _projectFile.Value;

    private class ResourceBlob : IProjectBlob
    {
        public ResourceBlob(DictionaryEntry entry, string? projectFile)
        {
            _entry = entry;
            _projectFile = projectFile;
        }

        public Stream OpenReadStream()
        {
            var obj = _entry.Value;

            if(obj is null)
                return Stream.Null;

            if(obj is string str)
                return new MemoryStream(DefaultEncoding.GetBytes(str));

            if(obj is byte[] byteArray)
                return new MemoryStream(byteArray);

            throw new InvalidOperationException(string.Format(ContentRes.Error_ResourceSetReader_OpenStreamUnsupportedType,
                                                              obj.GetType().FullName));
        }

        public string FullName => $"{_entry.Key}";
        public bool IsProjectFile => FullName == _projectFile;

        private readonly DictionaryEntry _entry;
        private readonly string? _projectFile;
    }

    private string? GetProjectFile() => _resourceSet.GetString(KeyProjectFile);

    private const string KeyProjectFile = "ProjectFile";

    private readonly bool _ownsResourceSet;
    private readonly Lazy<string?> _projectFile;
    private readonly ResourceSet _resourceSet;
    private IDictionaryEnumerator? _enumerator;
}
