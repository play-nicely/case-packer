﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace PlayNicely.Projects;

partial class TestCaseProject
{
    public interface IFileSystemChild : IFileSystemObject
    {
        void Delete() => Parent = null;

        IFileSystemContainer? Parent { get; set; }
    }

    public interface IFileSystemContainer : IFileSystemObject, IEnumerable<IFileSystemEntity>, IEnumerable
    {
        IFileSystemDirectory AddDirectory(string name);
        IFileSystemFile AddFile(string name);
        bool Exists(string path);
        IEnumerable<IFileSystemDirectory> GetDirectories();
        IEnumerable<IFileSystemFile> GetFiles();
        bool TryGet(string name, [NotNullWhen(true)] out IFileSystemEntity? entity);

        int Count { get; }
        IFileSystemEntity this[int index] { get; }
        IFileSystemEntity this[string name] { get; }
    }

    public interface IFileSystemDirectory : IFileSystemContainer, IFileSystemEntity { }

    public interface IFileSystemEntity : IFileSystemChild
    {
        string FullName { get; }
        string Name { get; set; }
    }

    public interface IFileSystemFile : IFileSystemEntity
    {
        Stream OpenReadStream();
        Stream OpenWriteStream();
    }

    public interface IFileSystemObject { }

    public class FileSystem : IFileSystemContainer, ICloneable
    {
        public FileSystem()
        {
            _entities = new();
        }

        public IFileSystemDirectory AddDirectory(string name) => NewDirectory(name, this);
        public IFileSystemFile AddFile(string name) => NewFile(name, this);
        public FileSystem Clone() => new(this);
        public bool Exists(string path) => _entities.Exists(path);
        public void Format() => _entities.Clear();
        public IEnumerable<IFileSystemDirectory> GetDirectories() => _entities.GetDirectories();
        public IEnumerator<IFileSystemEntity> GetEnumerator() => _entities.GetEnumerator();
        public IEnumerable<IFileSystemFile> GetFiles() => _entities.GetFiles();
        public bool Owned(IFileSystemEntity entity) => IsObjectOwnedByThis(entity);
        public bool TryGet(string name, [NotNullWhen(true)] out IFileSystemEntity? entity) => _entities.TryGet(name, out entity);

        object ICloneable.Clone() => Clone();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int Count => _entities.Count;
        public IFileSystemEntity this[int index] => _entities[index];
        public IFileSystemEntity this[string name] => _entities[name];

        public const string DirectorySeparator = "/";

        private interface IFileSystemCloneable
        {
            FileSystemEntity Clone(FileSystem newOwner);
        }

        private class FileSystemDirectory : FileSystemEntity, IFileSystemDirectory
        {
            public FileSystemDirectory(FileSystem owner) : base(owner)
            {
                _entities = new();
            }

            public IFileSystemDirectory AddDirectory(string name) => _owner.NewDirectory(name, this);
            public IFileSystemFile AddFile(string name) => _owner.NewFile(name, this);
            public override FileSystemEntity Clone(FileSystem newOwner) => new FileSystemDirectory(newOwner, this);
            public bool Exists(string path) => _entities.Exists(path);
            public IEnumerable<IFileSystemDirectory> GetDirectories() => _entities.GetDirectories();
            public IEnumerator<IFileSystemEntity> GetEnumerator() => _entities.GetEnumerator();
            public IEnumerable<IFileSystemFile> GetFiles() => _entities.GetFiles();
            public bool TryGet(string name, [NotNullWhen(true)] out IFileSystemEntity? entity) => _entities.TryGet(name, out entity);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public int Count => _entities.Count;
            public FileSystemEntityList Entities => _entities;
            public IFileSystemEntity this[int index] => _entities[index];
            public IFileSystemEntity this[string name] => _entities[name];

            private FileSystemDirectory(FileSystem newOwner, FileSystemDirectory source) : base(newOwner, source)
            {
                _entities = new();

                CloneEntities(newOwner, source, _entities);
            }

            private readonly FileSystemEntityList _entities;
        }

        [DebuggerDisplay("{" + nameof(DebuggerDisplay) + ",nq}")]
        private abstract class FileSystemEntity : IFileSystemEntity, IFileSystemCloneable
        {
            public abstract FileSystemEntity Clone(FileSystem newOwner);
            public bool IsOwnedBy(FileSystem fileSystem) => fileSystem == _owner;

            public string FullName => string.Join(FileSystem.DirectorySeparator, GetEntityNames(this));

            public string Name
            {
                get => _name;
                set
                {
                    if(_name == value)
                        return;

                    ArgumentException.ThrowIfNullOrWhiteSpace(value);

                    if(_parent is not null)
                        GetEntityListOf(_parent).ThrowIfNameIsNotUnique(this);

                    _name = value;
                }
            }

            public IFileSystemContainer? Parent
            {
                get => _parent;
                set
                {
                    if(_parent == value)
                        return;

                    RemoveFromParent();
                    AddToParent(value);
                }
            }

            protected FileSystemEntity(FileSystem owner) : this(owner, string.Empty, null) { }

            protected FileSystemEntity(FileSystem newOwner, FileSystemEntity source)
                : this(newOwner, source?._name, source?._parent)
            {
                ArgumentNullException.ThrowIfNull(source);
            }

            protected static FileSystemEntityList GetEntityListOf(IFileSystemObject obj)
            {
                if(obj is FileSystem fileSystem)
                    return fileSystem._entities;

                if(obj is FileSystemDirectory directory)
                    return directory.Entities;

                throw new FileSystemException(ContentRes.Error_TestCaseFileSystem_ObjectNotAContainer);
            }

            protected FileSystemEntityList ValidateAndGetEntityListOf(IFileSystemObject obj, [CallerMemberName] string? failingMethod = null)
            {
                ValidateObjectOwnership(obj, failingMethod);

                return GetEntityListOf(obj);
            }

            protected void ValidateObjectOwnership(IFileSystemObject obj, [CallerMemberName] string? failingMethod = null)
            {
                if(_owner.IsObjectOwnedByThis(obj) == false)
                    throw MakeInvalidObjectOwnerException(obj, failingMethod ?? ContentRes.Error_TestCaseFileSystem_InvalidObject);
            }

            protected virtual FileSystemEntityList ValidateParent(IFileSystemContainer newParent)
            {
                var result = ValidateAndGetEntityListOf(newParent);

                ValidateNoCyclicDependencies(newParent);

                return result;
            }

            protected readonly FileSystem _owner;

            private FileSystemEntity(FileSystem owner, string? name, IFileSystemContainer? parent)
            {
                _owner = owner;
                _name = name ?? string.Empty;
                _parent = parent;
            }

            private static Exception MakeInvalidObjectOwnerException(IFileSystemObject obj, string failingMethod)
            {
                return obj is IFileSystemEntity entity
                    ? new FileSystemException(string.Format(ContentRes.Error_TestCaseProject_InvalidEntityOwner, failingMethod, entity.Name))
                    : new FileSystemException(string.Format(ContentRes.Error_TestCaseProject_InvalidOwner, failingMethod));
            }

            private void AddToParent(IFileSystemContainer? newParent)
            {
                if(_parent is not null)
                    throw new InvalidOperationException(ContentRes.Error_TestCaseFileSystem_InvalidAddToParent);

                if(newParent is not null)
                {
                    var newEntities = ValidateParent(newParent);

                    newEntities.Add(this);
                }

                _parent = newParent;
            }

            private IFileSystemContainer? RemoveFromParent()
            {
                var oldParent = Interlocked.Exchange(ref _parent, null);

                if(oldParent is not null)
                {
                    var oldEntities = GetEntityListOf(oldParent);

                    try
                    {
                        oldEntities.Remove(this);
                    }
                    catch
                    {
                        _parent = oldParent;
                        throw;
                    }
                }

                return oldParent;
            }

            private void ValidateNoCyclicDependencies(IFileSystemContainer newParent)
            {
                var allParents = new List<IFileSystemContainer>();

                if(this is IFileSystemContainer container)
                    allParents.Add(container);

                var current = newParent;
                while(current is not null)
                {
                    if(allParents.Contains(current))
                    {
                        string? newParentFullPath = null;
                        string? currentFullPath = null;

                        if(newParent is IFileSystemEntity newParentEntity)
                            newParentFullPath = newParentEntity.FullName;
                        if(current is IFileSystemEntity currentEntity)
                            currentFullPath = currentEntity.FullName;

                        var newParentIsNamed = string.IsNullOrEmpty(newParentFullPath) == false;
                        var currentIsNamed = string.IsNullOrEmpty(currentFullPath) == false;

                        if(currentIsNamed && newParentIsNamed)
                            throw new FileSystemException(string.Format(ContentRes.Error_TestCaseProject_CyclicParentBoth, newParentFullPath, currentFullPath));
                        else if(newParentIsNamed)
                            throw new FileSystemException(string.Format(ContentRes.Error_TestCaseProject_CyclicParentNew, newParentFullPath));
                        else if(currentIsNamed)
                            throw new FileSystemException(string.Format(ContentRes.Error_TestCaseProject_CyclicParentCurrent, currentFullPath));
                        else
                            throw new FileSystemException(ContentRes.Error_TestCaseProject_CyclicParentNone);
                    }

                    allParents.Add(current);

                    current = (current as IFileSystemChild)?.Parent;
                }
            }

            private string DebuggerDisplay => this.FullName;

            private string _name;
            private IFileSystemContainer? _parent;
        }

        private class FileSystemFile : FileSystemEntity, IFileSystemFile
        {
            public FileSystemFile(FileSystem owner) : base(owner)
            {
                _content = [];
            }

            public override FileSystemEntity Clone(FileSystem newOwner) => new FileSystemFile(newOwner, this);
            public Stream OpenReadStream() => new MemoryStream(_content);
            public Stream OpenWriteStream() => new ContentStream(this);

            private class ContentStream : MemoryStream
            {
                public ContentStream(FileSystemFile file)
                {
                    _file = file;
                }

                public override void Flush()
                {
                    StoreContent();
                    base.Flush();
                }

                public override Task FlushAsync(CancellationToken cancel = default)
                {
                    StoreContent();

                    return base.FlushAsync(cancel);
                }

                protected override void Dispose(bool disposing)
                {
                    StoreContent();
                    base.Dispose(disposing);
                }

                private void StoreContent()
                {
                    _file._content = ToArray();
                }

                private readonly FileSystemFile _file;
            }

            private FileSystemFile(FileSystem newOwner, FileSystemFile source) : base(newOwner, source)
            {
                _content = source._content;
            }

            private byte[] _content;
        }

        private FileSystem(FileSystem source) : this()
        {
            CloneEntities(this, source, _entities);
        }

        private static void CloneEntities(FileSystem newOwner,
                                          IEnumerable<IFileSystemEntity> source,
                                          FileSystemEntityList destination)
        {
            foreach(var entity in source.Cast<FileSystemEntity>())
                destination.Add(entity.Clone(newOwner));
        }

        private bool IsObjectOwnedByThis([AllowNull] IFileSystemObject obj)
        {
            ArgumentNullException.ThrowIfNull(obj);

            if(obj is FileSystemEntity fileSystemObject && fileSystemObject.IsOwnedBy(this))
                return true;

            if(obj is FileSystem fileSystem && fileSystem == this)
                return true;

            return false;
        }

        private FileSystemDirectory NewDirectory(string name, IFileSystemContainer? parent = null)
        {
            return new FileSystemDirectory(this)
            {
                Name = name,
                Parent = parent,
            };
        }

        private FileSystemFile NewFile(string name, IFileSystemContainer? parent = null)
        {
            return new FileSystemFile(this)
            {
                Name = name,
                Parent = parent,
            };
        }

        private readonly FileSystemEntityList _entities;
    }

    [Serializable]
    public class FileSystemException : ApplicationException
    {
        public FileSystemException() : base() { }
        public FileSystemException(string? message) : base(message) { }
        public FileSystemException(string? message, Exception? innerException) : base(message, innerException) { }
    }

    private class FileSystemEntityList : IEnumerable<IFileSystemEntity>, IEnumerable
    {
        public FileSystemEntityList(Func<string>? getPath = null)
        {
            _entities = new();
            _path = new(() => getPath?.Invoke() ?? string.Empty);
        }

        public void Add(IFileSystemEntity fileSystemEntity)
        {
            ThrowIfNameIsNotUnique(fileSystemEntity);

            _entities.Add(fileSystemEntity);
        }

        public void Clear() => _entities.Clear();

        public bool Exists(string path)
        {
            ArgumentException.ThrowIfNullOrEmpty(path);

            TryGetEntity? tryGet = TryGet;

            var entityNames = path.Split(FileSystem.DirectorySeparator, StringSplitOptions.RemoveEmptyEntries);
            for(var i = 0; i < entityNames.Length; i++)
            {
                if(tryGet is null)
                    return false;

                if(tryGet(entityNames[i], out var entity) == false)
                    return false;

                if(entity is IFileSystemDirectory directory)
                    tryGet = directory.TryGet;
                else // is IFileSystemFile
                    tryGet = null;
            }

            return true;
        }

        public IEnumerable<IFileSystemDirectory> GetDirectories() => _entities.OfType<IFileSystemDirectory>();
        public IEnumerator<IFileSystemEntity> GetEnumerator() => _entities.GetEnumerator();
        public IEnumerable<IFileSystemFile> GetFiles() => _entities.OfType<IFileSystemFile>();
        public bool Remove(IFileSystemEntity fileSystemEntity) => _entities.Remove(fileSystemEntity);

        public bool TryGet(string name, [NotNullWhen(true)] out IFileSystemEntity? entity)
        {
            entity = _entities.FirstOrDefault(e => e.Name == name);

            return entity is not null;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int Count => _entities.Count;
        public IFileSystemEntity this[int index] => _entities[index];

        public IFileSystemEntity this[string name]
        {
            get => _entities.FirstOrDefault(entity => entity.Name == name)
                ?? throw new FileSystemException(BasePath == string.Empty
                    ? string.Format(ContentRes.Error_TestCaseProject_EntityNameNotFound, name)
                    : string.Format(ContentRes.Error_TestCaseProject_EntityNameNotFoundInPath, name, BasePath));
        }

        internal void ThrowIfNameIsNotUnique(IFileSystemEntity fileSystemEntity)
        {
            if(_entities.Any(entry => entry.Name == fileSystemEntity.Name))
                throw new FileSystemException(string.Format(ContentRes.Error_TestCaseFileSystem_NameAlreadyExists, fileSystemEntity.Name));
        }

        private delegate bool TryGetEntity(string name, [NotNullWhen(true)] out IFileSystemEntity? entity);

        private string BasePath => _path.Value;

        private readonly List<IFileSystemEntity> _entities;
        private readonly Lazy<string> _path;
    }

    private static IEnumerable<string> GetEntityNames(IFileSystemEntity entity) => GetEntityNamesFileToRoot(entity).Reverse();

    private static IEnumerable<string> GetEntityNamesFileToRoot(IFileSystemChild obj)
    {
        ArgumentNullException.ThrowIfNull(obj);

        var current = obj;
        while(current is not null)
        {
            if(current is IFileSystemEntity entity)
                yield return entity.Name;
            else
                throw new FileSystemException(ContentRes.Error_TestCaseFileSystem_FullPathInvalid);

            current = current.Parent as IFileSystemChild;
        }
    }
}
