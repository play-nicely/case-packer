﻿namespace PlayNicely.Projects;

[Serializable]
public class PlayNicelyException : ApplicationException
{
    public PlayNicelyException() : base() { }
    public PlayNicelyException(string? message) : base(message) { }
    public PlayNicelyException(string? message, Exception? innerException) : base(message, innerException) { }
}
