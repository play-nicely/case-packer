﻿namespace PlayNicely.Projects;

public interface IProjectPackageWriter
{
    // TODO: Support writing directories without any file content 
    Task WriteAsync(IProjectBlob blob, CancellationToken cancel = default);
}
