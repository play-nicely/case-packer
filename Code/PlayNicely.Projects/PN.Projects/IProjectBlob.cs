﻿using System.IO;

namespace PlayNicely.Projects;

public interface IProjectBlob
{
    public Stream OpenReadStream();

    public string FullName { get; }
    public bool IsProjectFile { get; }
}
