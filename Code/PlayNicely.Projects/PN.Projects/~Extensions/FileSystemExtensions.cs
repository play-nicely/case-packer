﻿using System.ComponentModel;
using System.IO;
using System.Threading.Channels;

namespace PlayNicely.Projects;

using static PlayNicely.Projects.TestCaseProject;

public static class FileSystemExtensions
{
    public static IFileSystemContainer AddFileAndContent(this IFileSystemContainer container, string name, string content)
    {
        return container.AddFileAndContent(name, stream =>
        {
            using var writer = new StreamWriter(stream);

            writer.Write(content);
        });
    }

    public static IFileSystemContainer AddFileAndContent(this IFileSystemContainer container,
                                                         string name,
                                                         Action<Stream> writeContent)
    {
        return container.AddFileAndWriteContent(name, stream =>
        {
            writeContent(stream);

            return container;
        });
    }

    public static Task<IFileSystemContainer> AddFileAndContentAsync(this IFileSystemContainer container,
                                                                    string name,
                                                                    Func<Stream, Task> writeContent,
                                                                    CancellationToken cancel = default)
    {
        cancel.ThrowIfCancellationRequested();

        return container.AddFileAndWriteContent(name, async stream =>
        {
            cancel.ThrowIfCancellationRequested();
            await writeContent(stream).ConfigureAwait(false);

            return container;
        });
    }

    public static IFileSystemContainer AddProjectFile(this TestCaseProject project, string name, string content)
    {
        return project.AddProjectFile(name, stream =>
        {
            using var writer = new StreamWriter(stream);

            writer.Write(content);
        });
    }

    public static IFileSystemContainer AddProjectFile(this TestCaseProject project, string name, Action<Stream> writeContent)
    {
        return project.AddProjectFileAndWriteContent(name, stream =>
        {
            writeContent(stream);

            return project.Root;
        });
    }

    public static Task<IFileSystemContainer> AddProjectFileAsync(this TestCaseProject project,
                                                                 string name,
                                                                 Func<Stream, Task> writeContent,
                                                                 CancellationToken cancel = default)
    {
        cancel.ThrowIfCancellationRequested();

        return project.AddProjectFileAndWriteContent(name, async stream =>
        {
            cancel.ThrowIfCancellationRequested();
            await writeContent(stream).ConfigureAwait(false);

            return (IFileSystemContainer) project.Root;
        });
    }

    private static T AddFileAndWriteContent<T>(this IFileSystemContainer container, string name, Func<Stream, T> writeContent)
    {
        ArgumentNullException.ThrowIfNull(writeContent);

        using var stream = container.AddFile(name)
                                    .OpenWriteStream();

        return writeContent(stream);
    }

    private static T AddProjectFileAndWriteContent<T>(this TestCaseProject project, string name, Func<Stream, T> writeContent)
    {
        ArgumentNullException.ThrowIfNull(writeContent);

        var projectFile = project.Root.AddFile(name);
        using var stream = projectFile.OpenWriteStream();

        project.ProjectFile = projectFile;

        return writeContent(stream);
    }
}
