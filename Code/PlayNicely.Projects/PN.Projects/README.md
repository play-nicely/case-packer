﻿[bdd]: https://en.wikipedia.org/wiki/Behavior-driven_development
[pn-executor]: https://www.nuget.org/packages/PlayNicely.Executor/
[pn-executor-dotnet]: https://www.nuget.org/packages/PlayNicely.Executor.DotNet/
[pn-specflow-executor]: https://www.nuget.org/packages/PlayNicely.SpecFlow.Executor/
[pn-specflow-dotnet]: https://www.nuget.org/packages/PlayNicely.SpecFlow.DotNet/
[pn-npmnpx]: https://www.nuget.org/packages/PlayNicely.NpmNpx/
[pn-sass]: https://www.nuget.org/packages/PlayNicely.Sass/
[vs-build-config]: https://learn.microsoft.com/en-us/visualstudio/ide/understanding-build-configurations?view=vs-2022

# Play Nicely - Projects

Play Nicely Projects supports the virtual definition of MSBuild projects, and
interfaces for reading and writing _packaged_ projects from/to _media_. These 
projects can be used to release test NuGet _tool_ packages that you may be 
developing. This package provides foundation artefacts for tool project 
testing, however, the actual test execution code and supporting
[BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) extensions are
developed separately, in these projects.

* [PlayNicely.Executor][pn-executor]
* [PlayNicely.Executor.DotNet][pn-executor-dotnet]
* [PlayNicely.SpecFlow.Executor][pn-specflow-executor]
* [PlayNicely.SpecFlow.DotNet][pn-specflow-dotnet]

These packages are used by these tools, among others:

* [PlayNicely.NpmNpx][pn-npmnpx]
* [PlayNicely.Sass][pn-sass]

## Getting Started

This base package supports the definition of a `TestCaseProject` via a virtual
`FileSystem` interface. This interface allows for the definition of directories
and files, and also the contents of those files.

The other artefacts support reading or writing of a `TestCaseProject` from/to
_media_. At this time, the project supports reading from .NET assembly
resources and writing to the physical file system. Implement concrete versions
of `IProjectPackageReader` or `IProjectPackageWriter` if you have a specific
media that is not currently supported.

## Defining Projects

The `TestCaseProject` class represents a virtual _project_, you can define a
`FileSystem` by adding directories and files using its fluent interface. You
can also specify which file is the _ProjectFile_ from the same file system.
Once defined the `TestCaseProject` doesn't do much on its own, but when
combined with
[PlayNicely.Executor][pn-executor], or
other dependent packages, it can have commands executed against it and any
output collected for assertions.

```csharp
var testCaseProject = new TestCaseProject("my-project");

// Define the project file system
var projectFile = testCaseProject.Root.AddDirectory("project1")
                                      .AddFile("proj.csproj");

testCaseProject.ProjectFile = projectFile;

using(var writer = new StreamWriter(projectFile.OpenWriteStream()))
{
    writer.WriteLine("<Project Sdk=\"Microsoft.NET.Sdk\">");
    writer.WriteLine("  <PropertyGroup>");
    writer.WriteLine("    <TargetFramework>netstandard2.0</TargetFramework>");
    writer.WriteLine("  </PropertyGroup>");
    writer.WriteLine("</Project>");
}
```

### Using Resource (resx) files

Building test case projects in code is fine, but it would be helpful if we could
define them declaritively as well.  The base package supports this by defining
projects in resx files and loading them into a `TestCaseProject` at runtime.

You would typically use this concept within the IDE, see next
[section](#using-the-ide), but you don't have to, you can declare the project 
using raw XML within a resx file.

To _load_ a `TestCaseProject` from a resource in an assembly, you use the
`ResourceSetPackageReader` class and pass it to the
`TestCaseProject.LoadFromAsync` method. The constructor requires a
`ResourceSet`, or you can use one of the static `Create` methods. If you have a
`.resx` file with a generated class, the easiest way to load the resource into
a `TestCaseProject` is like this:

```csharp
// Resx file is called Project1.resx with generation _on_
using var reader = ResourceSetPackageReader.Create<Project1>();
var testCaseProject = new TestCaseProject("Project1");

await testCaseProject.LoadFromAsync(reader);
```

#### Example

This example defines a `TestCaseProject` within a resx file. Using item keys
and values (for the file content).

If you want a `TestCaseProject` with the following structure:

```
solution-dir
|-- Project1
|   |-- Project1.csproj
|   |-- Program.cs
|
|-- Project2
|   |-- Project2.csproj
|   |-- Program.cs
|   |-- Consts.cs
|
|-- my-solution.sln
```

The resx file should have the following resources defined (note the item keys 
represent paths), and also the _special_ ProjectFile key, the value of which is
the path to the project file. If this key is present it sets the
`TestCaseProject.ProjectFile` property to the file at that path.

```xml
<?xml version="1.0" encoding="utf-8"?>
<root>
  <!-- ... schema stuff removed for simplicity ... -->
  <data name="ProjectFile" xml:space="preserve">
    <value>my-solution.sln</value>
  </data>
  <data name="my-solution.sln" xml:space="preserve">
    <value>...Excluded for bereveity...</value>
  </data>

  <data name="Project1/Project1.csproj" xml:space="preserve">
    <value>
&lt;Project Sdk="Microsoft.NET.Sdk"&gt;
  &lt;PropertyGroup&gt;
    &lt;TargetFramework&gt;net6.0&lt;/TargetFramework&gt;
  &lt;/PropertyGroup&gt;
&lt;/Project&gt;
    </value>
  </data>
  <data name="Project1/Program.cs" xml:space="preserve">
    <value>
using System;

namespace Project1
{
    public static class Program
    {
        public static int Main(params string[] args)
        {
            Console.WriteLine("Project1");

            return 0;
        }
    }
}
    </value>
  </data>

  <data name="Project2/Project2.csproj" xml:space="preserve">
    <value>
&lt;Project Sdk="Microsoft.NET.Sdk"&gt;
  &lt;PropertyGroup&gt;
    &lt;TargetFramework&gt;net6.0&lt;/TargetFramework&gt;
  &lt;/PropertyGroup&gt;
&lt;/Project&gt;
    </value>
  </data>
  <data name="Project2/Program.cs" xml:space="preserve">
    <value>
using System;

namespace Project1
{
    public static class Program
    {
        public static int Main(params string[] args)
        {
            Console.WriteLine("Project2: {0}", Consts.Version);

            return 0;
        }
    }
}
    </value>
  </data>
  <data name="Project2/Consts.cs" xml:space="preserve">
    <value>
using System;

namespace Project1
{
    public static class Consts
    {
        public const string Version = "1.2.3";
    }
}
    </value>
  </data>
</root>
```

### Using the IDE

Ok, so defining test case projects in code isn't ideal, neither is creating raw
resx files, really we want to use a familiar tool, i.e. the IDE. Rather than
specify file content using raw strings, as in the example above, we can define
projects in Visual Studio as normal, _Add Project..._, _New Item..._, etc., and
then reference them in the resx file for packaging.

> ⚠ **Watch out for Visual Studio behavior**\
  When you reference files, in a `.resx` file, _from outside the project
  directory_, Visual Studio has a tendancy to _copy_ them into the project
  structure. You may need to edit the resx files as XML and update these paths,
  to point to the correct file, as well as delete the copied files.

Our recomended setup for tool projects that require test cases, is to create a
solution structure like this.

```
solution-dir
|-- Project.UnderTest.Specs    # BDD Specifications for your project
|   |-- TestCases.Projects     # Define your test case 'packages' in resx files
|       |-- Project1.resx
|
|-- TestCase.Projects          # Define your test case projects in here using familiar tools.
|   |-- Project1
|       |-- Project1.csproj
|       |-- Program.cs
|
|-- Project.UnderTest          # The package project that needs to be tested before release
|   |-- Project.UnderTest.csproj
|   |-- SomeCode.cs
|
|-- solution.sln
```

Key points to consider:

* The `Project.UnderTest` is the actual _payload_ project of this repository.
* `Project.UnderTest.Specs` is a [BDD][bdd]
  project that tests the capabilities of `Project.UnderTest`.
  * The `Project1.resx` resource _references_ the files from
    `TestCase.Projects/Project1` directory with `ResXFileRef` items.
* The `TestCase.Projects` directory is a _space_ to define the test case projects,
  within the IDE. You can add new projects, new classes, interfaces, whatever
  you need in a familiar environment.

  > ⚠ **Unhappy path projects**\
    Beware, if this is an unhappy path test, that you need to exclude it from
    the solution build, [Configuration Manager...][vs-build-config].

The source below is an example of how the resx file's content should be set.

```xml
<?xml version="1.0" encoding="utf-8"?>
<root>
  <!-- ... schema stuff removed for simplicity ... -->
  <data name="Project1/Project1.csproj" type="System.Resources.ResXFileRef, System.Windows.Forms">
    <value>../../TestCase.Projects/Project1/Project1.csproj;System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089;utf-8</value>
  </data>
  <data name="Project1/Program.cs" type="System.Resources.ResXFileRef, System.Windows.Forms">
    <value>../../TestCase.Projects/Project1/Program.cs;System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089;utf-8</value>
  </data>
</root>
```


#### Example

The best examples, are real uses, you can find them in these dependent projects.

* [PlayNicely.NpmNpx][pn-npmnpx]
* [PlayNicely.Sass][pn-sass]
